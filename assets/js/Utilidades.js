

function MensajeForm(strTipo, strMensaje){
	var strClass = "";
	if(strTipo=="Ok"){
		strClass = strClass + " alert-success";
	}else if(strTipo=="Error"){
		strClass = strClass + " alert-danger";
	}else if(strTipo=="Advertencia"){
		strClass = strClass + " alert-warning";
	}
	$('#dvMensajeForm').removeClass('alert-danger alert-success alert-warning hidden').addClass(strClass);
	$('#spMensajeForm').html(strMensaje);
}


function Vacio(strValor){
	if((strValor==null)||(strValor=="")){
		return true;
	}else{
		return false;
	}
}

function ValidarIdUsuario(strIdUsuario){
	var _regIdUsuario = /[0-9a-zA-Z]$/;
	var _blnRespuesta = false;
	if(_regIdUsuario.test(strIdUsuario)) _blnRespuesta = true;
	return _blnRespuesta;
}

function ValidarFormatoContrasena(strContrasena){
	var _regPass = /[0-9a-zA-Z\_\-$*]$/;
	var _blnRespuesta = false;
	if(_regPass.test(strContrasena)) _blnRespuesta = true;
}


function ValidarCorreo(strCorreo){
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(strCorreo);
}

function ValidarFecha(strFecha){
	var blnCorrecto = false;
	var regexFecha = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
	if(regexFecha.test(strFecha)) blnCorrecto = true;
	return blnCorrecto;
}

function ValidarCelular(strNumero){
	var regexTel = /^[0-9]+$/;
	var blnCorrecto = false;
	if(regexTel.test(strNumero)){
		if(strNumero.length==8) blnCorrecto = true;
	}
	return blnCorrecto;
}


var getUrlParameter = function getUrlParameter(sParam) {
	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
		sURLVariables = sPageURL.split('&'),
		sParameterName,
		i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : sParameterName[1];
		}
	}
};
