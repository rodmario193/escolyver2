//SIDEBAR
function openNavbar(){
    document.getElementById('menu').style.width='100vw';
    //document.getElementById('content').style.marginLeft='100vw';
    document.getElementById('logout').style.display='none';
}
function closeNavbar(){
    document.getElementById('menu').style.width='0';
    //document.getElementById('content').style.marginLeft='0';
    document.getElementById('logout').style.display='inline';
    
}

function enableChat(){
    document.getElementById('wChat').style.display='inline-block';
    document.getElementById('wChat').style.transition='0.7s';
    document.getElementById('chatenable').style.display='inline';
    document.getElementById('chatdisable').style.display='none';
    document.getElementById('welcomeUser').style.display='none';
    document.getElementById('questUser').style.display='none';
    document.getElementById('imaUser').style.display='none';
}
function disableChat(){
    document.getElementById('wChat').style.display='none';
    document.getElementById('chatenable').style.display='none';
    document.getElementById('chatdisable').style.display='inline';
    document.getElementById('welcomeUser').style.display='inline';
    document.getElementById('questUser').style.display='inline';
    document.getElementById('imaUser').style.display='inline';
}
//PROFILE

/*
var tabButtons=document.querySelectorAll('.tabContainer .buttonContainer button');
var tabPanels=document.querySelectorAll('.tabContainer .tabPanel');
var tabBody = document.getElementById('colors');
*/

function showPanel(panelIndex, colorCode){
    let tabButtons=document.querySelectorAll('.tabContainer .buttonContainer button');
    let tabPanels=document.querySelectorAll('.tabContainer .tabPanel');
    let tabBody = document.getElementById('colors');

    tabButtons.forEach(function (node){
        node.style.backgroundColor='';
        node.style.color='';
    });
    tabButtons[panelIndex].style.backgroundColor=colorCode;
    tabButtons[panelIndex].style.color='white';
    tabPanels.forEach(function (node){
        node.style.display='none';
    });
    tabPanels[panelIndex].style.display='block';
    tabPanels[panelIndex].style.backgroundColor='colorCode';
    tabBody.style.backgroundColor=colorCode;
}
// showPanel(0,'#f44336');

//descubritelo

//favoritos

function filtrar(){
    $('.filtrar').show(300);
    $('#filtro1').hide();
};

function closefiltrar(){
    $('.filtrar').hide(1000);
    $('#filtro1').show(500);
};

function heartred(){
    $('.heartred').show();
    $('.heartgrey').hide();
    
};
function heartgrey(){
    $('.heartred').hide();
    $('.heartgrey').show();
};

//botones

function generalData(){
    $('.window_data').show(300);
    $('#button_container').hide();
    $('.space').hide();
};

function closeGeneral(){
    $('.window_data').hide(1000);
    $('#button_container').show(500);
    $('.space').show();
};

function horario(){
    $('.window_hora').show(300);
    $('#button_container').hide();
    $('.space').hide();
};

function closehorario(){
    $('.window_hora').hide(1000);
    $('#button_container').show(500);
    $('.space').show();
};
function pensum(){
    $('.window_pensum').show(300);
    $('#button_container').hide();
    $('.space').hide();
};

function closepensum(){
    $('.window_pensum').hide(1000);
    $('#button_container').show(500);
    $('.space').show();
};

function becas(){
    $('.window_becas').show(300);
    $('#button_container').hide();
    $('.space').hide();
};

function closebecas(){
    $('.window_becas').hide(1000);
    $('#button_container').show(500);
    $('.space').show();
};

function costos(){
    $('.window_costos').show(300);
    $('#button_container').hide();
    $('.space').hide();
};

function closecostos(){
    $('.window_costos').hide(1000);
    $('#button_container').show(500);
    $('.space').show();
};

function admision(){
    $('.window_admision').show(300);
    $('#button_container').hide();
    $('.space').hide();
};

function closeadmision(){
    $('.window_admision').hide(1000);
    $('#button_container').show(500);
    $('.space').show();
};

//Periodos
function peri1(){
    $('.nomcursos').show(300);

};

function closeperi1(){
    $('.nomcursos').hide(1000);

};

function peri2(){
    $('.nomcursos2').show(300);

};

function closeperi2(){
    $('.nomcursos2').hide(1000);

};

function peri3(){
    $('.nomcursos3').show(300);

};

function closeperi3(){
    $('.nomcursos3').hide(1000);

};

function peri4(){
    $('.nomcursos4').show(300);

};

function closeperi4(){
    $('.nomcursos4').hide(1000);

};

function peri5(){
    $('.nomcursos5').show(300);

};

function closeperi5(){
    $('.nomcursos5').hide(1000);

};

function peri6(){
    $('.nomcursos6').show(300);

};

function closeperi6(){
    $('.nomcursos6').hide(1000);

};

function peri7(){
    $('.nomcursos7').show(300);

};

function closeperi7(){
    $('.nomcursos7').hide(1000);

};

function peri8(){
    $('.nomcursos8').show(300);

};

function closeperi8(){
    $('.nomcursos8').hide(1000);

};

function peri9(){
    $('.nomcursos9').show(300);

};

function closeperi9(){
    $('.nomcursos9').hide(1000);

};

function peri10(){
    $('.nomcursos10').show(300);

};

function closeperi10(){
    $('.nomcursos10').hide(1000);

};