

CREATE TABLE TIPO_GRUPO(
	IdTipoGrupo INT PRIMARY KEY,
	Descripcion VARCHAR(100) NOT NULL
)
/*
<option value="1">Ingeniería</option>
<option value="2">Doctorado</option>
<option value="3">Diplomado</option>
<option value="4">Licenciatura</option>
<option value="5">Posgrado</option>
<option value="6">Maestría</option>
<option value="7">Técnico</option>
<option value="8">Profesorado</option>
<option value="9">Curso libre</option>
<option value="10">Nuevo idioma</option>
*/

INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(1,'Ingeniería');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(2,'Doctorado');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(3,'Diplomado');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(4,'Licenciatura');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(5,'Posgrado');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(6,'Maestría');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(7,'Técnico');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(8,'Profesorado');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(9,'Curso libre');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(10,'Nuevo idioma');


