

CREATE TABLE MUNICIPIO(
	IdMunicipio INT PRIMARY KEY,
	IdDepartamento INT NOT NULL,
	Descripcion VARCHAR(100) NOT NULL
);

INSERT INTO MUNICIPIO VALUES(100101,1001,'AMATITLAN');
INSERT INTO MUNICIPIO VALUES(100102,1001,'CHINAUTLA');
INSERT INTO MUNICIPIO VALUES(100103,1001,'CHUARRANCHO');
INSERT INTO MUNICIPIO VALUES(100104,1001,'FRAIJANES');
INSERT INTO MUNICIPIO VALUES(100105,1001,'GUATEMALA');
INSERT INTO MUNICIPIO VALUES(100106,1001,'MIXCO');
INSERT INTO MUNICIPIO VALUES(100107,1001,'PALENCIA');
INSERT INTO MUNICIPIO VALUES(100108,1001,'SAN JOSE DEL GOLFO');
INSERT INTO MUNICIPIO VALUES(100109,1001,'SAN JOSE PINULA');
INSERT INTO MUNICIPIO VALUES(100110,1001,'SAN JUAN SACATEPEQUEZ');
INSERT INTO MUNICIPIO VALUES(100111,1001,'SAN MIGUEL PETAPA');
INSERT INTO MUNICIPIO VALUES(100112,1001,'SAN PEDRO AYAMPUC');
INSERT INTO MUNICIPIO VALUES(100113,1001,'SAN PEDRO SACATEPEQUEZ');
INSERT INTO MUNICIPIO VALUES(100114,1001,'SAN RAYMUNDO');
INSERT INTO MUNICIPIO VALUES(100115,1001,'SANTA CATARINA PINULA');
INSERT INTO MUNICIPIO VALUES(100116,1001,'VILLA CANALES');
INSERT INTO MUNICIPIO VALUES(100117,1001,'VILLA NUEVA');
INSERT INTO MUNICIPIO VALUES(120401,1204,'EL JICARO');
INSERT INTO MUNICIPIO VALUES(120402,1204,'GUASTATOYA');
INSERT INTO MUNICIPIO VALUES(120403,1204,'MORAZAN');
INSERT INTO MUNICIPIO VALUES(120404,1204,'SAN AGUSTIN ACASAGUASTLAN');
INSERT INTO MUNICIPIO VALUES(120405,1204,'SAN ANTONIO LA PAZ');
INSERT INTO MUNICIPIO VALUES(120406,1204,'SAN CRISTOBAL ACASAGUASTLAN');
INSERT INTO MUNICIPIO VALUES(120407,1204,'SANARATE');
INSERT INTO MUNICIPIO VALUES(120408,1204,'SANSARE');
INSERT INTO MUNICIPIO VALUES(140101,1401,'ACATENANGO');
INSERT INTO MUNICIPIO VALUES(140102,1401,'CHIMALTENANGO');
INSERT INTO MUNICIPIO VALUES(140103,1401,'EL TEJAR');
INSERT INTO MUNICIPIO VALUES(140104,1401,'PARRAMOS');
INSERT INTO MUNICIPIO VALUES(140105,1401,'PATZICIA');
INSERT INTO MUNICIPIO VALUES(140106,1401,'PATZUN');
INSERT INTO MUNICIPIO VALUES(140107,1401,'SAN ANDRES ITZAPA');
INSERT INTO MUNICIPIO VALUES(140108,1401,'SAN JOSE POAQUIL');
INSERT INTO MUNICIPIO VALUES(140109,1401,'SAN JUAN COMALAPA');
INSERT INTO MUNICIPIO VALUES(140110,1401,'SAN MARTIN JILOTEPEQUE');
INSERT INTO MUNICIPIO VALUES(140111,1401,'SAN MIGUEL POCHUTA');
INSERT INTO MUNICIPIO VALUES(140112,1401,'SAN PEDRO YEPOCAPA');
INSERT INTO MUNICIPIO VALUES(140113,1401,'SANTA APOLONIA');
INSERT INTO MUNICIPIO VALUES(140114,1401,'SANTA CRUZ BALANYA');
INSERT INTO MUNICIPIO VALUES(140115,1401,'TECPAN ');
INSERT INTO MUNICIPIO VALUES(140116,1401,'ZARAGOZA');
INSERT INTO MUNICIPIO VALUES(140201,1402,'ALOTENANGO');
INSERT INTO MUNICIPIO VALUES(140202,1402,'ANTIGUA GUATEMALA');
INSERT INTO MUNICIPIO VALUES(140203,1402,'CIUDAD VIEJA');
INSERT INTO MUNICIPIO VALUES(140204,1402,'JOCOTENANGO');
INSERT INTO MUNICIPIO VALUES(140205,1402,'MAGDALENA MILPAS ALTAS');
INSERT INTO MUNICIPIO VALUES(140206,1402,'PASTORES');
INSERT INTO MUNICIPIO VALUES(140207,1402,'SAN ANTONIO AGUAS CALIENTES');
INSERT INTO MUNICIPIO VALUES(140208,1402,'SAN BARTOLOME MILPAS ALTAS');
INSERT INTO MUNICIPIO VALUES(140209,1402,'SAN LUCAS SACATEPEQUEZ');
INSERT INTO MUNICIPIO VALUES(140210,1402,'SAN MIGUEL DUENAS');
INSERT INTO MUNICIPIO VALUES(140211,1402,'SANTA CATARINA BARAHONA');
INSERT INTO MUNICIPIO VALUES(140212,1402,'SANTA LUCIA MILPAS ALTAS');
INSERT INTO MUNICIPIO VALUES(140213,1402,'SANTA MARIA DE JESUS');
INSERT INTO MUNICIPIO VALUES(140214,1402,'SANTIAGO SACATEPEQUEZ');
INSERT INTO MUNICIPIO VALUES(140215,1402,'SANTO DOMINGO XENACOJ');
INSERT INTO MUNICIPIO VALUES(140216,1402,'SUMPANGO');
INSERT INTO MUNICIPIO VALUES(110101,1101,'CAHABON');
INSERT INTO MUNICIPIO VALUES(110102,1101,'CHAHAL');
INSERT INTO MUNICIPIO VALUES(110103,1101,'CHISEC');
INSERT INTO MUNICIPIO VALUES(110104,1101,'COBAN');
INSERT INTO MUNICIPIO VALUES(110105,1101,'FRAY BARTOLOME DE LAS CASAS');
INSERT INTO MUNICIPIO VALUES(110106,1101,'LANQUIN');
INSERT INTO MUNICIPIO VALUES(110107,1101,'PANZOS');
INSERT INTO MUNICIPIO VALUES(110108,1101,'SAN CRISTOBAL VERAPAZ');
INSERT INTO MUNICIPIO VALUES(110109,1101,'SAN JUAN CHAMELCO');
INSERT INTO MUNICIPIO VALUES(110110,1101,'SAN PEDRO CARCHA');
INSERT INTO MUNICIPIO VALUES(110111,1101,'SANTA CRUZ VERAPAZ');
INSERT INTO MUNICIPIO VALUES(110112,1101,'SENAHU');
INSERT INTO MUNICIPIO VALUES(110113,1101,'TACTIC');
INSERT INTO MUNICIPIO VALUES(110114,1101,'TAMAHU');
INSERT INTO MUNICIPIO VALUES(110115,1101,'TUCURU');
INSERT INTO MUNICIPIO VALUES(110116,1101,'SANTA CATALINA LA TINTA');
INSERT INTO MUNICIPIO VALUES(110201,1102,'CUBULCO');
INSERT INTO MUNICIPIO VALUES(110202,1102,'EL CHOL');
INSERT INTO MUNICIPIO VALUES(110203,1102,'GRANADOS');
INSERT INTO MUNICIPIO VALUES(110204,1102,'PURULHA');
INSERT INTO MUNICIPIO VALUES(110205,1102,'RABINAL');
INSERT INTO MUNICIPIO VALUES(110206,1102,'SALAMA');
INSERT INTO MUNICIPIO VALUES(110207,1102,'SAN JERONIMO');
INSERT INTO MUNICIPIO VALUES(110208,1102,'SAN MIGUEL CHICAJ');
INSERT INTO MUNICIPIO VALUES(120101,1201,'EL ESTOR');
INSERT INTO MUNICIPIO VALUES(120102,1201,'LIVINGSTON');
INSERT INTO MUNICIPIO VALUES(120103,1201,'LOS AMATES');
INSERT INTO MUNICIPIO VALUES(120104,1201,'MORALES');
INSERT INTO MUNICIPIO VALUES(120105,1201,'PUERTO BARRIOS');
INSERT INTO MUNICIPIO VALUES(170101,1701,'DOLORES');
INSERT INTO MUNICIPIO VALUES(170102,1701,'FLORES');
INSERT INTO MUNICIPIO VALUES(170103,1701,'LA LIBERTAD');
INSERT INTO MUNICIPIO VALUES(170104,1701,'MELCHOR DE MENCOS');
INSERT INTO MUNICIPIO VALUES(170105,1701,'POPTUN');
INSERT INTO MUNICIPIO VALUES(170106,1701,'SAN ANDRES');
INSERT INTO MUNICIPIO VALUES(170107,1701,'SAN BENITO');
INSERT INTO MUNICIPIO VALUES(170108,1701,'SAN FRANCISCO');
INSERT INTO MUNICIPIO VALUES(170109,1701,'SAN JOSE');
INSERT INTO MUNICIPIO VALUES(170110,1701,'SAN LUIS');
INSERT INTO MUNICIPIO VALUES(170111,1701,'SANTA ANA');
INSERT INTO MUNICIPIO VALUES(170112,1701,'SAYAXCHE');
INSERT INTO MUNICIPIO VALUES(170113,1701,'SANTA ELENA');
INSERT INTO MUNICIPIO VALUES(130301,1303,'BARBERENA');
INSERT INTO MUNICIPIO VALUES(130302,1303,'CASILLAS');
INSERT INTO MUNICIPIO VALUES(130303,1303,'CHIQUIMULILLA');
INSERT INTO MUNICIPIO VALUES(130304,1303,'CUILAPA');
INSERT INTO MUNICIPIO VALUES(130305,1303,'GUAZACAPAN');
INSERT INTO MUNICIPIO VALUES(130306,1303,'NUEVA SANTA ROSA');
INSERT INTO MUNICIPIO VALUES(130307,1303,'ORATORIO');
INSERT INTO MUNICIPIO VALUES(130308,1303,'PUEBLO NUEVO VINAS');
INSERT INTO MUNICIPIO VALUES(130309,1303,'SAN JUAN TECUACO');
INSERT INTO MUNICIPIO VALUES(130310,1303,'SAN RAFAEL LAS FLORES');
INSERT INTO MUNICIPIO VALUES(130311,1303,'SANTA CRUZ NARANJO');
INSERT INTO MUNICIPIO VALUES(130312,1303,'SANTA MARIA IXHUATAN');
INSERT INTO MUNICIPIO VALUES(130313,1303,'SANTA ROSA DE LIMA');
INSERT INTO MUNICIPIO VALUES(130314,1303,'TAXISCO');
INSERT INTO MUNICIPIO VALUES(140301,1403,'ESCUINTLA');
INSERT INTO MUNICIPIO VALUES(140302,1403,'GUANAGAZAPA');
INSERT INTO MUNICIPIO VALUES(140303,1403,'IZTAPA');
INSERT INTO MUNICIPIO VALUES(140304,1403,'LA DEMOCRACIA');
INSERT INTO MUNICIPIO VALUES(140305,1403,'LA GOMERA');
INSERT INTO MUNICIPIO VALUES(140306,1403,'MASAGUA');
INSERT INTO MUNICIPIO VALUES(140307,1403,'NUEVA CONCEPCION');
INSERT INTO MUNICIPIO VALUES(140308,1403,'PALIN');
INSERT INTO MUNICIPIO VALUES(140309,1403,'PUERTO DE SAN JOSE');
INSERT INTO MUNICIPIO VALUES(140310,1403,'SAN VICENTE PACAYA');
INSERT INTO MUNICIPIO VALUES(140311,1403,'SANTA LUCIA COTZUMALGUAPA');
INSERT INTO MUNICIPIO VALUES(140312,1403,'SIQUINALA');
INSERT INTO MUNICIPIO VALUES(140313,1403,'TIQUISATE');
INSERT INTO MUNICIPIO VALUES(150501,1505,'ASINTAL');
INSERT INTO MUNICIPIO VALUES(150502,1505,'CHAMPERICO');
INSERT INTO MUNICIPIO VALUES(150503,1505,'NUEVO SAN CARLOS');
INSERT INTO MUNICIPIO VALUES(150504,1505,'RETALHULEU');
INSERT INTO MUNICIPIO VALUES(150505,1505,'SAN ANDRES VILLA SECA');
INSERT INTO MUNICIPIO VALUES(150506,1505,'SANTA CRUZ MULUA');
INSERT INTO MUNICIPIO VALUES(150507,1505,'SAN FELIPE');
INSERT INTO MUNICIPIO VALUES(150508,1505,'SAN MARTIN ZAPOTITLAN');
INSERT INTO MUNICIPIO VALUES(150509,1505,'SAN SEBASTIAN');
INSERT INTO MUNICIPIO VALUES(150601,1506,'CHICACAO');
INSERT INTO MUNICIPIO VALUES(150602,1506,'CUYOTENANGO');
INSERT INTO MUNICIPIO VALUES(150603,1506,'MAZATENANGO');
INSERT INTO MUNICIPIO VALUES(150604,1506,'PATULUL');
INSERT INTO MUNICIPIO VALUES(150605,1506,'PUEBLO NUEVO');
INSERT INTO MUNICIPIO VALUES(150606,1506,'RIO BRAVO');
INSERT INTO MUNICIPIO VALUES(150607,1506,'SAMAYAC');
INSERT INTO MUNICIPIO VALUES(150608,1506,'SAN ANTONIO SUCHITEPEQUEZ');
INSERT INTO MUNICIPIO VALUES(150609,1506,'SAN BERNARDINO');
INSERT INTO MUNICIPIO VALUES(150610,1506,'SAN FRANCISCO ZAPOTITLAN');
INSERT INTO MUNICIPIO VALUES(150611,1506,'SAN GABRIEL');
INSERT INTO MUNICIPIO VALUES(150612,1506,'SAN JOSE EL IDOLO');
INSERT INTO MUNICIPIO VALUES(150613,1506,'SAN JUAN BAUTISTA');
INSERT INTO MUNICIPIO VALUES(150614,1506,'SAN LORENZO');
INSERT INTO MUNICIPIO VALUES(150615,1506,'SAN MIGUEL PANAN');
INSERT INTO MUNICIPIO VALUES(150616,1506,'SAN PABLO JOCOPILAS');
INSERT INTO MUNICIPIO VALUES(150617,1506,'SANTA BARBARA');
INSERT INTO MUNICIPIO VALUES(150618,1506,'SANTO DOMINGO SUCHITEPEQUEZ');
INSERT INTO MUNICIPIO VALUES(150619,1506,'SANTO TOMAS LA UNION');
INSERT INTO MUNICIPIO VALUES(150620,1506,'ZUNILITO');
INSERT INTO MUNICIPIO VALUES(150101,1501,'AYUTLA');
INSERT INTO MUNICIPIO VALUES(150102,1501,'CATARINA');
INSERT INTO MUNICIPIO VALUES(150103,1501,'COMITANCILLO');
INSERT INTO MUNICIPIO VALUES(150104,1501,'CONCEPCION TUTUAPA');
INSERT INTO MUNICIPIO VALUES(150105,1501,'EL QUETZAL');
INSERT INTO MUNICIPIO VALUES(150106,1501,'EL RODEO');
INSERT INTO MUNICIPIO VALUES(150107,1501,'EL TUMBADOR');
INSERT INTO MUNICIPIO VALUES(150108,1501,'ESQUIPULAS PALO GORDO');
INSERT INTO MUNICIPIO VALUES(150109,1501,'IXCHIGUAN');
INSERT INTO MUNICIPIO VALUES(150110,1501,'LA REFORMA');
INSERT INTO MUNICIPIO VALUES(150111,1501,'MALACATAN');
INSERT INTO MUNICIPIO VALUES(150112,1501,'NUEVO PROGRESO');
INSERT INTO MUNICIPIO VALUES(150113,1501,'OCOS');
INSERT INTO MUNICIPIO VALUES(150114,1501,'PAJAPITA');
INSERT INTO MUNICIPIO VALUES(150115,1501,'RIO BLANCO');
INSERT INTO MUNICIPIO VALUES(150116,1501,'SAN ANTONIO SACATEPEQUEZ');
INSERT INTO MUNICIPIO VALUES(150117,1501,'SAN CRISTOBAL CUCHO');
INSERT INTO MUNICIPIO VALUES(150118,1501,'SAN JOSE OJETENAM');
INSERT INTO MUNICIPIO VALUES(150119,1501,'SAN LORENZO');
INSERT INTO MUNICIPIO VALUES(150120,1501,'SAN MARCOS');
INSERT INTO MUNICIPIO VALUES(150121,1501,'SAN MIGUEL IXTAHUACAN');
INSERT INTO MUNICIPIO VALUES(150122,1501,'SAN PABLO');
INSERT INTO MUNICIPIO VALUES(150123,1501,'SAN PEDRO SACATEPEQUEZ');
INSERT INTO MUNICIPIO VALUES(150124,1501,'SAN RAFAEL PIE DE LA CUESTA');
INSERT INTO MUNICIPIO VALUES(150125,1501,'SIBINAL');
INSERT INTO MUNICIPIO VALUES(150126,1501,'SIPACAPA');
INSERT INTO MUNICIPIO VALUES(150127,1501,'TACANA');
INSERT INTO MUNICIPIO VALUES(150128,1501,'TAJUMULCO');
INSERT INTO MUNICIPIO VALUES(150129,1501,'TEJUTLA');
INSERT INTO MUNICIPIO VALUES(150130,1501,'TECUN UMAN');
INSERT INTO MUNICIPIO VALUES(150201,1502,'ALMOLONGA');
INSERT INTO MUNICIPIO VALUES(150202,1502,'CABRICAN');
INSERT INTO MUNICIPIO VALUES(150203,1502,'CAJOLA');
INSERT INTO MUNICIPIO VALUES(150204,1502,'CANTEL');
INSERT INTO MUNICIPIO VALUES(150205,1502,'COATEPEQUE');
INSERT INTO MUNICIPIO VALUES(150206,1502,'COLOMBA');
INSERT INTO MUNICIPIO VALUES(150207,1502,'CONCEPCIAN CHIQUIRICHAPA');
INSERT INTO MUNICIPIO VALUES(150208,1502,'EL PALMAR');
INSERT INTO MUNICIPIO VALUES(150209,1502,'FLORES COSTACUCA');
INSERT INTO MUNICIPIO VALUES(150210,1502,'GENOVA');
INSERT INTO MUNICIPIO VALUES(150211,1502,'HUITAN');
INSERT INTO MUNICIPIO VALUES(150212,1502,'LA ESPERANZA');
INSERT INTO MUNICIPIO VALUES(150213,1502,'OLINTEPEQUE');
INSERT INTO MUNICIPIO VALUES(150214,1502,'PALESTINA DE LOS ALTOS');
INSERT INTO MUNICIPIO VALUES(150215,1502,'QUEZALTENANGO');
INSERT INTO MUNICIPIO VALUES(150216,1502,'SALCAJA');
INSERT INTO MUNICIPIO VALUES(150217,1502,'SAN CARLOS SIJA');
INSERT INTO MUNICIPIO VALUES(150218,1502,'SAN FRANCISCO LA UNION');
INSERT INTO MUNICIPIO VALUES(150219,1502,'SAN JUAN OSTUNCALCO');
INSERT INTO MUNICIPIO VALUES(150220,1502,'SAN MARTIN SACATEPEQUEZ');
INSERT INTO MUNICIPIO VALUES(150221,1502,'SAN MATEO');
INSERT INTO MUNICIPIO VALUES(150222,1502,'SAN MIGUEL SIGUILA');
INSERT INTO MUNICIPIO VALUES(150223,1502,'SIBILIA');
INSERT INTO MUNICIPIO VALUES(150224,1502,'ZUNIL');
INSERT INTO MUNICIPIO VALUES(150225,1502,'SANTA MARIA DE JESUS ( ZUNIL )');
INSERT INTO MUNICIPIO VALUES(150226,1502,'CHIQUIBAL ( SAN CARLOS SIJA )');
INSERT INTO MUNICIPIO VALUES(150227,1502,'LAS PALMAS ( COATEPEQUE )');
INSERT INTO MUNICIPIO VALUES(150228,1502,'CUICABAL ( SIBILIA )');
INSERT INTO MUNICIPIO VALUES(150229,1502,'SAN JOSE CHIQUILAJA (QUETZALTE');
INSERT INTO MUNICIPIO VALUES(150230,1502,'EL EDEN ( PALESTINA DE LOS ALT');
INSERT INTO MUNICIPIO VALUES(150231,1502,'CHUATUJ ( SAN CARLOS SIJA )');
INSERT INTO MUNICIPIO VALUES(150232,1502,'EL TAMBOR ( EL PALMAR )');
INSERT INTO MUNICIPIO VALUES(150233,1502,'LAS MERCEDES ( COLOMBA )');
INSERT INTO MUNICIPIO VALUES(150234,1502,'PALMIRA ( COLOMBA )');
INSERT INTO MUNICIPIO VALUES(150301,1503,'MOMOSTENANGO');
INSERT INTO MUNICIPIO VALUES(150302,1503,'SAN ANDRES XECUL');
INSERT INTO MUNICIPIO VALUES(150303,1503,'SAN BARTOLO');
INSERT INTO MUNICIPIO VALUES(150304,1503,'SAN CRISTOBAL TOTONICAPAN');
INSERT INTO MUNICIPIO VALUES(150305,1503,'SAN FRANCISCO EL ALTO');
INSERT INTO MUNICIPIO VALUES(150306,1503,'SANTA LUCIA LA REFORMA');
INSERT INTO MUNICIPIO VALUES(150307,1503,'SANTA MARIA CHIQUIMULA');
INSERT INTO MUNICIPIO VALUES(150308,1503,'TOTONICAPAN');
INSERT INTO MUNICIPIO VALUES(150401,1504,'CONCEPCION');
INSERT INTO MUNICIPIO VALUES(150402,1504,'NAHUALA');
INSERT INTO MUNICIPIO VALUES(150403,1504,'PANAJACHEL');
INSERT INTO MUNICIPIO VALUES(150404,1504,'SAN ANDRES SEMETABAJ');
INSERT INTO MUNICIPIO VALUES(150405,1504,'SAN ANTONIO PALOPO');
INSERT INTO MUNICIPIO VALUES(150406,1504,'SAN JOSE CHACAYA');
INSERT INTO MUNICIPIO VALUES(150407,1504,'SAN JUAN LA LAGUNA');
INSERT INTO MUNICIPIO VALUES(150408,1504,'SAN LUCAS TOLIMAN');
INSERT INTO MUNICIPIO VALUES(150409,1504,'SAN MARCOS LA LAGUNA');
INSERT INTO MUNICIPIO VALUES(150410,1504,'SAN PABLO LA LAGUNA');
INSERT INTO MUNICIPIO VALUES(150411,1504,'SAN PEDRO LA LAGUNA');
INSERT INTO MUNICIPIO VALUES(150412,1504,'SANTA CATARINA IXTAHUACAN');
INSERT INTO MUNICIPIO VALUES(150413,1504,'SANTA CATARINA PALOPO');
INSERT INTO MUNICIPIO VALUES(150414,1504,'SANTA CLARA LA LAGUNA');
INSERT INTO MUNICIPIO VALUES(150415,1504,'SANTA CRUZ LA LAGUNA');
INSERT INTO MUNICIPIO VALUES(150416,1504,'SANTA LUCIA UTATLAN');
INSERT INTO MUNICIPIO VALUES(150417,1504,'SANTA MARIA VISITACION');
INSERT INTO MUNICIPIO VALUES(150418,1504,'SANTIAGO ATITLAN');
INSERT INTO MUNICIPIO VALUES(150419,1504,'SOLOLA');
INSERT INTO MUNICIPIO VALUES(160101,1601,'AGUACATAN');
INSERT INTO MUNICIPIO VALUES(160102,1601,'BARILLAS');
INSERT INTO MUNICIPIO VALUES(160103,1601,'CHIANTLA');
INSERT INTO MUNICIPIO VALUES(160104,1601,'COLOTENANGO');
INSERT INTO MUNICIPIO VALUES(160105,1601,'CONCEPCION');
INSERT INTO MUNICIPIO VALUES(160106,1601,'CUILCO');
INSERT INTO MUNICIPIO VALUES(160107,1601,'HUEHUETENANGO');
INSERT INTO MUNICIPIO VALUES(160108,1601,'IXTAHUACAN');
INSERT INTO MUNICIPIO VALUES(160109,1601,'JACALTENANGO');
INSERT INTO MUNICIPIO VALUES(160110,1601,'LA DEMOCRACIA');
INSERT INTO MUNICIPIO VALUES(160111,1601,'LA LIBERTAD');
INSERT INTO MUNICIPIO VALUES(160112,1601,'MALACATANCITO');
INSERT INTO MUNICIPIO VALUES(160113,1601,'NENTON');
INSERT INTO MUNICIPIO VALUES(160114,1601,'SAN ANTONIO HUISTA');
INSERT INTO MUNICIPIO VALUES(160115,1601,'SAN GASPAR IXCHIL');
INSERT INTO MUNICIPIO VALUES(160116,1601,'SAN JUAN ATITLAN');
INSERT INTO MUNICIPIO VALUES(160117,1601,'SAN JUAN IXCOY');
INSERT INTO MUNICIPIO VALUES(160118,1601,'SAN MATEO IXTATAN');
INSERT INTO MUNICIPIO VALUES(160119,1601,'SAN MIGUEL ACATAN');
INSERT INTO MUNICIPIO VALUES(160120,1601,'SAN PEDRO NECTA');
INSERT INTO MUNICIPIO VALUES(160121,1601,'SAN RAFAEL LA INDEPENDENCIA');
INSERT INTO MUNICIPIO VALUES(160122,1601,'SAN RAFAEL PETZAL');
INSERT INTO MUNICIPIO VALUES(160123,1601,'SAN SEBASTIAN COATAN');
INSERT INTO MUNICIPIO VALUES(160124,1601,'SAN SEBASTIAN HUEHUETENANGO');
INSERT INTO MUNICIPIO VALUES(160125,1601,'SANTA ANA HUISTA');
INSERT INTO MUNICIPIO VALUES(160126,1601,'SANTA BARBARA');
INSERT INTO MUNICIPIO VALUES(160127,1601,'SANTA EULALIA');
INSERT INTO MUNICIPIO VALUES(160128,1601,'SANTIAGO CHIMALTENANGO');
INSERT INTO MUNICIPIO VALUES(160129,1601,'SOLOMA');
INSERT INTO MUNICIPIO VALUES(160130,1601,'TECTITLAN');
INSERT INTO MUNICIPIO VALUES(160131,1601,'TODOS SANTOS CUCHUMATANES');
INSERT INTO MUNICIPIO VALUES(160132,1601,'UNION CANTINIL');
INSERT INTO MUNICIPIO VALUES(160201,1602,'CANILLA');
INSERT INTO MUNICIPIO VALUES(160202,1602,'CHAJUL');
INSERT INTO MUNICIPIO VALUES(160203,1602,'CHICAMAN');
INSERT INTO MUNICIPIO VALUES(160204,1602,'CHICHE');
INSERT INTO MUNICIPIO VALUES(160205,1602,'CHICHICASTENANGO');
INSERT INTO MUNICIPIO VALUES(160206,1602,'CHINIQUE');
INSERT INTO MUNICIPIO VALUES(160207,1602,'CUNEN');
INSERT INTO MUNICIPIO VALUES(160208,1602,'IXCAN');
INSERT INTO MUNICIPIO VALUES(160209,1602,'JOYABAJ');
INSERT INTO MUNICIPIO VALUES(160210,1602,'NEBAJ');
INSERT INTO MUNICIPIO VALUES(160211,1602,'PACHALUN');
INSERT INTO MUNICIPIO VALUES(160212,1602,'PATZITE');
INSERT INTO MUNICIPIO VALUES(160213,1602,'SACAPULAS');
INSERT INTO MUNICIPIO VALUES(160214,1602,'SAN ANDRES SALCABAJA');
INSERT INTO MUNICIPIO VALUES(160215,1602,'SAN ANTONIO ILOTENANGO');
INSERT INTO MUNICIPIO VALUES(160216,1602,'SAN BARTOLOME JOCOTENANGO');
INSERT INTO MUNICIPIO VALUES(160217,1602,'SAN JUAN COTZAL');
INSERT INTO MUNICIPIO VALUES(160218,1602,'SAN MIGUEL USPANTAN');
INSERT INTO MUNICIPIO VALUES(160219,1602,'SAN PEDRO JOCOPILAS');
INSERT INTO MUNICIPIO VALUES(160220,1602,'SANTA CRUZ DEL QUICHE');
INSERT INTO MUNICIPIO VALUES(160221,1602,'ZACUALPA');
INSERT INTO MUNICIPIO VALUES(120201,1202,'CAMOTAN');
INSERT INTO MUNICIPIO VALUES(120202,1202,'CHIQUIMULA');
INSERT INTO MUNICIPIO VALUES(120203,1202,'CONCEPCION LAS MINAS');
INSERT INTO MUNICIPIO VALUES(120204,1202,'ESQUIPULAS');
INSERT INTO MUNICIPIO VALUES(120205,1202,'IPALA');
INSERT INTO MUNICIPIO VALUES(120206,1202,'JOCOTAN');
INSERT INTO MUNICIPIO VALUES(120207,1202,'OLOPA');
INSERT INTO MUNICIPIO VALUES(120208,1202,'QUEZALTEPEQUE');
INSERT INTO MUNICIPIO VALUES(120209,1202,'SAN JACINTO');
INSERT INTO MUNICIPIO VALUES(120210,1202,'SAN JOSE LA ARADA');
INSERT INTO MUNICIPIO VALUES(120211,1202,'SAN JUAN ERMITA');
INSERT INTO MUNICIPIO VALUES(120301,1203,'CABANAS');
INSERT INTO MUNICIPIO VALUES(120302,1203,'ESTANZUELA');
INSERT INTO MUNICIPIO VALUES(120303,1203,'GUALAN');
INSERT INTO MUNICIPIO VALUES(120304,1203,'HUITE');
INSERT INTO MUNICIPIO VALUES(120305,1203,'LA UNION');
INSERT INTO MUNICIPIO VALUES(120306,1203,'RIO HONDO');
INSERT INTO MUNICIPIO VALUES(120307,1203,'SAN DIEGO');
INSERT INTO MUNICIPIO VALUES(120308,1203,'TECULUTAN');
INSERT INTO MUNICIPIO VALUES(120309,1203,'USUMATLAN');
INSERT INTO MUNICIPIO VALUES(120310,1203,'ZACAPA');
INSERT INTO MUNICIPIO VALUES(130101,1301,'AGUA BLANCA');
INSERT INTO MUNICIPIO VALUES(130102,1301,'ASUNCION MITA');
INSERT INTO MUNICIPIO VALUES(130103,1301,'ATESCATEMPA');
INSERT INTO MUNICIPIO VALUES(130104,1301,'COMAPA');
INSERT INTO MUNICIPIO VALUES(130105,1301,'CONGUACO');
INSERT INTO MUNICIPIO VALUES(130106,1301,'EL ADELANTO');
INSERT INTO MUNICIPIO VALUES(130107,1301,'EL PROGRESO');
INSERT INTO MUNICIPIO VALUES(130108,1301,'JALPATAGUA');
INSERT INTO MUNICIPIO VALUES(130109,1301,'JEREZ');
INSERT INTO MUNICIPIO VALUES(130110,1301,'JUTIAPA');
INSERT INTO MUNICIPIO VALUES(130111,1301,'MOYUTA');
INSERT INTO MUNICIPIO VALUES(130112,1301,'PASACO');
INSERT INTO MUNICIPIO VALUES(130113,1301,'QUESADA');
INSERT INTO MUNICIPIO VALUES(130114,1301,'SAN JOSE ACATEMPA');
INSERT INTO MUNICIPIO VALUES(130115,1301,'SANTA CATARINA MITA');
INSERT INTO MUNICIPIO VALUES(130116,1301,'YUPILTEPEQUE');
INSERT INTO MUNICIPIO VALUES(130117,1301,'ZAPOTITLAN');
INSERT INTO MUNICIPIO VALUES(130201,1302,'JALAPA');
INSERT INTO MUNICIPIO VALUES(130202,1302,'MATAQUESCUINTLA');
INSERT INTO MUNICIPIO VALUES(130203,1302,'MONJAS');
INSERT INTO MUNICIPIO VALUES(130204,1302,'SAN CARLOS ALZATATE');
INSERT INTO MUNICIPIO VALUES(130205,1302,'SAN LUIS JILOTEPEQUE');
INSERT INTO MUNICIPIO VALUES(130206,1302,'SAN MANUEL CHAPARRON');
INSERT INTO MUNICIPIO VALUES(130207,1302,'SAN PEDRO PINULA');





