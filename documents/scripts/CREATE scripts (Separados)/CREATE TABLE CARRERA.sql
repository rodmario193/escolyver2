CREATE TABLE CARRERA (
  IdCarrera INT PRIMARY KEY AUTO_INCREMENT,
  Nombre varchar(200) NOT NULL,
  Descripcion TEXT NULL,
  DescripcionInterna TEXT NULL,
  Grupo INT NOT NULL,
  NombreFacultad varchar(100) NULL,
  TelefonoFacultad varchar(30) DEFAULT NULL,
  Modalidad varchar(11) DEFAULT NULL,
  CostoMensual float DEFAULT NULL,
  CostoMatricula float DEFAULT NULL,
  CostoInscripcion float DEFAULT NULL,
  AplicaBeca int(11) DEFAULT '0',
  DetallesBeca text,
  ReqTesis int(11) DEFAULT '0',
  ReqPrivado int(11) DEFAULT '0',
  ReqMaestria int(11) DEFAULT '0',
  ReqCreditos int(11) DEFAULT '0',
  ReqEps int(11) DEFAULT '0',
  DetallesCierre text,
  HIniMat varchar(10) DEFAULT NULL,
  HFinMat varchar(11) DEFAULT NULL,
  HIniVesp varchar(11) DEFAULT NULL,
  HFinVesp varchar(11) DEFAULT NULL,
  HIniNoc varchar(11) DEFAULT NULL,
  HFinNoc varchar(11) DEFAULT NULL,
  HIniSab varchar(11) DEFAULT NULL,
  HFinSab varchar(11) DEFAULT NULL,
  HIniDom varchar(11) DEFAULT NULL,
  HFinDom varchar(11) DEFAULT NULL,
  DetallesNuevoIngreso text,
  DetallesReingreso text,
  DetallesContinuacion text,
  DetallesTraslado text,
  DetallesExtranjero text,
  DuracionPeriodo INT NULL,
  DetallesPago TEXT NULL,
  DetallesPromociones TEXT NULL,
  UsuarioModifica VARCHAR(30) NULL,
  FechaModifica DATETIME NULL,
  Duracion_Periodo_Meses INT DEFAULT 6
  FOREIGN KEY (Grupo) REFERENCES GRUPO(IdGrupo) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO CARRERA(Nombre, Grupo, Descripcion) VALUES ("Ingenieria en Ciencias y Sistemas", 1, "Ingenieria en Sistemas USAC");
INSERT INTO CARRERA(Nombre, Grupo, Descripcion) VALUES ("Psicología", 8, "Psicología USAC");

ALTER TABLE CARRERA ADD COLUMN DetallesPago TEXT NULL;
ALTER TABLE CARRERA ADD COLUMN DetallesPromociones TEXT NULL;
