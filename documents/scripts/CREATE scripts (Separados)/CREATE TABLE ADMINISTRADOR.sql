CREATE TABLE ADMINISTRADOR(
	IdAdmin VARCHAR(30) PRIMARY KEY,
	Nombre VARCHAR(100) NOT NULL,
	Correo VARCHAR(30) NOT NULL,
	Password VARCHAR(30) NOT NULL,
	FechaActualizacion DATE NULL,
	UsuarioActualiza VARCHAR(30) NULL,
	Token TEXT NULL,
	FechaToken DATETIME NULL
);


INSERT INTO ADMINISTRADOR(IdAdmin,Nombre,Correo, Password) VALUES ('JOSADMIN','Josue Sigui','jsigui@gmail.com','abc123');
