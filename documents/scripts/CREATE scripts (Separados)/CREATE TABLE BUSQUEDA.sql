

CREATE TABLE BUSQUEDA(
	ID_BUSQUEDA INT PRIMARY KEY AUTO_INCREMENT,
	ID_USUARIO VARCHAR(30) NOT NULL,
	FECHA_CREACION DATETIME NOT NULL,
	ESTADO INT DEFAULT 1,
	ID_COMPRA INT NOT NULL,
    ID_GRUPO INT NULL,
    FECHA_UTILIZACION DATETIME NULL,
	PRUEBA INT NOT NULL,
    FOREIGN KEY (ID_GRUPO) REFERENCES GRUPO(IdGrupo)
);	

