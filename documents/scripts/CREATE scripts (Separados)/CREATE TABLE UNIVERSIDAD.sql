
CREATE TABLE UNIVERSIDAD(
	IdUniversidad INT PRIMARY KEY AUTO_INCREMENT,
	Nombre VARCHAR(100) NOT NULL,
	Departamento INT NOT NULL,
	Municipio INT NOT NULL,
	Localidad VARCHAR(100) NULL,
	Zona INT NULL,
	Telefono1 VARCHAR(30) NULL,
	Telefono2 VARCHAR(30) NULL,
	Url VARCHAR(100) NULL,
	DetalleInscripcion TEXT NULL,
	CostoParqueo FLOAT NULL,
	Disponibilidad INT NULL,
	DetalleParqueo TEXT NULL,
	Imagen MEDIUMBLOB NULL,
	InfoAdicional TEXT NULL,
	UsuarioModifica VARCHAR(30) NULL,
	FechaModificacion DATETIME NULL
);


INSERT INTO UNIVERSIDAD(Nombre,Departamento,Municipio,Localidad,Zona,Telefono1,Telefono2,Url,DetalleInscripcion,CostoParqueo,Disponibilidad,DetalleParqueo,Imagen) 
VALUES ('Universidad de San Carlos de Guatemala', 1001, 100101, NULL, 12, NULL, NULL, 'www.usac.edu.gt', NULL, 100.00, 1, NULL, NULL);
INSERT INTO UNIVERSIDAD(Nombre,Departamento,Municipio,Localidad,Zona,Telefono1,Telefono2,Url,DetalleInscripcion,CostoParqueo,Disponibilidad,DetalleParqueo,Imagen) 
VALUES ('Universidad Mariano Galvez de Guatemala', 1001, 100101, "zona 2 Guatemala", 12, "23000001", "23000002", 'www.umg.edu.gt', 
"Para iniciar el proceso de inscripcion debe llamar a nuestras oficinas en horarios laborales", 500.00, 1, "Parqueo disponible, solicite a Administracion", NULL);


ALTER TABLE UNIVERSIDAD ADD COLUMN InfoAdicional TEXT NULL