
CREATE TABLE CURSO(
	IdCurso INT PRIMARY KEY AUTO_INCREMENT,
	Nombre VARCHAR(100) NOT NULL,
	Descripcion TEXT NULL,
	Categoria VARCHAR(30) NULL,
	UsuarioModifica VARCHAR(30) NULL,
	FechaModificacion DATETIME NULL
);

INSERT INTO CURSO (Nombre, Descripcion) VALUES('Matematica Basica 1','Matematica Basica 1 USAC');
INSERT INTO CURSO (Nombre, Descripcion) VALUES('Matematica Basica 2','Matematica Basica 2 USAC');


