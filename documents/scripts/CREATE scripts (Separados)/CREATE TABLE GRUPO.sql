

CREATE TABLE GRUPO(
	IdGrupo INT PRIMARY KEY AUTO_INCREMENT,
	Nombre VARCHAR(100) NOT NULL,
	TipoGrupo INT NOT NULL,
	Descripcion TEXT NULL,
	UsuarioModifica VARCHAR(30) NULL,
	FechaModificacion DATETIME NULL,
	Prueba INT NOT NULL,
	FOREIGN KEY (TipoGrupo) REFERENCES TIPO_GRUPO(IdTipoGrupo) ON DELETE CASCADE
);

INSERT INTO GRUPO(Nombre, TipoGrupo, Descripcion) VALUES('Ingenieria en Sistemas',1,'Ingenieria en Sistemas');
INSERT INTO GRUPO(Nombre, TipoGrupo, Descripcion) VALUES('Ingenieria Industrial',1,'Ingenieria Industrial');
INSERT INTO GRUPO(Nombre, TipoGrupo, Descripcion) VALUES('Administracion de Empresas',4,'Administracion de Empresas');



