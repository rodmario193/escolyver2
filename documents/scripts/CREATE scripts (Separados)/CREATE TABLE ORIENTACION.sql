CREATE TABLE ORIENTACION(
	IdOrientacion INT PRIMARY KEY AUTO_INCREMENT,
	IdUsuario VARCHAR(30),
	Test1 INT DEFAULT 0,
	Test2 INT DEFAULT 0,
	Test3 INT DEFAULT 0,
	Grupo1 INT NULL,
	Grupo2 INT NULL,
	Grupo3 INT NULL,
	Estado INT DEFAULT 0,
	IdCompra INT NOT NULL,
	Fecha_Creacion DATETIME NOT NULL,
	FOREIGN KEY (IdUsuario) REFERENCES USUARIO(IdUsuario) ON DELETE CASCADE
);
