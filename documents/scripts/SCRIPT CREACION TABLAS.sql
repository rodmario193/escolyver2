
/* ADMINISTRADOR */
CREATE TABLE ADMINISTRADOR(
	IdAdmin VARCHAR(30) PRIMARY KEY,
	Nombre VARCHAR(100) NOT NULL,
	Correo VARCHAR(30) NOT NULL,
	Password VARCHAR(30) NOT NULL,
	FechaActualizacion DATE NULL,
	UsuarioActualiza VARCHAR(30) NULL,
	Token TEXT NULL,
	FechaToken DATETIME NULL
);
INSERT INTO ADMINISTRADOR(IdAdmin,Nombre,Correo, Password) VALUES ('JOSADMIN','Josue Sigui','jsigui@gmail.com','abc123');
INSERT INTO ADMINISTRADOR(IdAdmin,Nombre,Correo, Password) VALUES ('PAOLA_ADM','Paola','p@admin.com','3scoly123$$');
INSERT INTO ADMINISTRADOR(IdAdmin,Nombre,Correo, Password) VALUES ('EDU_ADM','Eduardo','e@admin.com','3scoly123$$');
INSERT INTO ADMINISTRADOR(IdAdmin,Nombre,Correo, Password) VALUES ('AXEL_ADM','Axel','a@admin.com','3scoly123$$');
INSERT INTO ADMINISTRADOR(IdAdmin,Nombre,Correo, Password) VALUES ('DIEGO_ADM','Diego','d@admin.com','3scoly123$$');
/*TIPO GRUPO */
CREATE TABLE TIPO_GRUPO(
	IdTipoGrupo INT PRIMARY KEY,
	Descripcion VARCHAR(100) NOT NULL
);
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(1,'Ingeniería');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(2,'Doctorado');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(3,'Diplomado');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(4,'Licenciatura');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(5,'Posgrado');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(6,'Maestría');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(7,'Técnico');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(8,'Profesorado');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(9,'Curso libre');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(10,'Nuevo idioma');
INSERT INTO TIPO_GRUPO(IdTipoGrupo, Descripcion) VALUES(11,'Certificación');
/*GRUPO*/
CREATE TABLE GRUPO(
	IdGrupo INT PRIMARY KEY AUTO_INCREMENT,
	Nombre VARCHAR(100) NOT NULL,
	TipoGrupo INT NOT NULL,
	Descripcion TEXT NULL,
	UsuarioModifica VARCHAR(30) NULL,
	FechaModificacion DATETIME NULL,
	Prueba INT NOT NULL,
	FOREIGN KEY (TipoGrupo) REFERENCES TIPO_GRUPO(IdTipoGrupo) ON DELETE CASCADE
);
/*UNIVERSIDAD*/
CREATE TABLE UNIVERSIDAD(
	IdUniversidad INT PRIMARY KEY AUTO_INCREMENT,
	Nombre VARCHAR(100) NOT NULL,
	Departamento INT NOT NULL,
	Municipio INT NOT NULL,
	Localidad VARCHAR(200) NULL,
	Zona INT NULL,
	Telefono1 VARCHAR(30) NULL,
	Telefono2 VARCHAR(30) NULL,
	Url VARCHAR(100) NULL,
	DetalleInscripcion TEXT NULL,
	CostoParqueo FLOAT NULL,
	Disponibilidad INT NULL,
	DetalleParqueo TEXT NULL,
	Imagen MEDIUMBLOB NULL,
	InfoAdicional TEXT NULL,
	UsuarioModifica VARCHAR(30) NULL,
	FechaModificacion DATETIME NULL
);
/*CURSO*/
CREATE TABLE CURSO(
	IdCurso INT PRIMARY KEY AUTO_INCREMENT,
	Nombre VARCHAR(100) NOT NULL,
	Descripcion TEXT NULL,
	Categoria VARCHAR(30) NULL,
	UsuarioModifica VARCHAR(30) NULL,
	FechaModificacion DATETIME NULL
);
/*CARRERA*/
CREATE TABLE CARRERA (
  IdCarrera INT PRIMARY KEY AUTO_INCREMENT,
  Nombre varchar(200) NOT NULL,
  Descripcion TEXT NULL,
  DescripcionInterna TEXT NULL,
  Grupo INT NOT NULL,
  NombreFacultad varchar(100) NULL,
  TelefonoFacultad varchar(30) DEFAULT NULL,
  Modalidad varchar(11) DEFAULT NULL,
  CostoMensual float DEFAULT NULL,
  CostoMatricula float DEFAULT NULL,
  CostoInscripcion float DEFAULT NULL,
  AplicaBeca int(11) DEFAULT '0',
  DetallesBeca text,
  ReqTesis int(11) DEFAULT '0',
  ReqPrivado int(11) DEFAULT '0',
  ReqMaestria int(11) DEFAULT '0',
  ReqCreditos int(11) DEFAULT '0',
  ReqEps int(11) DEFAULT '0',
  DetallesCierre text,
  HIniMat varchar(10) DEFAULT NULL,
  HFinMat varchar(11) DEFAULT NULL,
  HIniVesp varchar(11) DEFAULT NULL,
  HFinVesp varchar(11) DEFAULT NULL,
  HIniNoc varchar(11) DEFAULT NULL,
  HFinNoc varchar(11) DEFAULT NULL,
  HIniSab varchar(11) DEFAULT NULL,
  HFinSab varchar(11) DEFAULT NULL,
  HIniDom varchar(11) DEFAULT NULL,
  HFinDom varchar(11) DEFAULT NULL,
  DetallesNuevoIngreso text,
  DetallesReingreso text,
  DetallesContinuacion text,
  DetallesTraslado text,
  DetallesExtranjero text,
  DuracionPeriodo INT NULL,
  DetallesPago TEXT NULL,
  DetallesPromociones TEXT NULL,
  UsuarioModifica VARCHAR(30) NULL,
  FechaModifica DATETIME NULL,
	Duracion_Periodo_Meses INT DEFAULT 6,
	CorreoFacultad VARCHAR(100) NULL,
	Certi_Internacional int(11) DEFAULT '0',
	DescripcionHorarios VARCHAR(200) NULL,
	ModalidadPresencial INT(11) DEFAULT 0,
	ModalidadSemiPresencial INT(11) DEFAULT 0,
	ModalidadEnLinea INT(11) DEFAULT 0,
	DescripcionModalidad VARCHAR(200) NULL,
  FOREIGN KEY (Grupo) REFERENCES GRUPO(IdGrupo) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*CARRERA X UNI*/
CREATE TABLE CARRERAXUNI (
	IdCarrera INT NOT NULL,
	IdUniversidad INT NOT NULL,
	PRIMARY KEY(IdCarrera, IdUniversidad),
	FOREIGN KEY (IdCarrera) REFERENCES CARRERA(IdCarrera) ON DELETE CASCADE,
	FOREIGN KEY (IdUniversidad) REFERENCES UNIVERSIDAD(IdUniversidad) ON DELETE CASCADE
);
/*PERIODO*/
CREATE TABLE PERIODO(
	IdPeriodo INT PRIMARY KEY AUTO_INCREMENT,
	Orden INT NOT NULL,
	IdCarrera INT NOT NULL,
	FOREIGN KEY (IdCarrera) REFERENCES CARRERA(IdCarrera) ON DELETE CASCADE
);
/*CURSO X PERIODO*/
CREATE TABLE CURSOXPERIODO(
	IdPeriodo INT NOT NULL,
	IdCurso INT NOT NULL,
	FOREIGN KEY (IdPeriodo) REFERENCES PERIODO(IdPeriodo) ON DELETE CASCADE,
	FOREIGN KEY (IdCurso) REFERENCES CURSO(IdCurso) ON DELETE CASCADE
);
/*USUARIO*/
CREATE TABLE USUARIO(
	IdUsuario VARCHAR(30) PRIMARY KEY,
	Password NVARCHAR(30) NOT NULL,
	PrimerNombre VARCHAR(30) NOT NULL,
	SegundoNombre VARCHAR(30) NULL,
	PrimerApellido VARCHAR(30) NOT NULL,
	SegundoApellido VARCHAR(30) NULL,
	Genero VARCHAR(2) NOT NULL,
	FechaNacimiento VARCHAR(20) NULL,
	Correo VARCHAR(50) NOT NULL,
	TelefonoCelular VARCHAR(30) NOT NULL,
	Departamento INT NOT NULL,
	Municipio INT NOT NULL,
	Direccion TEXT NULL,
	Instituto VARCHAR(100) NULL,
	NombreFacturacion VARCHAR(100) NULL,
	ApellidoFacturacion VARCHAR(100) NULL,
	Nit VARCHAR(30) NULL,
	DepartamentoFacturacion INT NULL,
	Imagen MEDIUMBLOB NULL,
	Activo INT DEFAULT 0,
	Token TEXT NULL,
	FechaToken DATETIME NULL,
	RedSocial VARCHAR(15) NOT NULL,
	Medio VARCHAR(15) NOT NULL
);
/*ITEM COMPRA*/
CREATE TABLE ITEM_COMPRA(
	IdItem VARCHAR(5) PRIMARY KEY,
	Cantidad INT NOT NULL,
	Saldo FLOAT NOT NULL,
	Tipo CHAR NOT NULL
);
-- Busquedas
INSERT INTO ITEM_COMPRA VALUES('001',1,7.00,'B');
INSERT INTO ITEM_COMPRA VALUES('002',2,10.00,'B');
INSERT INTO ITEM_COMPRA VALUES('003',3,13.00,'B');
-- Orientacion
INSERT INTO ITEM_COMPRA VALUES('100',1,100.00,'O');
/*COMPRA*/
CREATE TABLE COMPRA(
	IdCompra INT AUTO_INCREMENT,
	IdUsuario VARCHAR(50) NOT NULL,
	FechaCompra DATETIME NOT NULL,
	IdTransaccion VARCHAR(100) NOT NULL,
	IdItemCompra VARCHAR(5) NOT NULL,
	PRIMARY KEY(IdCompra)
);
INSERT INTO COMPRA (IdUsuario, FechaCompra, IdTransaccion) VALUES ('C0MPR@',NOW(),'C0mpr@Pru3b@');
/*BUSQUEDA*/
CREATE TABLE BUSQUEDA(
	ID_BUSQUEDA INT PRIMARY KEY AUTO_INCREMENT,
	ID_USUARIO VARCHAR(30) NOT NULL,
	FECHA_CREACION DATETIME NOT NULL,
	ESTADO INT DEFAULT 1,
	ID_COMPRA INT NOT NULL,
    ID_GRUPO INT NULL,
    FECHA_UTILIZACION DATETIME NULL,
	PRUEBA INT NOT NULL,
    FOREIGN KEY (ID_GRUPO) REFERENCES GRUPO(IdGrupo)
);

CREATE TABLE FAVORITO(
	IdUsuario VARCHAR(30) NOT NULL,
    IdCarrera INT NOT NULL,
    PRIMARY KEY (IdUsuario, IdCarrera),
    FOREIGN KEY (IdUsuario) REFERENCES USUARIO(IdUsuario) ON DELETE CASCADE,
    FOREIGN KEY (IdCarrera) REFERENCES CARRERA(IdCarrera) ON DELETE CASCADE
);
CREATE TABLE CALIFICACION(
	IdUsuario VARCHAR(30) NOT NULL,
    IdCarrera INT NOT NULL,
    Nota INT NOT NULL,
    PRIMARY KEY (IdUsuario, IdCarrera),
    FOREIGN KEY (IdUsuario) REFERENCES USUARIO(IdUsuario) ON DELETE CASCADE,
    FOREIGN KEY (IdCarrera) REFERENCES CARRERA(IdCarrera) ON DELETE CASCADE
);
CREATE TABLE ORIENTACION(
	IdOrientacion INT PRIMARY KEY AUTO_INCREMENT,
	IdUsuario VARCHAR(30),
	Test1 INT DEFAULT 0,
	Test2 INT DEFAULT 0,
	Test3 INT DEFAULT 0,
	Grupo1 INT NULL,
	Grupo2 INT NULL,
	Grupo3 INT NULL,
	Estado INT DEFAULT 0,
	IdCompra INT NOT NULL,
	Fecha_Creacion DATETIME NOT NULL,
	FOREIGN KEY (IdUsuario) REFERENCES USUARIO(IdUsuario) ON DELETE CASCADE
);

CREATE TABLE CONFIGURACION(
	Llave VARCHAR(30) PRIMARY KEY,
  Valor VARCHAR(30) NOT NULL
);


CREATE TABLE REGISTRO_ORIENTACION(
		IdOrientacion INT PRIMARY KEY,
    Actividad1 VARCHAR(100),
    Actividad2 VARCHAR(100) NULL,
    Actividad3 VARCHAR(100) NULL,
    Actividad4 VARCHAR(100) NULL,
    Actividad5 VARCHAR(100) NULL,
    Actividad6 VARCHAR(100) NULL,
    Objeto1 VARCHAR(100),
    Objeto2 VARCHAR(100) NULL,
    Objeto3 VARCHAR(100) NULL,
    Objeto4 VARCHAR(100) NULL,
    Objeto5 VARCHAR(100) NULL,
    Objeto6 VARCHAR(100) NULL,
    Logro1 VARCHAR(100),
    Logro2 VARCHAR(100) NULL,
    Logro3 VARCHAR(100) NULL,
    Logro4 VARCHAR(100) NULL,
    Pregunta1Opcion1 INT,
    Pregunta1Opcion2 INT,
    Pregunta1Opcion3 INT,
    Pregunta1Opcion4 INT,
    Pregunta2Opcion1 INT,
    Pregunta2Opcion2 INT,
    Pregunta2Opcion3 INT,
    Pregunta2Opcion4 INT,
    Pregunta3Opcion1 INT,
    Pregunta3Opcion2 INT,
    Pregunta3Opcion3 INT,
    Pregunta3Opcion4 INT,
    Pregunta3Opcion5 INT,
    Pregunta3Opcion6 INT,
    Pregunta3Opcion7 INT,
    EstudioMama VARCHAR(100) NULL,
    GraduacionMama INT,
    TrabajoMama VARCHAR(100) NULL,
    EstudioPapa VARCHAR(100) NULL,
    GraduacionPapa INT,
    TrabajoPapa VARCHAR(100) NULL,
    EstudiosHermanos1 VARCHAR(100) NULL,
    EstudiosHermanos2 VARCHAR(100) NULL,
    EstudiosHermanos3 VARCHAR(100) NULL,
    EstudiosHermanos4 VARCHAR(100) NULL,
    EstudiosFamiliares1 VARCHAR(100) NULL,
    EstudiosFamiliares2 VARCHAR(100) NULL,
    EstudiosFamiliares3 VARCHAR(100) NULL,
    EstudiosFamiliares4 VARCHAR(100) NULL,
    EstudiosFamiliares5 VARCHAR(100) NULL,
    EstudiosFamiliares6 VARCHAR(100) NULL
);
