
	-- DROP PROCEDURE SP_CONSUMIR_BUSQUEDA;

	DELIMITER $$
	CREATE PROCEDURE SP_CONSUMIR_BUSQUEDA(	
		IN IdUsuario_IN VARCHAR(50),
		IN IdGrupo_IN INT,
		IN IN_Prueba INT,
		OUT RESPUESTA INT
	)
	BEGIN
		DECLARE EXIT HANDLER FOR SQLEXCEPTION
		BEGIN
			SELECT 0 INTO RESPUESTA;
			ROLLBACK;
		END;

		START TRANSACTION;
		
		-- Validar que se tengan busquedas disponibles
		SET 	@NUM_BUSQUEDAS = 0;
		SELECT 	COUNT(1) INTO @NUM_BUSQUEDAS
		FROM 	BUSQUEDA
		WHERE	ID_USUARIO = IdUsuario_IN
		AND		ESTADO = 1
		AND		PRUEBA = 
		(
			CASE
				WHEN IN_Prueba = 1 THEN 1
				ELSE 0
			END
		);

		IF @NUM_BUSQUEDAS > 0 THEN
		
			-- Validar que no se haya buscado el mismo grupo
			SET @BUSQUEDA_REPETIDA = 0;
			SELECT 	COUNT(1) INTO @BUSQUEDA_REPETIDA
			FROM 	BUSQUEDA
			WHERE	ID_USUARIO = IdUsuario_IN
			AND		ID_GRUPO = IdGrupo_IN
			AND		ESTADO = 0
            AND		FECHA_UTILIZACION > NOW() - INTERVAL 1 WEEK;
			
			IF @BUSQUEDA_REPETIDA = 0 THEN
				SET @ID_BUSQUEDA = 0;
				SELECT 	MAX(ID_BUSQUEDA) INTO @ID_BUSQUEDA
				FROM 	BUSQUEDA
				WHERE	ID_USUARIO = IdUsuario_IN
				AND 	ESTADO = 1
				AND		PRUEBA = 
				(
					CASE
						WHEN IN_Prueba = 1 THEN 1
						ELSE 0
					END
				);
				
				UPDATE 	BUSQUEDA
				SET		ID_GRUPO = IdGrupo_IN,
						ESTADO = 0,
						FECHA_UTILIZACION = NOW()
				WHERE	ID_BUSQUEDA = @ID_BUSQUEDA;
			
				SELECT 1 INTO RESPUESTA;
			ELSE
				SELECT 2 INTO RESPUESTA;
			END IF;
			
		ELSE
			SELECT 3 INTO RESPUESTA;
		END IF;
		
		
		COMMIT;
	END$$






