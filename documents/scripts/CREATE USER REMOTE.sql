
CREATE USER 'escolyadmin'@'localhost' IDENTIFIED BY '3Scoly@dm!n';
CREATE USER 'escolyadmin'@'%' IDENTIFIED BY '3Scoly@dm!n';
GRANT ALL ON *.* TO 'escolyadmin'@'localhost';
GRANT ALL ON *.* TO 'escolyadmin'@'%';
FLUSH PRIVILEGES; 
EXIT;
