<?php

require_once('config.php');
require_once('Pagadito.php');

$Pagadito = new Pagadito(UID, WSK);

$Pagadito->mode_sandbox_on();

if ($Pagadito->connect()) {
	$Pagadito->add_detail(1, 'Producto de prueba', '50', 'producto_prueba.jpg');
	$ern = rand(1000, 2000);
	if (!$Pagadito->exec_trans($ern)) {
		/*
		 * En caso de fallar la transacción, verificamos el error devuelto.
		 * Debido a que la API nos puede devolver diversos mensajes de
		 * respuesta, validamos el tipo de mensaje que nos devuelve.
		 */
		switch($Pagadito->get_rs_code())
		{
			case "PG2001":
				/*Incomplete data*/
			case "PG3002":
				/*Error*/
			case "PG3003":
				/*Unregistered transaction*/
			case "PG3004":
				/*Match error*/
			case "PG3005":
				/*Disabled connection*/
			default:
				echo "
					<SCRIPT>
						alert(\"".$Pagadito->get_rs_code().": ".$Pagadito->get_rs_message()."\");
						location.href = 'index.php';
					</SCRIPT>
				";
				break;
		}
	}
	
}else {
	/*
	 * En caso de fallar la conexión, verificamos el error devuelto.
	 * Debido a que la API nos puede devolver diversos mensajes de
	 * respuesta, validamos el tipo de mensaje que nos devuelve.
	 */
	switch($Pagadito->get_rs_code())
	{
		case "PG2001":
			/*Incomplete data*/
		case "PG3001":
			/*Problem connection*/
		case "PG3002":
			/*Error*/
		case "PG3003":
			/*Unregistered transaction*	/
		case "PG3005":
			/*Disabled connection*/
		case "PG3006":
			/*Exceeded*/
		default:
			echo "
				<SCRIPT>
					alert(\"".$Pagadito->get_rs_code().": ".$Pagadito->get_rs_message()."\");
					location.href = 'index.php';
				</SCRIPT>
			";
			break;
	}
}


?>