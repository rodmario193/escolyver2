

angular.module('mantCursocarrera').component('mantCursocarrera', {
    templateUrl: 'mant-cursocarrera/mant-cursocarrera.template.html',
    controller: ['$routeParams','$http','Autenticacion',
	function mantCursoCarreraController($routeParams, $http, Autenticacion) {
		Autenticacion.ValidarSesion();
		var self = this;
		self.Carrera = {
			"IdCarrera": $routeParams.idCarrera,
			"Nombre": $routeParams.NombreCarrera
		};

		var _strIdCarrera = $routeParams.idCarrera;

		ObtenerPeriodos(self);

    self.ActualizarDuracion = function(){
      $('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "actduracion",
				"IdCarrera": self.Carrera.IdCarrera,
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ===="),
        "NuevaDuracion": self.NuevaDuracion
			};
			$http({
			method: 'POST',
			url: '../../services/cursocarrera.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				var _Respuesta = response.data.Respuesta;
        if(_Respuesta.Exito){
          MensajeForm("Ok", _Respuesta.Mensaje);
          self.Duracion = self.NuevaDuracion;
        }else{
          MensajeForm("Error", _Respuesta.Mensaje);
        }
				$('#mdLoading').modal('hide');
        $('#mdCambiarDuracion').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
    }

		self.AgregarPeriodo = function(){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "addperiodo",
				"IdCarrera": self.Carrera.IdCarrera,
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
			};
			$http({
			method: 'POST',
			url: '../../services/cursocarrera.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo == 1){
					MensajeForm("Ok", "Periodo agregado exitosamente");
					ObtenerPeriodos(self);
				}else{
					MensajeForm("Error", "Error agregando periodo, contacte al administrador");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}


		self.CambioDespliegue = function(Periodo){
			Periodo.BanderaDesp = !Periodo.BanderaDesp;
		}


		self.VerModalAgregarCurso = function(Periodo){
			self.PeriodoSel = Periodo;
			console.log(Periodo.Id);
			ObtenerCursosSinAsignar(self);
			$('#mdAgregarCursos').modal('show');
		}


		self.VerModalEliminarPeriodo = function(Periodo){
			self.PeriodoEliminar = Periodo;
			$('#mdConfElimPeriodo').modal('show');
		}


		self.EliminarPeriodo = function(){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "elimperiodo",
				"IdPeriodo": self.PeriodoEliminar.Id,
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
			};
			$http({
			method: 'POST',
			url: '../../services/cursocarrera.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo == 1){
					MensajeForm("Ok", "Periodo eliminado exitosamente");
					$('#mdConfElimPeriodo').modal('hide');
					ObtenerPeriodos(self);
				}else{
					MensajeForm("Error", "Error eliminando período, contacte al administrador");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}


		self.VerModalEliminarCurso = function(Periodo, Curso){
			self.CursoEliminar = Curso;
			self.PeriodoEliminar = Periodo;
			$('#mdConfEliminar').modal('show');
		}


		self.EliminarCursoDePeriodo = function(){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "eliminar",
				"IdCurso": self.CursoEliminar.Id,
				"IdPeriodo": self.PeriodoEliminar.Id,
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
			};
			$http({
			method: 'POST',
			url: '../../services/cursocarrera.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo == 1){
					MensajeForm("Ok", "Curso eliminado exitosamente");
					$('#mdConfEliminar').modal('hide');
					ObtenerPeriodos(self);
				}else{
					MensajeForm("Error", "Error eliminando curso, contacte al administrador");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}


		self.AsignarCursosEnPeriodo = function(){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "asigcursos",
				"Periodo": self.PeriodoSel.Id,
				"Cursos": self.CursosNoAsignados,
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
			};
			$http({
			method: 'POST',
			url: '../../services/cursocarrera.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo == 1){
					MensajeForm("Ok", "Cursos asignados exitosamente");
					ObtenerPeriodos(self);
          $('#mdAgregarCursos').modal('hide');
				}else{
					MensajeForm("Error", "Error asignando cursos, contacte al administrador");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}


		function ObtenerCursosSinAsignar(self){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "cursosnoasig",
				"IdCarrera": self.Carrera.IdCarrera
			};
			$http({
			method: 'POST',
			url: '../../services/cursocarrera.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				self.CursosNoAsignados = response.data.cursos;
				if(response.data.cursos.length==0){
					MensajeForm("Advertencia", "No se obtuvieron cursos sin asignar");
					$('#mdAgregarCursos').modal('hide');
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}


		function ObtenerPeriodos(self){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "periodos",
				"IdCarrera": self.Carrera.IdCarrera
			};
			$http({
			method: 'POST',
			url: '../../services/cursocarrera.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
        var _Respuesta = response.data.Respuesta;
				self.Periodos = _Respuesta.Periodos;
        self.Duracion = _Respuesta.Duracion;
				if(_Respuesta.Periodos==0){
					MensajeForm("Advertencia", "No se obtuvieron periodos");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

	}
]
});
