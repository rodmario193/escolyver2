

angular.module('formCarrera').component('formCarrera', {
    templateUrl: 'form-carrera/form-carrera.template.html',
    controller: ['$routeParams','$http',
	function formCarreraController($routeParams, $http) {
		var self = this;
		var strIdCarrera = $routeParams.idCarrera;
		self.Operacion = "";

		self.Carrera = {
			"IdCarrera": 0,
			"Nombre": "",
			"Grupo": "",
			"Descripcion":"",
			"DescripcionInterna":"",
			"Modalidad":"",
			"CostoMensual": 0,
			"CostoMatricula": 0,
			"CostoInscripcion": 0,
			"TelefonoFacultad": "",
			"NombreFacultad": "",
			"AplicaBeca": 0,
			"DetallesBeca": "",
			"ReqTesis": 0,
			"ReqPrivado": 0,
			"ReqMaestria": 0,
			"ReqCreditos": 0,
			"ReqEps": 0,
			"DetallesCierre": "",
			"HIniMat": "",
			"HFinMat": "",
			"HIniVesp": "",
			"HFinVesp": "",
			"HIniNoc": "",
			"HFinNoc": "",
			"HIniSab": "",
			"HFinSab": "",
			"HIniDom": "",
			"HFinDom": "",
			"DetallesNuevoIngreso":"",
			"DetallesReingreso":"",
			"DetallesContinuacion":"",
			"DetallesTraslado":"",
			"DetallesExtranjero":"",
			"DetallesPago":"",
			"DetallesPromociones":"",
      "CorreoFacultad": "", //marodriguez 20180422,
      "Certi_Internacional": 0, //marodriguez 20180422
      "DescripcionHorarios": "", //marodriguez 20180422,
      "ModalidadPresencial": 0, //marodriguez 20180422,
      "ModalidadSemiPresencial": 0, //marodriguez 20180422,
      "ModalidadEnLinea": 0, //marodriguez 20180422,
      "DescripcionModalidad": "" //marodriguez 20180422,
		}

		ObtenerAgrupaciones(self);

		/*
		self.Grupos = [
			{"Nombre":"Ingenieria en Ciencias y Sistemas"},
			{"Nombre":"Ingenieria Industrial"}
		];
		*/

		if(strIdCarrera=="$$NUEVO"){
			//Nueva Carrera
			self.Titulo = "Nueva Carrera";
			self.Operacion = "Agregar";
		}else{
			//Modificar Carrera
			self.Titulo = "Modificar Carrera";
			ObtenerDetalleCarrera(self, strIdCarrera);
			self.Operacion = "Modificar";
			$('#txtIdUniversidad').prop('disabled',true);
		}

		//Elige que tipo de operacion se ejecutara al momento de pulsar el boton "Guardar"
		self.OperacionForm = function(){
			if(self.Operacion == "Agregar"){
				AgregarCarrera(self);
			}else if(self.Operacion == "Modificar" ){
				ModificarCarrera(self);
			}
		}

		function ObtenerAgrupaciones(self){
			var jsonObject = {
				"Operacion": "grupos"
			};
			$http({
			method: 'POST',
			url: '../../services/carrera.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.grupos.length>0){
					self.Grupos = response.data.grupos;
				}else{
					MensajeForm("Error", "Error obteniendo agrupaciones, intente mas tarde");
					self.Bloquear = true;
				}
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
			});
		}

		function ObtenerDetalleCarrera(self, strIdCarrera){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "detalle",
				"IdCarrera": strIdCarrera,
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
			};
			$http({
			method: 'POST',
			url: '../../services/carrera.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					self.Carrera = response.data.respuesta.Carrera;
				}else{
					MensajeForm("Error", "Error obteniendo detalle de carrera, contacte al administrador");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

		function ModificarCarrera(self){
			if( (Vacio(self.Carrera.Nombre)) || (Vacio(self.Carrera.Grupo)) ){
				MensajeForm("Error", "Favor ingresar todos los valores obligatorios");
			}else if(!JornadaValida(self.Carrera)){
				MensajeForm("Error", "Verifique formato de horas de jornada ingresados");
      }else if( !ValidarCorreo(self.Carrera.CorreoFacultad) &&  self.Carrera.CorreoFacultad!="" ){ //Validar formato de correo electrónico
        MensajeForm("Error", "Revisar formato de correo electr&oacute;nico de facultad");
			}else{
				$('#mdLoading').modal('show');
				var jsonObject = {
					"Operacion": "modificar",
					"Carrera": self.Carrera,
					"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
				};
				$http({
				method: 'POST',
				url: '../../services/carrera.php',
				headers: {
					'Content-Type': 'application/json; charset=utf-8',
					'Data-Type': 'json'
				},
				data: jsonObject
				}).then(function(response, status){
					if(response.data.respuesta.codigo==1){
						MensajeForm("Ok","Carrera modificada exitosamente");
					}else if(response.data.respuesta.codigo==1062){
						MensajeForm("Error","El id de carrera ingresado ya existe, favor ingresar otro");
					}else{
						MensajeForm("Error", "Error modificando carrera, contacte al administrador");
					}
					$('#mdLoading').modal('hide');
				},	function(response){
					MensajeForm("Error", "Error de comunicacion, intente mas tarde");
					$('#mdLoading').modal('hide');
				});
			}
		}

		function AgregarCarrera(self){
			if( (Vacio(self.Carrera.Nombre)) || (Vacio(self.Carrera.Grupo)) ){
				MensajeForm("Error", "Favor ingresar todos los valores obligatorios");
			}else if(!JornadaValida(self.Carrera)){
				MensajeForm("Error", "Verifique formato de horas de jornada ingresados");
      }else if( !ValidarCorreo(self.Carrera.CorreoFacultad) &&  self.Carrera.CorreoFacultad!="" ){ //Validar formato de correo electrónico
        MensajeForm("Error", "Revisar formato de correo electr&oacute;nico de facultad");
			}else{
				$('#mdLoading').modal('show');
				var jsonObject = {
					"Operacion": "agregar",
					"Carrera": self.Carrera,
					"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
				};
				$http({
				method: 'POST',
				url: '../../services/carrera.php',
				headers: {
					'Content-Type': 'application/json; charset=utf-8',
					'Data-Type': 'json'
				},
				data: jsonObject
				}).then(function(response, status){
					if(response.data.respuesta.codigo==1){
						MensajeForm("Ok","Carrera agregada exitosamente");
					}else if(response.data.respuesta.codigo==1062){
						MensajeForm("Error","El id de carrera ingresado ya existe, favor ingresar otro");
					}else{
						MensajeForm("Error", "Error agregando carrera, contacte al administrador");
					}
					$('#mdLoading').modal('hide');
				},	function(response){
					MensajeForm("Error", "Error de comunicacion, intente mas tarde");
					$('#mdLoading').modal('hide');
				});
			}
		}

		function JornadaValida(Carrera){
			var _blnRespuesta = false;
			if(
				HorarioValido(Carrera.HIniMat) && HorarioValido(Carrera.HFinMat) &&
				HorarioValido(Carrera.HIniVesp) && HorarioValido(Carrera.HFinVesp) &&
				HorarioValido(Carrera.HIniNoc) && HorarioValido(Carrera.HFinNoc) &&
				HorarioValido(Carrera.HIniSab) && HorarioValido(Carrera.HFinSab) &&
				HorarioValido(Carrera.HIniDom) && HorarioValido(Carrera.HFinDom)
			){
				_blnRespuesta = true;
			}
			return _blnRespuesta;
		}

		function HorarioValido(strHora){
			var _blnRespuesta = false;
			if(!Vacio(strHora)){
				var _regexHora = /^[0-9][0-9]:[0-9][0-9]+$/;
				if(_regexHora.test(strHora)){
					_blnRespuesta = true;
				}else{
          console.log("Hora incorrecta => " + strHora);
        }
			}else{
				_blnRespuesta = true;
			}
			return _blnRespuesta;
		}


	}
]
});
