

angular.module('formOrientacion').component('formOrientacion', {
    templateUrl: 'form-orientacion/form-orientacion.template.html',
    controller: ['$routeParams','$http','Autenticacion',
	function formOrientacionController($routeParams, $http, Autenticacion) {
		Autenticacion.ValidarSesion();

		var self = this;
		self.idOrientacion = $routeParams.idOrientacion;
		
		self.FinalizarExamen = function(intIdExamen){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "finalizar",
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ===="),
				"IdOrientacion": self.idOrientacion,
				"NumExamen": intIdExamen
			};
			$http({
			method: 'POST',
			url: '../../services/orientacion.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.Codigo==1){
					MensajeForm("Ok", "Ex&aacute;men finalizado exitosamente");
					ObtenerDetalleOrientacion();
				}else{
					MensajeForm("Error", response.data.respuesta.Mensaje);
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}
		
		self.EnviarEnlaceExamen = function(intIdExamen){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "correo",
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ===="),
				"IdOrientacion": self.idOrientacion,
				"NumExamen": intIdExamen
			};
			$http({
			method: 'POST',
			url: '../../services/orientacion.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.Codigo==1){
					MensajeForm("Ok", "Correo con enlace de ex&aacute;men enviado exitosamente");
					ObtenerDetalleOrientacion();
				}else{
					MensajeForm("Error", response.data.respuesta.Mensaje);
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}
		
		
		ObtenerGrupos();
		ObtenerDetalleOrientacion();
		
		function ObtenerDetalleOrientacion(){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "detalle",
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ===="),
				"IdOrientacion": self.idOrientacion
			};
			$http({
			method: 'POST',
			url: '../../services/orientacion.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					self.Orientacion = response.data.respuesta.orientacion;
				}else{
					MensajeForm("Error", "Error obteniento detalle de orientaci&oacute;n");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}
		
		function ObtenerGrupos(){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "lista_grupos",
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
			};
			$http({
			method: 'POST',
			url: '../../services/orientacion.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				var Respuesta = response.data.respuesta
				if(Respuesta.Codigo==1){
					self.Grupos = Respuesta.Grupos;
				}else{
					MensajeForm("Error", Respuesta.Mensaje);
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}
		
		self.ActualizarGrupos = function(){
			
			if( Vacio(self.Orientacion.Grupo1) || Vacio(self.Orientacion.Grupo2) || Vacio(self.Orientacion.Grupo3) ){
				MensajeForm("Error", "Debe seleccionar los 3 grupos para asignar a la orientaci&oacute;n");
			}else{
				$('#mdLoading').modal('show');
				var jsonObject = {
					"Operacion": "act_grupos",
					"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ===="),
					"Grupo1": self.Orientacion.Grupo1,
					"Grupo2": self.Orientacion.Grupo2,
					"Grupo3": self.Orientacion.Grupo3,
					"IdOrientacion": self.idOrientacion
				};
				$http({
				method: 'POST',
				url: '../../services/orientacion.php',
				headers: {
					'Content-Type': 'application/json; charset=utf-8',
					'Data-Type': 'json'
				},
				data: jsonObject
				}).then(function(response, status){
					var Respuesta = response.data.respuesta
					if(Respuesta.Codigo==1){
						MensajeForm("Ok", "Grupos de orientaci&oacute;n actualizados exitosamente");
					}else{
						MensajeForm("Error", Respuesta.Mensaje);
					}
					$('#mdLoading').modal('hide');
				},	function(response){
					MensajeForm("Error", "Error de comunicacion, intente mas tarde");
					$('#mdLoading').modal('hide');
				});
			}
			
		}

		

	}
]
});


