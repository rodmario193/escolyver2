

angular.module('formUniversidad').component('formUniversidad', {
    templateUrl: 'form-universidad/form-universidad.template.html',
    controller: ['$routeParams','$http', 'Autenticacion',
	function formUniversidadController($routeParams, $http, Autenticacion) {
		Autenticacion.ValidarSesion();

		var self = this;
		//Obtiene parametro de Id Universidad enviado en URL
		var strIdUniversidad = $routeParams.idUniversidad;
		self.Operacion = "";

		//Inicializar objeto de Universidad
		self.Universidad = {
			"IdUniversidad": "",
			"Nombre": "",
			"Departamento": "",
			"Municipio": "",
			"Localidad": "",
			"Zona": 0,
			"Telefono1": "",
			"Telefono2": "",
			"Url": "",
			"DetalleInscripcion": "",
			"CostoParqueo": 0,
			"Disponibilidad": 0,
			"DetalleParqueo": "",
			"Imagen":"",
			"InfoAdicional":""
		};


		if(strIdUniversidad=="$$NUEVO"){
			//Nueva Universidad
			self.Titulo = "Nueva Universidad";
			self.Operacion = "Agregar";
		}else{
			//Modificar Universidad
			self.Titulo = "Modificar Universidad";
			ObtenerDetalleUniversidad(self, strIdUniversidad);
			self.Operacion = "Modificar";
			$('#txtIdUniversidad').prop('disabled',true);
		}

		ObtenerDepartamentos(self);

		self.CambioDepartamento = function(){
			ObtenerMunicipios(self);
		}

		//Elige que tipo de operacion se ejecutara al momento de pulsar el boton "Guardar"
		self.OperacionForm = function(){
			if(self.Operacion == "Agregar"){
				AgregarUniversidad(self);
			}else if(self.Operacion == "Modificar" ){
				ModificarUniversidad(self);
			}
		}

		function ObtenerMunicipios(self){
			var jsonObject = {
				"Operacion": "municipios",
				"IdDepartamento": self.Universidad.Departamento
			};
			$http({
			method: 'POST',
			url: '../../services/universidad.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.municipios.length>0){
					self.Municipios = response.data.municipios;
				}else{
					MensajeForm("Error", "Error obteniendo municipios, intente mas tarde");
					self.Bloquear = true;
				}
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
			});
		}

		function ObtenerDepartamentos(self){
			var jsonObject = {
				"Operacion": "departamentos"
			};
			$http({
			method: 'POST',
			url: '../../services/universidad.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.departamentos.length>0){
					self.Departamentos = response.data.departamentos;
				}else{
					MensajeForm("Error", "Error obteniendo departamentos, intente mas tarde");
					self.Bloquear = true;
				}
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
			});
		}

		//Funcion para modificar la universidad seleccionada
		function ModificarUniversidad(self){
			if( (Vacio(self.Universidad.Nombre)) ||
				(Vacio(self.Universidad.Departamento)) || (Vacio(self.Universidad.Municipio)) ){
				MensajeForm("Error", "Favor ingresar todos los valores obligatorios");
			}else{
				$('#mdLoading').modal('show');
				var _nuevaImagen = localStorage.getItem("imagen");
				if( !Vacio(_nuevaImagen) ) {
					self.Universidad.Imagen = _nuevaImagen;
				}
				var jsonObject = {
					"Operacion": "modificar",
					"Universidad": self.Universidad,
					"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
				};
				$http({
				method: 'POST',
				url: '../../services/universidad.php',
				headers: {
					'Content-Type': 'application/json; charset=utf-8',
					'Data-Type': 'json'
				},
				data: jsonObject
				}).then(function(response, status){
					if(response.data.respuesta.codigo==1){
						MensajeForm("Ok","Universidad modificada exitosamente");
					}else{
						MensajeForm("Error", "Error modificando universidad, contacte al administrador");
					}
					$('#mdLoading').modal('hide');
				},	function(response){
					MensajeForm("Error", "Error de comunicacion, intente mas tarde");
					$('#mdLoading').modal('hide');
				});
			}
		}

		//Funcion para agregar nueva universidad
		function AgregarUniversidad(self){
			if( (Vacio(self.Universidad.Nombre)) ||
				(Vacio(self.Universidad.Departamento)) || (Vacio(self.Universidad.Municipio)) ){
				MensajeForm("Error", "Favor ingresar todos los valores obligatorios");
      }else if( !Vacio(self.Universidad.Localidad) && (self.Universidad.Localidad.length > 200)  ){
        MensajeForm("Error", "La direcci&oacute;n tiene un m&aacute;ximo de 200 car&aacute;cteres");
			}else{
				$('#mdLoading').modal('show');
				var _nuevaImagen = localStorage.getItem("imagen");
				if( !Vacio(_nuevaImagen) ) {
					self.Universidad.Imagen = _nuevaImagen;
				}
				var jsonObject = {
					"Operacion": "agregar",
					"Universidad": self.Universidad,
					"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
				};
				$http({
				method: 'POST',
				url: '../../services/universidad.php',
				headers: {
					'Content-Type': 'application/json; charset=utf-8',
					'Data-Type': 'json'
				},
				data: jsonObject
				}).then(function(response, status){
					if(response.data.respuesta.codigo==1){
						MensajeForm("Ok","Universidad agregada exitosamente");
					}else if(response.data.respuesta.codigo==1062){
						MensajeForm("Error","El id de universidad ingresado ya existe, favor ingresar otro");
					}else{
						MensajeForm("Error", "Error agregando universidad, contacte al administrador");
					}
					$('#mdLoading').modal('hide');
				},	function(response){
					MensajeForm("Error", "Error de comunicacion, intente mas tarde");
					$('#mdLoading').modal('hide');
				});
			}
		}

		function ObtenerDetalleUniversidad(self, strIdUniversidad){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "detalle",
				"IdUniversidad": strIdUniversidad,
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
			};
			$http({
			method: 'POST',
			url: '../../services/universidad.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					self.Universidad = response.data.respuesta.Universidad;
					$('#imgUsuario').attr('src',self.Universidad.Imagen);
					ObtenerMunicipios(self);
				}else{
					MensajeForm("Error", "Error obteniendo detalle de universidad, contacte al administrador");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

	}
]
});
