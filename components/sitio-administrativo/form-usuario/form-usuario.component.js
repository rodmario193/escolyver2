

angular.module('formUsuario').component('formUsuario', {
    templateUrl: 'form-usuario/form-usuario.template.html',
    controller: ['$routeParams','$http','Autenticacion',
	function formUsuarioController($routeParams, $http, Autenticacion) {
		Autenticacion.ValidarSesion();

		var self = this;
		var _strIdUsuario = $routeParams.idUsuario;
		self.IdUsuario = _strIdUsuario;
		self.Titulo = "Datos de usuario " + _strIdUsuario;
		self.Usuario = {
			"IdUsuario": "",
			"PrimerNombre": "",
			"SegundoNombre": "",
			"PrimerApellido": "",
			"SegundoApellido": "",
			"Genero": "",
			"FechaNacimiento": "",
			"Correo": "",
			"TelefonoCelular": "",
			"Departamento": 0,
			"Municipio": 0,
			"Direccion": "",
			"Instituto": "",
			"NombreFacturacion": "",
			"ApellidoFacturacion": "",
			"Nit": "",
			"DepartamentoFacturacion": 0,
			"Imagen": "",
      "RedSocial": "",
      "Medio": ""
		};
		ObtenerDepartamentos(self);
		ObtenerDetalleUsuario(self);

		function ObtenerDetalleUsuario(self){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "detalle_admin",
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ===="),
				"IdUsuario": _strIdUsuario
			};
			$http({
			method: 'POST',
			url: '../../services/usuario.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo == 1){
					self.Usuario = response.data.respuesta.usuario;
					ObtenerMunicipios(self);
				}else{
					MensajeForm("Error", "Error obteniendo informaci&oacute;n de usuario");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

		self.CambioDepartamento = function(){
			ObtenerMunicipios(self);
		}

		function ObtenerMunicipios(self){
			//$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "municipios",
				"IdDepartamento": self.Usuario.Departamento
			};
			$http({
			method: 'POST',
			url: '../../services/universidad.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.municipios.length>0){
					self.Municipios = response.data.municipios;
				}else{
					MensajeForm("Error", "Error obteniendo municipios, intente mas tarde");
					self.Bloquear = true;
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

		function ObtenerDepartamentos(self){
			var jsonObject = {
				"Operacion": "departamentos"
			};
			$http({
			method: 'POST',
			url: '../../services/universidad.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.departamentos.length>0){
					self.Departamentos = response.data.departamentos;
					self.DeptsFacturacion =  response.data.departamentos;
				}else{
					MensajeForm("Error", "Error obteniendo departamentos, intente mas tarde");
					self.Bloquear = true;
				}
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
			});
		}





	}
]
});
