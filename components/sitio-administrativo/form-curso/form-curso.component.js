

angular.module('formCurso').component('formCurso', {
    templateUrl: 'form-curso/form-curso.template.html',
    controller: ['$routeParams','$http','Autenticacion',
	function formCursoController($routeParams, $http, Autenticacion) {
		
		Autenticacion.ValidarSesion();
		
		var self = this;
		var strIdCurso = $routeParams.idCurso;
		self.Operacion = "";

		
		if(strIdCurso=="$$NUEVO"){
			//Nueva Universidad
			self.Titulo = "Nuevo Curso";
			self.Operacion = "Agregar";
		}else{
			//Modificar Universidad
			self.Titulo = "Modificar Curso";
			ObtenerDetalleCurso(self, strIdCurso);
			self.Operacion = "Modificar";
		}
		
		self.Curso = {
			"IdCurso": 0,
			"Nombre": "",
			"Descripcion": "",
			"Categoria": ""
		}
		
		self.OperacionForm = function(){
			if(self.Operacion == "Agregar"){
				AgregarCurso(self);
			}else if(self.Operacion == "Modificar" ){
				ModificarCurso(self);
			}
		}
		
		function ModificarCurso(self){
			if( (Vacio(self.Curso.Nombre)) ){
				MensajeForm("Error", "Favor ingresar todos los valores obligatorios");
			}else{
				$('#mdLoading').modal('show');
				var jsonObject = {
					"Operacion": "modificar",
					"Curso": self.Curso,
					"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
				};
				$http({
				method: 'POST',
				url: '../../services/curso.php',
				headers: {
					'Content-Type': 'application/json; charset=utf-8',
					'Data-Type': 'json'
				},
				data: jsonObject
				}).then(function(response, status){
					if(response.data.respuesta.codigo==1){
						MensajeForm("Ok","Curso modificado exitosamente");
					}else{
						MensajeForm("Error", "Error modificando curso, contacte al administrador");
					}
					$('#mdLoading').modal('hide');
				},	function(response){
					MensajeForm("Error", "Error de comunicacion, intente mas tarde");
					$('#mdLoading').modal('hide');
				});
			}
		}
		
		function ObtenerDetalleCurso(self, strIdCurso){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "detalle",
				"IdCurso": strIdCurso,
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
			};
			$http({
			method: 'POST',
			url: '../../services/curso.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					self.Curso = response.data.respuesta.Curso;
				}else{
					MensajeForm("Error", "Error obteniendo detalle de curso, contacte al administrador");
				}	
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}
		
		function AgregarCurso(self){
			if( (Vacio(self.Curso.Nombre)) ){
				MensajeForm("Error", "Favor ingresar todos los valores obligatorios");
			}else{
				$('#mdLoading').modal('show');
				var jsonObject = {
					"Operacion": "agregar",
					"Curso": self.Curso,
					"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
				};
				$http({
				method: 'POST',
				url: '../../services/curso.php',
				headers: {
					'Content-Type': 'application/json; charset=utf-8',
					'Data-Type': 'json'
				},
				data: jsonObject
				}).then(function(response, status){
					if(response.data.respuesta.codigo==1){
						MensajeForm("Ok","Curso agregado exitosamente");
					}else{
						MensajeForm("Error", "Error agregando curso, contacte al administrador");
					}
					$('#mdLoading').modal('hide');
				},	function(response){
					MensajeForm("Error", "Error de comunicacion, intente mas tarde");
					$('#mdLoading').modal('hide');
				});
			}
		}
		
		
	}
]
});


