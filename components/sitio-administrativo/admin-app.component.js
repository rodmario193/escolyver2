angular.module('UniT').controller('AdminController', function ($scope) {
    $scope.usuario = localStorage.getItem("jLrUs6d50WBXoEfgTWoseg====");

    $scope.Logout = function () {
        localStorage.removeItem("jLrUs6d50WBXoEfgTWoseg====");
        localStorage.removeItem("t3fDLyrCu8+AeTfJh1U9iQ====");
        window.location = "admin-login.html";
    }
});

angular.module('UniT').service('Autenticacion', ['$http',
    function ($http) {

        this.ValidarSesion = function () {
            $('#dvMensajeForm').addClass('hidden'); //Fix temporal para ocultar mensajes de éxito/rechazo al cambiar de pantalla
            strToken = localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====");
            if (Vacio(strToken)) {
                localStorage.removeItem("jLrUs6d50WBXoEfgTWoseg====");
                localStorage.removeItem("t3fDLyrCu8+AeTfJh1U9iQ====");
                window.location = "admin-login.html";
            } else {
                //$('#mdLoading').modal('show');
                var jsonObject = {
                    "Token": strToken,
                    "Operacion": "validar_admin"
                };
                $http({
                    method: 'POST',
                    url: '../../services/login.php',
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Data-Type': 'json'
                    },
                    data: jsonObject
                }).then(function (response, status) {
                    if (response.data.valido != 1) {
                        localStorage.removeItem("jLrUs6d50WBXoEfgTWoseg====");
                        localStorage.removeItem("t3fDLyrCu8+AeTfJh1U9iQ====");
                        window.location = "admin-login.html";
                    }
                }, function (response) {
                    //MensajeForm("Error", "Error de comunicaci&oacute;n, intente mas tarde");
                    localStorage.removeItem("jLrUs6d50WBXoEfgTWoseg====");
                    localStorage.removeItem("t3fDLyrCu8+AeTfJh1U9iQ====");
                    window.location = "admin-login.html";
                });
            }

        }


    }
]);
