

angular.module('detalleregOrientacion').component('detalleregOrientacion', {
    templateUrl: 'detallereg-orientacion/detallereg-orientacion.template.html',
    controller: ['$routeParams','$http','Autenticacion',
	function detalleregOrientacionController($routeParams, $http, Autenticacion) {

		Autenticacion.ValidarSesion();

		var self = this;
    self.Orientacion = {
      "Id": $routeParams.idOrientacion,
      "Actividad1": "",
      "Actividad2": "",
      "Actividad3": "",
      "Actividad4": "",
      "Actividad5": "",
      "Actividad6": "",
      "Objeto1": "",
      "Objeto2": "",
      "Objeto3": "",
      "Objeto4": "",
      "Objeto5": "",
      "Objeto6": "",
      "Logro1": "",
      "Logro2": "",
      "Logro3": "",
      "Logro4": "",
      "Pregunta1Opcion1": 0,
      "Pregunta1Opcion2": 0,
      "Pregunta1Opcion3": 0,
      "Pregunta1Opcion4": 0,
      "Pregunta2Opcion1": 0,
      "Pregunta2Opcion2": 0,
      "Pregunta2Opcion3": 0,
      "Pregunta2Opcion4": 0,
      "Pregunta3Opcion1": 0,
      "Pregunta3Opcion2": 0,
      "Pregunta3Opcion3": 0,
      "Pregunta3Opcion4": 0,
      "Pregunta3Opcion5": 0,
      "Pregunta3Opcion6": 0,
      "Pregunta3Opcion7": 0,
      "EstudioMama": "",
      "GraduacionMama": 0,
      "TrabajoMama": "",
      "EstudioPapa": "",
      "GraduacionPapa": 0,
      "TrabajoPapa": "",
      "EstudiosHermanos1": "",
      "EstudiosHermanos2": "",
      "EstudiosHermanos3": "",
      "EstudiosHermanos4": "",
      "EstudiosFamiliares1": "",
      "EstudiosFamiliares2": "",
      "EstudiosFamiliares3": "",
      "EstudiosFamiliares4": "",
      "EstudiosFamiliares5": "",
      "EstudiosFamiliares6": ""
    };

    DetalleRegOrientacion(self);

    self.VerDetalleOrientacion = function(){
        window.location = "user-main.html#!/detalle-orientacion/" + self.Orientacion.Id;
    }

    function DetalleRegOrientacion(self){
        $('#mdLoading').modal('show');
        var jsonObject = {
          "Operacion": "detallereg",
          "Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ===="),
          "IdOrientacion": self.Orientacion.Id
        };
        $http({
        method: 'POST',
        url: '../../services/orientacion.php',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Data-Type': 'json'
        },
        data: jsonObject
        }).then(function(response, status){
          var _Respuesta = response.data.Respuesta;
          if(_Respuesta.Exito){
            self.Orientacion = _Respuesta.Orientacion;
          }else{
            MensajeForm("Error", _Respuesta.Mensaje);
          }
          $('#mdLoading').modal('hide');
        },	function(response){
          MensajeForm("Error", "Error de comunicacion, intente mas tarde");
        });
    }

	}
]
});
