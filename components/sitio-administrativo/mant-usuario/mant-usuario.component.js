

angular.module('mantUsuario').component('mantUsuario', {
    templateUrl: 'mant-usuario/mant-usuario.template.html',
    controller: ['$http', 'Autenticacion',
	function mantUsuarioController($http, Autenticacion) {
		Autenticacion.ValidarSesion();
		var self = this;
		
		self.VerDetalleUsuario = function(Usuario){
			window.location = "admin-main.html#!/form-usuario/" + Usuario.Id;
		}
		
		self.VerOrientaciones = function(Usuario){
			window.location = "admin-main.html#!/mant-orientacion/" + Usuario.Id;
		}
		
		ObtenerUsuarios();

		function ObtenerUsuarios(){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "lista",
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
			};
			$http({
			method: 'POST',
			url: '../../services/usuario.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				self.Usuarios = response.data.respuesta.usuarios;
				if(response.data.respuesta.usuarios.length==0){
					MensajeForm("Error", "No se obtuvieron usuarios");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}
	}
]
});


