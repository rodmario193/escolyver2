

angular.module('formAgrupcarreras').component('formAgrupcarreras', {
    templateUrl: 'form-agrupcarreras/form-agrupcarreras.template.html',
    controller: ['$routeParams','$http',  'Autenticacion',
	function formAgrupcarrerasController($routeParams, $http, Autenticacion) {
		
		Autenticacion.ValidarSesion();
		
		var self = this;
		/*
		self.Agrupacion = {
			"idAgrupacion": $routeParams.idAgrupacion,
			"Nombre": "Ingenieria en Sistemas",
			"Descripcion": "Abarca las carreras tanto de Ingenieria como de Licenciatura en Sistemas",
			"Tipo":"1"
		}
		*/
		self.Agrupacion = {
			"Id":0,
			"Nombre": "",
			"Descripcion": "",
			"TipoGrupo":0,
			"Prueba": 0
		}
		
		self.Bloquear = false;
		/*
		self.TiposGrupo = [
			{ IdTipoGrupo: 1, "Descripcion" : "Ingenieria" },
			{ IdTipoGrupo: 2, "Descripcion" : "Doctorado" },
			{ IdTipoGrupo: 3, "Descripcion" : "Diplomado" },
			{ IdTipoGrupo: 4, "Descripcion" : "Licenciatura" }
		]
		*/
		
		var strIdAgrupacion = $routeParams.idAgrupacion;
		
		if(strIdAgrupacion=="$$NUEVO"){
			//Nueva Universidad
			self.Titulo = "Nueva Agrupacion";
			self.Operacion = "Agregar";
		}else{
			//Modificar Universidad
			self.Titulo = "Modificar Agrupacion";
			ObtenerDetalleAgrupacion(self, strIdAgrupacion);
			self.Operacion = "Modificar";
			//$('#txtNombreAgrupacion').prop('disabled',true);
		}
		
		ObtenerTiposGrupo(self);
		
		self.OperacionForm = function(){
			if(self.Operacion == "Agregar"){
				AgregarAgrupacion(self);
			}else if(self.Operacion == "Modificar" ){
				ModificarAgrupacion(self);
			}
		}
		
		function ModificarAgrupacion(self){
			if( (Vacio(self.Agrupacion.Nombre)) || (Vacio(self.Agrupacion.TipoGrupo)) ){
				MensajeForm("Error", "Favor ingresar todos los valores obligatorios");
			}else{
				$('#mdLoading').modal('show');
				var jsonObject = {
					"Operacion": "modificar",
					"Agrupacion": self.Agrupacion,
					"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
				};
				$http({
				method: 'POST',
				url: '../../services/grupo.php',
				headers: {
					'Content-Type': 'application/json; charset=utf-8',
					'Data-Type': 'json'
				},
				data: jsonObject
				}).then(function(response, status){
					if(response.data.respuesta.codigo==1){
						MensajeForm("Ok","Agrupacion modificada exitosamente");
					}else{
						MensajeForm("Error", "Error modificando agrupacion, contacte al administrador");
					}
					$('#mdLoading').modal('hide');
				},	function(response){
					MensajeForm("Error", "Error de comunicacion, intente mas tarde");
					$('#mdLoading').modal('hide');
				});
			}
		}
		
		
		function AgregarAgrupacion(self){
			if( (Vacio(self.Agrupacion.Nombre)) || (Vacio(self.Agrupacion.TipoGrupo)) ){
				MensajeForm("Error", "Favor ingresar todos los valores obligatorios");
			}else{
				$('#mdLoading').modal('show');
				var jsonObject = {
					"Operacion": "agregar",
					"Agrupacion": self.Agrupacion,
					"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
				};
				$http({
				method: 'POST',
				url: '../../services/grupo.php',
				headers: {
					'Content-Type': 'application/json; charset=utf-8',
					'Data-Type': 'json'
				},
				data: jsonObject
				}).then(function(response, status){
					if(response.data.respuesta.codigo==1){
						MensajeForm("Ok","Agrupacion agregada exitosamente");
					}else if(response.data.respuesta.codigo==1062){
						MensajeForm("Error","El nombre de agrupacion ingresado ya existe, favor ingresar otro");
					}else{
						MensajeForm("Error", "Error agregando agrupacion, contacte al administrador");
					}
					$('#mdLoading').modal('hide');
				},	function(response){
					MensajeForm("Error", "Error de comunicacion, intente mas tarde");
					$('#mdLoading').modal('hide');
				});
			}
		}
		
		function ObtenerTiposGrupo(self){
			var jsonObject = {
				"Operacion": "tiposgrupo"
			};
			$http({
			method: 'POST',
			url: '../../services/grupo.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.tiposgrupo.length>0){
					self.TiposGrupo = response.data.tiposgrupo;
				}else{
					MensajeForm("Error", "Error obteniendo tipos de agrupacion, intente mas tarde");
					self.Bloquear = true;
				}
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
			});
		}
		
		function ObtenerDetalleAgrupacion(self, strIdAgrupacion){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "detalle",
				"IdAgrupacion": strIdAgrupacion,
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
			};
			$http({
			method: 'POST',
			url: '../../services/grupo.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					self.Agrupacion = response.data.respuesta.Agrupacion;
				}else{
					MensajeForm("Error", "Error obteniendo detalle de agrupacion, contacte al administrador");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}
		
		
	}
]
});


