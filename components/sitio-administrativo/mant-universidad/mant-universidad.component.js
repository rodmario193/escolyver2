

angular.module('mantUniversidad').component('mantUniversidad', {
    templateUrl: 'mant-universidad/mant-universidad.template.html',
    controller: ['$http', 'Autenticacion',
	function mantUniversidadController($http, Autenticacion) {
		
		Autenticacion.ValidarSesion();
		
		var self = this;
		
		//ValidarLogin();
		
		self.ModificarUniversidad = function(Universidad){
			window.location = "admin-main.html#!/form-universidad/" + Universidad.Id;
		}
		
		self.ModCarrerasUniversidad = function(Universidad){
			window.location = "admin-main.html#!/mant-carrerauni/" + Universidad.Id + "/" + Universidad.Nombre;
		}
		
		self.VerModalEliminar = function(Universidad){
			$('#mdConfEliminar').modal('show');
			self.UniversidadEliminar = Universidad;
		}
		
		self.EliminarUniversidad =  function(){
			$('#mdConfEliminar').modal('hide');
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "eliminar",
				"IdUniversidad": self.UniversidadEliminar.Id,
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
			};
			$http({
			method: 'POST',
			url: '../../services/universidad.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					MensajeForm("Ok","Universidad eliminada exitosamente");
					ObtenerUniversidades();
				}else{
					MensajeForm("Error", "Error eliminando universidad, contacte al administrador");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}
		
		
		ObtenerUniversidades();
		
		function ObtenerUniversidades(){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "lista"
			};
			$http({
			method: 'POST',
			url: '../../services/universidad.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				self.Universidades = response.data.universidades;
				if(response.data.universidades.length==0){
					MensajeForm("Error", "No se obtuvieron universidades");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}
	}
]
});


