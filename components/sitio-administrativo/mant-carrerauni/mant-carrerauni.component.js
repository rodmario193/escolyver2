

angular.module('mantCarrerauni').component('mantCarrerauni', {
    templateUrl: 'mant-carrerauni/mant-carrerauni.template.html',
    controller: ['$routeParams','$http','Autenticacion',
	function mantCarreraUniController($routeParams, $http, Autenticacion) {
		Autenticacion.ValidarSesion();
		var self = this;

		self.Universidad = {
			"IdUniversidad": $routeParams.idUniversidad,
			"Nombre": $routeParams.nombreUniversidad
		};

		ObtenerCarreras(self);

		self.VerModalAgregarCarreras = function(){
			ObtenerCarrerasSinAsignar(self);
			$('#mdAgregarCarreras').modal('show');
		}

		self.VerModalEliminarCarrera = function(Carrera){
			self.CarreraEliminar = Carrera;
			$('#mdConfEliminar').modal('show');
		}

		self.EliminarCarrera = function(){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "elimcarrera",
				"IdUniversidad": self.Universidad.IdUniversidad,
				"IdCarrera": self.CarreraEliminar.Id,
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
			};
			$http({
			method: 'POST',
			url: '../../services/carrerauni.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo == 1){
					MensajeForm("Ok", "Carrera eliminada exitosamente");
					$('#mdConfEliminar').modal('hide');
					ObtenerCarreras(self);
				}else{
					MensajeForm("Error", "Error eliminando carrera, contacte al administrador");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

		self.AsignarCarreras = function(){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "asigcarreras",
				"IdUniversidad": self.Universidad.IdUniversidad,
				"Carreras": self.CarrerasNoAsignadas,
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
			};
			$http({
			method: 'POST',
			url: '../../services/carrerauni.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo == 1){
					MensajeForm("Ok", "Carreras asignadas exitosamente");
					$('#mdAgregarCarreras').modal('hide');
					ObtenerCarreras(self);
				}else{
					MensajeForm("Error", "Error asignando carreras, contacte al administrador");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			}).finally( function() {
				$('#mdAgregarCarreras').modal('hide');
			});
		}

		function ObtenerCarrerasSinAsignar(self){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "carrerasnoasig",
        "IdUniversidad": self.Universidad.IdUniversidad
			};
			$http({
			method: 'POST',
			url: '../../services/carrerauni.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				self.CarrerasNoAsignadas = response.data.carreras;
				if(response.data.carreras.length==0){
					MensajeForm("Advertencia", "No se obtuvieron carreras sin asignar");
					$('#mdAgregarCarreras').modal('hide');
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}


		function ObtenerCarreras(self){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "carreras",
				"IdUniversidad": self.Universidad.IdUniversidad
			};
			$http({
			method: 'POST',
			url: '../../services/carrerauni.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				self.Carreras = response.data.carreras;
				if(response.data.carreras.length==0){
					MensajeForm("Advertencia", "No se obtuvieron carreras asignadas para esta universidad");
				}
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
			}).finally( function() {
				$('#mdLoading').modal('hide');
			});
		}

		/*
		self.CarrerasNoAsignadas = [
			{ Id: "UMG101", Nombre: "Ingenieria en Sistemas", Tipo: "Profesional", Seleccionada:0 },
			{ Id: "USAC102", Nombre: "Ingenieria Industrial", Tipo: "Profesional",Seleccionada:0 },
			{ Id: "USAC202", Nombre: "Administracion de Empresas", Tipo: "Profesional",Seleccionada:0 },
			{ Id: "UMG102", Nombre: "Ingenieria Industrial", Tipo: "Profesional",Seleccionada:0 }
		];
		*/


	}
]
});
