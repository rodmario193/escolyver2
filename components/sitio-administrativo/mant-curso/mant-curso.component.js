

angular.module('mantCurso').component('mantCurso', {
    templateUrl: 'mant-curso/mant-curso.template.html',
    controller: ['$http', 'Autenticacion',
	function mantCursoController($http, Autenticacion) {
		
		Autenticacion.ValidarSesion();
		
		var self = this;
		
		self.ModificarCurso = function(Curso){
			window.location = "admin-main.html#!/form-curso/" + Curso.Id;
		}
		
		
		self.VerModalEliminar = function(Curso){
			$('#mdConfEliminar').modal('show');
			self.CursoEliminar = Curso;
		}
		
		self.EliminarCurso =  function(){
			$('#mdConfEliminar').modal('hide');
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "eliminar",
				"IdCurso": self.CursoEliminar.Id,
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
			};
			$http({
			method: 'POST',
			url: '../../services/curso.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					MensajeForm("Ok","Curso eliminado exitosamente");
					ObtenerCursos();
				}else{
					MensajeForm("Error", "Error eliminando curso, contacte al administrador");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}
		
		ObtenerCursos();
		
		function ObtenerCursos(){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "lista"
			};
			$http({
			method: 'POST',
			url: '../../services/curso.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				self.Cursos = response.data.cursos;
				if(response.data.cursos.length==0){
					MensajeForm("Advertencia", "No se obtuvieron cursos");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}
		
	}
]
});


