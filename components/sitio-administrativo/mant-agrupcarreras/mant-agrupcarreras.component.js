

angular.module('mantAgrupcarreras').component('mantAgrupcarreras', {
    templateUrl: 'mant-agrupcarreras/mant-agrupcarreras.template.html',
    controller: ['$http', 'Autenticacion',
	function mantAgrupcarrerasController($http, Autenticacion) {
		
		Autenticacion.ValidarSesion();
		
		var self = this;
		
		self.ModificarAgrupacion = function(Agrupacion){
			window.location = "admin-main.html#!/form-agrupcarreras/" + Agrupacion.Id;
		}

		self.VerModalEliminar = function(Agrupacion){
			$('#mdConfEliminar').modal('show');
			self.GrupoAEliminar = Agrupacion;
		}
		
		self.ElminarAgrupacion =  function(){
			$('#mdConfEliminar').modal('hide');
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "eliminar",
				"IdGrupo": self.GrupoAEliminar.Id,
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
			};
			$http({
			method: 'POST',
			url: '../../services/grupo.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					MensajeForm("Ok","Agrupacion eliminada exitosamente");
					ObtenerAgrupaciones();
				}else{
					MensajeForm("Error", "Error eliminando agrupacion, contacte al administrador");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}
		
		ObtenerAgrupaciones();
		
		function ObtenerAgrupaciones(){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "lista"
			};
			$http({
			method: 'POST',
			url: '../../services/grupo.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				self.Agrupaciones = response.data.agrupaciones;
				if(response.data.agrupaciones.length==0){
					MensajeForm("Advertencia", "No se obtuvieron agrupaciones de carreras");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}
		
	}
]
});


