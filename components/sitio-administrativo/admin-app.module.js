
angular.module('UniT', [
'ngRoute',
'mantUniversidad',
'formUniversidad',
'mantCarrera',
'formCarrera',
'mantCurso',
'formCurso',
'mantCarrerauni',
'mantCursocarrera',
'mantUsuario',
'formUsuario',
'mantAgrupcarreras',
'formAgrupcarreras',
'mantOrientacion',
'formOrientacion',
'detalleregOrientacion'
]);
