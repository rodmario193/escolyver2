
 angular.
  module('UniT').
  config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');


      $routeProvider.
        when('/admin', {
			templateUrl: "admin-ini.html"
        }).
        when('/mant-universidad', {
			template: '<mant-universidad></mant-universidad>'
        }).
		when('/form-universidad/:idUniversidad', {
			template: '<form-universidad></form-universidad>'
        }).
		when('/mant-carrera', {
			template: '<mant-carrera></mant-carrera>'
        }).
		when('/form-carrera/:idCarrera', {
			template: '<form-carrera></form-carrera>'
        }).
		when('/mant-curso', {
			template: '<mant-curso></mant-curso>'
        }).
		when('/form-curso/:idCurso', {
			template: '<form-curso></form-curso>'
        }).
		when('/mant-usuario', {
			template: '<mant-usuario></mant-usuario>'
        }).
		when('/form-usuario/:idUsuario', {
			template: '<form-usuario></form-usuario>'
        }).
		when('/mant-carrerauni/:idUniversidad/:nombreUniversidad', {
			template: '<mant-carrerauni></mant-carrerauni>'
        }).
		when('/mant-cursocarrera/:idCarrera/:NombreCarrera', {
			template: '<mant-cursocarrera></mant-cursocarrera>'
        }).
		when('/mant-agrupcarreras', {
			template: '<mant-agrupcarreras></mant-agrupcarreras>'
        }).
		when('/form-agrupcarreras/:idAgrupacion', {
			template: '<form-agrupcarreras></form-agrupcarreras>'
        }).
		when('/mant-orientacion/:idUsuario', {
			template: '<mant-orientacion></mant-orientacion>'
        }).
		when('/form-orientacion/:idOrientacion', {
			template: '<form-orientacion></form-orientacion>'
        }).
    when('/detallereg-orientacion/:idOrientacion', {
    			template: '<detallereg-orientacion></detallereg-orientacion>'
    		}).
        otherwise('/admin');
    }
  ]);
