

angular.module('mantOrientacion').component('mantOrientacion', {
    templateUrl: 'mant-orientacion/mant-orientacion.template.html',
    controller: ['$routeParams','$http', 'Autenticacion',
	function mantOrientacionController($routeParams, $http, Autenticacion) {

		Autenticacion.ValidarSesion();

		var self = this;
		self.IdUsuario = $routeParams.idUsuario;

		self.ModificarOrientacion = function(Orientacion){
			window.location = "admin-main.html#!/form-orientacion/" + Orientacion.Id.toString();
		}

    self.VerDatosRegistro = function(Orientacion){
      window.location = "admin-main.html#!/detallereg-orientacion/" + Orientacion.Id.toString();
    }


		ObtenerOrientaciones();

		function ObtenerOrientaciones(){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "lista",
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ===="),
				"IdUsuario": self.IdUsuario
			};
			$http({
			method: 'POST',
			url: '../../services/orientacion.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					self.Orientaciones = response.data.respuesta.orientaciones;
					if(response.data.respuesta.orientaciones.length==0){
						MensajeForm("Advertencia", "El usuario no tiene orientaciones asignadas");
					}
				}else{
					MensajeForm("Error", "Error obteniento orientaciones de usuario");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}



	}
]
});
