

angular.module('mantCarrera').component('mantCarrera', {
    templateUrl: 'mant-carrera/mant-carrera.template.html',
    controller: ['$http', 'Autenticacion',
	function mantCarreraController($http, Autenticacion) {
		Autenticacion.ValidarSesion();
		var self = this;
		
		self.ModificarCarrera = function(Carrera){
			window.location = "admin-main.html#!/form-carrera/" + Carrera.Id;
		}
		
		self.ModificarCursosCarrera = function(Carrera){
			window.location = "admin-main.html#!/mant-cursocarrera/" + Carrera.Id + "/" + Carrera.Nombre;
		}
		

		self.VerModalEliminar = function(Carrera){
			$('#mdConfEliminar').modal('show');
			self.CarreraEliminar = Carrera;
		}
		
		self.EliminarCarrera =  function(){
			$('#mdConfEliminar').modal('hide');
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "eliminar",
				"IdCarrera": self.CarreraEliminar.Id,
				"Param1": localStorage.getItem("t3fDLyrCu8+AeTfJh1U9iQ====")
			};
			$http({
			method: 'POST',
			url: '../../services/carrera.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					MensajeForm("Ok","Carrera eliminada exitosamente");
					ObtenerCarreras();
				}else{
					MensajeForm("Error", "Error eliminando carrera, contacte al administrador");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}
		
		ObtenerCarreras();
		
		function ObtenerCarreras(){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "lista"
			};
			$http({
			method: 'POST',
			url: '../../services/carrera.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				self.Carreras = response.data.carreras;
				if(response.data.carreras.length==0){
					MensajeForm("Advertencia", "No se obtuvieron carreras");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}
	}
]
});


