angular.module('UniT').controller('AdminController', function($scope, $http) {
	/*
	$scope.usuario = localStorage.getItem("rSfbx9Lrz2cbP3TEGTpdKA====");
	$scope.nombre = localStorage.getItem("nomusuario");
	*/
	$scope.UsuarioLogueado = JSON.parse(localStorage.getItem("usuario"));

	$scope.Logout = function(){
		/*
		localStorage.removeItem("rSfbx9Lrz2cbP3TEGTpdKA====");
		localStorage.removeItem("LTNeeyqArqQgOirh3izGIg====");
		*/
		localStorage.removeItem("usuario");
		window.location = "user-login.html";
	}

	$scope.PasswordTextKeyPress = function(event, strTipo){
		if(event.keyCode==13){
			if(strTipo=='pass'){
				$scope.CambiarContrasena();
			}
		}
	}

	$scope.VerModalEnvioPreguntas = function(){
		$scope.txtPregunta = "";
		$('#mdEnvioPregunta').modal('show');
		$('#dvMensajeFormModal').addClass('hidden');
	}

	$scope.VerModalCambioPass = function(){
		$scope.ContrasenaActual = '';
		$scope.NuevaContrasena = '';
		$scope.RepNuevaContrasena = '';
		$('#mdCambiarPass').modal('show');
		$('#dvMensajeFormModal').addClass('hidden');
	}

	$scope.EnviarPregunta = function(){
		$('#mdLoading').modal('show');
		if($scope.txtPregunta==""){
			MensajeFormEnvioPregunta("Favor de ingresar su pregunta");
		}else{
			$('#mdLoading').modal('show');
			var jsonObject = {
				//"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg===="),
				"Param1": $scope.UsuarioLogueado.token,
				"Operacion": "pregunta",
				"Pregunta": $scope.txtPregunta
			};
			$http({
			method: 'POST',
			url: '../../services/usuario.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					MensajeFormEnvioPregunta("Ok", "Correo enviado exitosamente, dentro de poco un administrador se contactara con usted");
				}else{
					MensajeFormEnvioPregunta("Error", "Error enviando pregunta a administradores, intente mas tarde");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeFormEnvioPregunta("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}
	}


	$scope.CambiarContrasena = function(){
		if( Vacio($scope.ContrasenaActual) || Vacio($scope.NuevaContrasena) || Vacio($scope.RepNuevaContrasena) ){
			MensajeFormCambioPass("Advertencia", "Ingrese todos los campos requeridos");
		}else if( $scope.NuevaContrasena != $scope.RepNuevaContrasena ){
			MensajeFormCambioPass("Error", "Las contraseñas no coiniciden");
		}else if($scope.NuevaContrasena.length<6){
			MensajeFormCambioPass("Error", "Contraseña debe tener mínimo 6 caracteres");
		}else if(ValidarFormatoContrasena($scope.NuevaContrasena)){
			MensajeFormCambioPass("Error", "Caracteres inválidos en nueva contraseña");
		}else{
			$('#mdLoading').modal('show');
			var jsonObject = {
				//"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg===="),
				"Param1": $scope.UsuarioLogueado.Token,
				"Operacion": "cambiopass",
				"PassActual": $scope.ContrasenaActual,
				"NuevoPass": $scope.NuevaContrasena
			};
			$http({
			method: 'POST',
			url: '../../services/usuario.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					$scope.ContrasenaActual = '';
					$scope.NuevaContrasena = '';
					$scope.RepNuevaContrasena = '';
					MensajeFormCambioPass("Ok", "¡Listo!");
				}else{
					MensajeFormCambioPass("Error", "Algo salió mal, probá otra vez");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeFormCambioPass("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

	}

});

angular.module('UniT').service('Autenticacion', ['$http',
	function($http) {
		this.ValidarSesion = function(){
			$('#dvMensajeForm').addClass('hidden'); //Fix temporal para ocultar mensajes de éxito/rechazo al cambiar de pantalla
			// strToken = localStorage.getItem("LTNeeyqArqQgOirh3izGIg====");
			UsuarioLogueado = JSON.parse(localStorage.getItem("usuario"));
			if (UsuarioLogueado === undefined || UsuarioLogueado === null ) {
				localStorage.removeItem("usuario");
				window.location = "user-login.html";
			}
			strToken = UsuarioLogueado.Token;
			if(Vacio(strToken)){
				/*
				localStorage.removeItem("rSfbx9Lrz2cbP3TEGTpdKA====");
				localStorage.removeItem("LTNeeyqArqQgOirh3izGIg====");
				*/
				localStorage.removeItem("usuario");
				window.location = "user-login.html";
			}else{
				//$('#mdLoading').modal('show');
				var jsonObject = {
					"Token": strToken,
					"Operacion": "validar"
				};
				$http({
				method: 'POST',
				url: '../../services/login.php',
				headers: {
					'Content-Type': 'application/json; charset=utf-8',
					'Data-Type': 'json'
				},
				data: jsonObject
				}).then(function(response, status){
					if(response.data.valido!=1){
						/*
						localStorage.removeItem("rSfbx9Lrz2cbP3TEGTpdKA====");
						localStorage.removeItem("LTNeeyqArqQgOirh3izGIg====");
						*/
						localStorage.removeItem("usuario");
						window.location = "user-login.html";
					}
				},	function(response){

				});
			}

		}


	}
]);

function MensajeFormCambioPass(strTipo, strMensaje){
	var strClass = "";
	if(strTipo=="Ok"){
		strClass = strClass + " alert-success";
	}else if(strTipo=="Error"){
		strClass = strClass + " alert-danger";
	}else if(strTipo=="Advertencia"){
		strClass = strClass + " alert-warning";
	}
	$('#dvMensajeCambioPass').removeClass('alert-danger alert-success alert-warning hidden').addClass(strClass);
	$('#spMensajeCambioPass').html(strMensaje);
}

function MensajeFormEnvioPregunta(strTipo, strMensaje){
	var strClass = "";
	if(strTipo=="Ok"){
		strClass = strClass + " alert-success";
	}else if(strTipo=="Error"){
		strClass = strClass + " alert-danger";
	}else if(strTipo=="Advertencia"){
		strClass = strClass + " alert-warning";
	}
	$('#dvMensajeEnvioPreguntas').removeClass('alert-danger alert-success alert-warning hidden').addClass(strClass);
	$('#spMensajeEnvioPreguntas').html(strMensaje);
}
