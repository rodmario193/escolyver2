

angular.module('detalleBusqueda').component('detalleBusqueda', {
    templateUrl: 'detalle-busqueda/detalle-busqueda.template.html',
    controller: ['$routeParams','$http', 'Autenticacion',
	function detalleBusquedaController($routeParams, $http, Autenticacion) {
		Autenticacion.ValidarSesion();

		var self = this;
		var strIdCarrera = $routeParams.idCarrera;
		self.NotaTotal = 0;
		self.NotaIndividual = 0;

		ObtenerDetalleBusqueda(self, strIdCarrera);

    self.VerModalPensum = function(){
      $('#mdLoading').modal('show');
      var jsonObject = {
        "Operacion": "pensum",
        "IdCarrera": strIdCarrera,
        "Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg====")
      };
      $http({
      method: 'POST',
      url: '../../services/busqueda.php',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Data-Type': 'json'
      },
      data: jsonObject
      }).then(function(response, status){
        var _Respuesta = response.data.Respuesta;
        if(_Respuesta.Exito){
          $('#mdPensum').modal('show');
          self.Periodos = _Respuesta.Pensum;
        }else{
          MensajeForm("Error", _Respuesta.Mensaje);
        }
        $('#mdLoading').modal('hide');
      },	function(response){
        MensajeForm("Error", "Error de comunicacion, intente mas tarde");
        $('#mdLoading').modal('hide');
      });
    }

    self.CambioDespliegue = function(Periodo){
			Periodo.BanderaDesp = !Periodo.BanderaDesp;
		}

		self.Votar = function(intCalificacion){
			if(self.Carrera.NotaIndividual!=intCalificacion){
				$('#mdLoading').modal('show');
				var jsonObject = {
					"Operacion": "votar",
					"IdCarrera": strIdCarrera,
					"Nota": intCalificacion,
					"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg====")
				};
				$http({
				method: 'POST',
				url: '../../services/busqueda.php',
				headers: {
					'Content-Type': 'application/json; charset=utf-8',
					'Data-Type': 'json'
				},
				data: jsonObject
				}).then(function(response, status){
					if(response.data.respuesta.codigo==1){
						self.Carrera.NotaIndividual = intCalificacion;
						self.NotaIndividual = intCalificacion;
						self.NotaTotal = response.data.respuesta.NotaTotal;
						MensajeFormCalificacion("Ok", "¡Muchas gracias!");
					}else{
						MensajeFormCalificacion("Error", "Error en ingreso de calificación, favor intentar nuevamente");
					}
					$('#mdLoading').modal('hide');
				},	function(response){
					MensajeForm("Error", "Error de comunicacion, intente mas tarde");
					$('#mdLoading').modal('hide');
				});
			}
		}

		function ObtenerDetalleBusqueda(self, strIdCarrera){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "detalle",
				"IdCarrera": strIdCarrera,
				"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg====")
			};
			$http({
			method: 'POST',
			url: '../../services/busqueda.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
        var _Respuesta = response.data.Respuesta;
				if(_Respuesta.Exito){
					self.Carrera = _Respuesta.Carrera;
					self.NotaTotal = _Respuesta.Carrera.NotaTotal;
					self.NotaIndividual = _Respuesta.Carrera.NotaIndividual;
				}else{
					MensajeForm("Error", _Respuesta.Mensaje);
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

		self.ObtenerImagenEstrella = function(intNumEstrella, intTipoCalificacion){
			var _strClaseCss = "star-gray.png";
			var intNota = 0;
			if(intTipoCalificacion==1){
				intNota = self.NotaTotal;
			}else if(intTipoCalificacion==2){
				intNota = self.NotaIndividual;
			}
			if( (intNota==1) && (intNumEstrella==1) ){
				_strClaseCss = "star.png";
			}else if ( (intNota==2) && (intNumEstrella<=2) ) {
				_strClaseCss = "star.png";
			}else if ( (intNota==3) && (intNumEstrella<=3) ) {
				_strClaseCss = "star.png";
			}else if ( (intNota==4) && (intNumEstrella<=4) ) {
				_strClaseCss = "star.png";
			}else if ( (intNota==5) && (intNumEstrella<=5) ) {
				_strClaseCss = "star.png";
			}

			return _strClaseCss;
		}

    self.ObtenerValorConFormato = function(strValor){
      if(strValor=="") return "No aplica";
      if(strValor!="") return strValor;
    }

		self.ObtenerClaseCss = function(strValor){
      if(strValor=="") return "pNulo";
      if(strValor!="") return "";
    }

		function MensajeFormCalificacion(strTipo, strMensaje){
			var strClass = "";
			if(strTipo=="Ok"){
				strClass = strClass + " alert-success";
			}else if(strTipo=="Error"){
				strClass = strClass + " alert-danger";
			}else if(strTipo=="Advertencia"){
				strClass = strClass + " alert-warning";
			}
			$('#dvMensajeFormModal').removeClass('alert-danger alert-success alert-warning hidden').addClass(strClass);
			$('#spMensajeFormModal').html(strMensaje);
		}
	}
]
});
