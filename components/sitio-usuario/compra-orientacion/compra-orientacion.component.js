

angular.module('compraOrientacion').component('compraOrientacion', {
    templateUrl: 'compra-orientacion/compra-orientacion.template.html',
    controller: ['$routeParams','$http','Autenticacion',
	function compraOrientacionController($routeParams, $http, Autenticacion) {
		Autenticacion.ValidarSesion();
		var self = this;

		self.IdItem = $routeParams.idItem;
		self.CantidadBusquedas = $routeParams.cantidad;
		self.TextoMonto = "$. " + $routeParams.precio;
		self.Monto = $routeParams.precio;

		self.Usuario = {
			"NombreFacturacion": "",
			"ApellidoFacturacion": "",
			"Nit": "",
			"NombreDepFacturacion": ""
		};
		self.Listo = false;

		self.IniciarPago = function(){
      $('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "tk_o",
				"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg====")
			};
			$http({
			method: 'POST',
			url: '../../services/compra.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
        var _Respuesta = response.data.Respuesta;
        if(_Respuesta.Token!=""){
          //console.log(_Respuesta.Token);
          localStorage.setItem("hdAMzG3BQA/7EBQ2", _Respuesta.Token);
          //console.log('compra2.php?monto=' + self.Monto + "&cantidad=" + self.CantidadBusquedas + "&tokencompra=" +  _Respuesta.Token);
          window.location = 'compra2.php?monto=' + self.Monto + "&cantidad=" + self.CantidadBusquedas + "&tokencompra=" +  _Respuesta.Token;
        }else{
          MensajeForm("Error", "Error en autenticación inicial de operación");
        }
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});


		}

		var ID_USUARIO = localStorage.getItem("rSfbx9Lrz2cbP3TEGTpdKA====");
		ObtenerInfoFinanciera(self);

		//Obtener Informacion de Facturacion de usuario
		function ObtenerInfoFinanciera(self){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "datos",
				"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg====")
			};
			$http({
			method: 'POST',
			url: '../../services/compra.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo == 1){
					self.Usuario = response.data.respuesta.usuario; //Informacion de facturacion obtenida exitosamente
					if( Vacio(self.Usuario.NombreFacturacion) || Vacio(self.Usuario.ApellidoFacturacion) ||
						Vacio(self.Usuario.Nit) || Vacio(self.Usuario.NombreDepFacturacion) ){ //Validar que datos de facturacion hayan sido llenados
						MensajeForm("Error", "Aun no has ingresado TODOS los datos para tu factura. Andá a “mi perfil” para modificarlos."); //No estan llenos todos los datos de facturacion
					}else{
						//Toda la informacion de facturacion esta disponible
						if(self.IdItem == "001"){
							$('#dvPago1').removeClass('hidden');
						}else if(self.IdItem == "002"){
							$('#dvPago2').removeClass('hidden');
						}else if(self.IdItem == "003"){
							$('#dvPago3').removeClass('hidden');
						}
					}
				}else{
					MensajeForm("Error", "Aun no has ingresado los datos para tu factura. Andá a “mi perfil” para modificarlos.");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

	}
]
});
