
 angular.
  module('UniT').
  config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/user', {
			templateUrl: "user-ini.html"
        }).
        when('/list-busqueda', {
			template: '<list-busqueda></list-busqueda>'
        }).
		when('/list-agrupaciones/:idTipo/:esPrueba', {
			template: '<list-agrupaciones></list-agrupaciones>'
        }).
		when('/list-tipo/:esPrueba', {
			template: '<list-tipo></list-tipo>'
        }).
		when('/form-busqueda/:idGrupo/:nombreGrupo', {
			template: '<form-busqueda></form-busqueda>'
        }).
		when('/compra-busqueda/:idItem/:precio/:cantidad', {
			template: '<compra-busqueda></compra-busqueda>'
		}).
		when('/perfil-usuario', {
			template: '<perfil-usuario></perfil-usuario>'
		}).
		when('/detalle-busqueda/:idCarrera', {
			template: '<detalle-busqueda></detalle-busqueda>'
		}).
		when('/list-orientacion', {
			template: '<list-orientacion></list-orientacion>'
		}).
		when('/detalle-orientacion/:idOrientacion', {
			template: '<detalle-orientacion></detalle-orientacion>'
		}).
		when('/registro-orientacion/:idOrientacion', {
			template: '<registro-orientacion></registro-orientacion>'
		}).
		when('/compra-orientacion/:idItem/:precio/:cantidad', {
			template: '<compra-orientacion></compra-orientacion>'
		}).
        otherwise('/user');
    }
  ]);
