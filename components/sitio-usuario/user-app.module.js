
angular.module('UniT', [
'ngRoute',
'listBusqueda',
'listAgrupaciones',
'listTipo',
'formBusqueda',
'compraBusqueda',
'perfilUsuario',
'detalleBusqueda',
'listOrientacion',
'detalleOrientacion',
'registroOrientacion',
'compraOrientacion'
]);
