

angular.module('detalleOrientacion').component('detalleOrientacion', {
    templateUrl: 'detalle-orientacion/detalle-orientacion.template.html',
    controller: ['$routeParams','$http','Autenticacion',
	function detalleOrientacionController($routeParams, $http, Autenticacion) {

		Autenticacion.ValidarSesion();

		var self = this;
		self.IdOrientacion = $routeParams.idOrientacion;


    self.ColocarValorNoDisponible = function(strValor){
      var _strValorRetorno = strValor;

      if(strValor=="N/D") _strValorRetorno = "¡Muy pronto lo sabrás!";

      return _strValorRetorno;
    }

    self.ColocarTextoEstadoExamen = function(strEstado){
      var _strTxtEstado = "";

      if(strEstado=="Pendiente") _strTxtEstado = "No lo has hecho";
      if(strEstado=="Enviado") _strTxtEstado = "Enlace enviado a tu correo";
      if(strEstado=="Finalizado") _strTxtEstado = "¡Ya lo hiciste!";

      return _strTxtEstado;
    }

		ObtenerDetalleOrientacion();

		function ObtenerDetalleOrientacion(){
			//$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "detalle_usuario",
				"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg===="),
				"IdOrientacion": self.IdOrientacion
			};
			$http({
			method: 'POST',
			url: '../../services/orientacion.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				var Respuesta = response.data.respuesta;
				self.Orientacion = Respuesta.orientacion;
				//$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				//$('#mdLoading').modal('hide');
			});
		}

	}
]
});
