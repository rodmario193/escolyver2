angular.module("perfilUsuario").component("perfilUsuario", {
  templateUrl: "perfil-usuario/perfil-usuario.template.html",
  controller: [
    "$routeParams",
    "$http",
    "Autenticacion",
    function perfilUsuarioController($routeParams, $http, Autenticacion) {
      Autenticacion.ValidarSesion();

      var self = this;

      self.Usuario = {
        IdUsuario: "",
        PrimerNombre: "",
        SegundoNombre: "",
        PrimerApellido: "",
        SegundoApellido: "",
        Genero: "",
        FechaNacimiento: "",
        Correo: "",
        TelefonoCelular: "",
        Departamento: 0,
        Municipio: 0,
        Direccion: "",
        Instituto: "",
        NombreFacturacion: "",
        ApellidoFacturacion: "",
        Nit: "",
        DepartamentoFacturacion: 0,
        Imagen: "",
        RedSocial: "",
        Medio: ""
      };
      // self.IdUsuario = localStorage.getItem("rSfbx9Lrz2cbP3TEGTpdKA====");
      self.UsuarioLogueado = JSON.parse(localStorage.getItem("usuario"));
      self.IdUsuario = UsuarioLogueado.IdUsuario;
      self.Titulo = "¿Querés cambiar algo " + self.IdUsuario + "?";

      ObtenerDepartamentos(self);
      ObtenerDetalleUsuario(self);

      function ObtenerDetalleUsuario(self) {
        $("#mdLoading").modal("show");
        var jsonObject = {
          Operacion: "detalle",
          //"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg====")
          Param1: UsuarioLogueado.Token
        };
        $http({
          method: "POST",
          url: "../../services/usuario.php",
          headers: {
            "Content-Type": "application/json; charset=utf-8",
            "Data-Type": "json"
          },
          data: jsonObject
        }).then(
          function(response, status) {
            if (response.data.respuesta.codigo == 1) {
              self.Usuario = response.data.respuesta.usuario;
              ObtenerMunicipios(self);
              showPanel(0, "#f44336");
            } else {
              MensajeForm(
                "Error",
                "Error obteniendo informaci&oacute;n de usuario"
              );
            }
            $("#mdLoading").modal("hide");
          },
          function(response) {
            MensajeForm("Error", "Error de comunicacion, intente mas tarde");
            $("#mdLoading").modal("hide");
          }
        );
      }

      self.EditarUsuario = function() {
        if (
          Vacio(self.Usuario.PrimerNombre) ||
          Vacio(self.Usuario.PrimerApellido) ||
          Vacio(self.Usuario.Genero) ||
          Vacio(self.Usuario.Correo) ||
          Vacio(self.Usuario.TelefonoCelular) ||
          Vacio(self.Usuario.Departamento) ||
          Vacio(self.Usuario.Municipio) ||
          Vacio(self.Usuario.RedSocial) ||
          Vacio(self.Usuario.Medio) ||
          Vacio(self.Usuario.Instituto) ||
          Vacio(self.Usuario.FechaNacimiento)
        ) {
          MensajeForm("Error", "¡Te hacen falta datos que completar!");
        } else if (!ValidarFecha(self.Usuario.FechaNacimiento)) {
          MensajeForm("Advertencia", "Error de formato de fecha de nacimiento");
        } else if (!ValidarIdUsuario(self.Usuario.IdUsuario)) {
          MensajeForm(
            "Advertencia",
            "El id de usuario tiene un formato incorrecto"
          );
        } else if (!ValidarCorreo(self.Usuario.Correo)) {
          //Validar formato de correo electrónico
          MensajeForm("Advertencia", "Pusiste mal tu correo");
        } else if (!ValidarCelular(self.Usuario.TelefonoCelular)) {
          //Validar formato de numero de telefono
          MensajeForm("Advertencia", "Pusiste mal tu teléfono");
        } else {
          $("#mdLoading").modal("show");
          var jsonObject = {
            Operacion: "editar",
            Usuario: self.Usuario,
            // Param1: localStorage.getItem("LTNeeyqArqQgOirh3izGIg====")
            Param1: self.UsuarioLogueado.Token
          };
          $http({
            method: "POST",
            url: "../../services/usuario.php",
            headers: {
              "Content-Type": "application/json; charset=utf-8",
              "Data-Type": "json"
            },
            data: jsonObject
          }).then(
            function(response, status) {
              if (response.data.respuesta.codigo == 1) {
                MensajeForm("Ok", "Perfil editado exitosamente");
              } else {
                MensajeForm("Error", response.data.respuesta.mensaje);
              }
              $("#mdLoading").modal("hide");
            },
            function(response) {
              MensajeForm("Error", "Error de comunicacion, intente mas tarde");
              $("#mdLoading").modal("hide");
            }
          );
        }
      };

      self.CambioDepartamento = function() {
        ObtenerMunicipios(self);
      };

      function ObtenerMunicipios(self) {
        var jsonObject = {
          Operacion: "municipios",
          IdDepartamento: self.Usuario.Departamento
        };
        $http({
          method: "POST",
          url: "../../services/universidad.php",
          headers: {
            "Content-Type": "application/json; charset=utf-8",
            "Data-Type": "json"
          },
          data: jsonObject
        }).then(
          function(response, status) {
            if (response.data.municipios.length > 0) {
              self.Municipios = response.data.municipios;
            } else {
              MensajeForm(
                "Error",
                "Error obteniendo municipios, intente mas tarde"
              );
              self.Bloquear = true;
            }
          },
          function(response) {
            MensajeForm("Error", "Error de comunicacion, intente mas tarde");
          }
        );
      }

      function ObtenerDepartamentos(self) {
        var jsonObject = {
          Operacion: "departamentos"
        };
        $http({
          method: "POST",
          url: "../../services/universidad.php",
          headers: {
            "Content-Type": "application/json; charset=utf-8",
            "Data-Type": "json"
          },
          data: jsonObject
        }).then(
          function(response, status) {
            if (response.data.departamentos.length > 0) {
              self.Departamentos = response.data.departamentos;
              self.DeptsFacturacion = response.data.departamentos;
            } else {
              MensajeForm(
                "Error",
                "Error obteniendo departamentos, intente mas tarde"
              );
              self.Bloquear = true;
            }
          },
          function(response) {
            MensajeForm("Error", "Error de comunicacion, intente mas tarde");
          }
        );
      }
    }
  ]
});
