

angular.module('listBusqueda').component('listBusqueda', {
    templateUrl: 'list-busqueda/list-busqueda.template.html',
    controller: ['$http','Autenticacion',
	function mantUniversidadController($http, Autenticacion) {

		Autenticacion.ValidarSesion();

		var self = this;

		ObtenerNumeroBusquedas(self);

		ListaOpcionesCompras(self);
		ObtenerBusquedas(self);

		self.IniciarCompra = function(Compra){
			window.location = "user-main.html#!/compra-busqueda/" + Compra.IdItem + "/" + Compra.Saldo + "/" + Compra.Cantidad;
		}

		self.IrADetalleBusqueda = function(Busqueda){
			window.location = "user-main.html#!/form-busqueda/" + Busqueda.Id + "/" + Busqueda.Nombre;
		}

    self.ObtenerTextoBusqueda = function(strCantidad){
      var _strTexto = "";

      if(strCantidad=="1") _strTexto = "¡Quiero una!";
      if(strCantidad=="2") _strTexto = "¡Quiero dos!";
      if(strCantidad=="3") _strTexto = "¡Quiero tres!";

      return _strTexto;
    }

		function ListaOpcionesCompras(self){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "opciones",
        "TipoCompra": "B"
			};
			$http({
			method: 'POST',
			url: '../../services/compra.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					self.Compras = response.data.respuesta.compras;
				}else{
					MensajeForm("Error", "Error obteniendo opciones de compra");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}


		function ObtenerNumeroBusquedas(self){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "num_busquedas",
				"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg====")
			};
			$http({
			method: 'POST',
			url: '../../services/busqueda.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					self.NumBusquedas  = response.data.respuesta.numbusquedas;
					self.NumBusquedasPrueba = response.data.respuesta.numbusquedasprueba;
					if(self.NumBusquedas > 0){
						$('#aNuevaBusqueda').removeClass('hidden');
					}
					if(self.NumBusquedasPrueba > 0){
						$('#aNuevaBusquedaPrueba').removeClass('hidden');
					}
				}else{
					MensajeForm("Error", "Error obteniendo cantidad de b&uacute;squedas disponibles");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

		function ObtenerBusquedas(self){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "lista_busquedas",
				"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg====")
			};
			$http({
			method: 'POST',
			url: '../../services/busqueda.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					self.Busquedas = response.data.respuesta.busquedas;
				}else{
					MensajeForm("Error", "Error obteniendo b&uacute;squedas realizadas");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

	}
]
});
