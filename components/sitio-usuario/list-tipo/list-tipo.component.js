

angular.module('listTipo').component('listTipo', {
    templateUrl: 'list-tipo/list-tipo.template.html',
    controller: ['$routeParams', '$http', 'Autenticacion',
	function listTipoController($routeParams, $http, Autenticacion) {
		Autenticacion.ValidarSesion();
		var self = this;
		self.EsPrueba = $routeParams.esPrueba;
		self.IrAGruposBusqueda = function(Tipo){
			window.location = "user-main.html#!/list-agrupaciones/" + Tipo.Id + "/" + self.EsPrueba;
		}

		ListaTiposBusqueda(self);
		function ListaTiposBusqueda(self){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "lista_tipos",
				"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg====")
			};
			$http({
			method: 'POST',
			url: '../../services/busqueda.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					self.Tipos = response.data.respuesta.tipos;
				}else{
					MensajeForm("Error", "Error obteniendo tipos de grupo de b&uacute;squeda, intente mas tarde");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

	}
]
});
