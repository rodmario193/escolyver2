

angular.module('listOrientacion').component('listOrientacion', {
    templateUrl: 'list-orientacion/list-orientacion.template.html',
    controller: ['$http','Autenticacion',
	function mantUniversidadController($http, Autenticacion) {

		Autenticacion.ValidarSesion();

		var self = this;

    ListaOpcionesCompras(self);
    function ListaOpcionesCompras(self){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "opciones",
        "TipoCompra": "O"
			};
			$http({
			method: 'POST',
			url: '../../services/compra.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					self.Compras = response.data.respuesta.compras;
				}else{
					MensajeForm("Error", "Error obteniendo opciones de compra");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

    self.IniciarCompra = function(Compra){
			window.location = "user-main.html#!/compra-orientacion/" + Compra.IdItem + "/" + Compra.Saldo + "/" + Compra.Cantidad;
		}

		self.VerDetalleOrientacion = function(Orientacion){
			//window.location = "user-main.html#!/detalle-orientacion/" + Orientacion.Id;
      // Validar si ya se han registrado los datos de la orientación
      $('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "estado",
				"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg===="),
        "IdOrientacion": Orientacion.Id
			};
			$http({
			method: 'POST',
			url: '../../services/orientacion.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
        $('#mdLoading').modal('hide');
				var Respuesta = response.data.respuesta;
				if(Respuesta.OrientacionActiva){ //Orientación activa, ir a pantalla de estatus
          window.location = "user-main.html#!/detalle-orientacion/" + Orientacion.Id;
        }else{  //Orientación inactiva, ir a pantalla de registro
          window.location = "user-main.html#!/registro-orientacion/" + Orientacion.Id;
        }
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				//$('#mdLoading').modal('hide');
			});
		}

		ObtenerOrientacionesDeUsuario();
		function ObtenerOrientacionesDeUsuario(){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "lista_usuario",
				"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg====")
			};
			$http({
			method: 'POST',
			url: '../../services/orientacion.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				var Respuesta = response.data.respuesta;
				if(Respuesta.Codigo==1){
					self.Orientaciones = Respuesta.Orientaciones;
				}else if(Respuesta.Codigo==2){
					//MensajeForm("Advertencia", Respuesta.Mensaje);
				}else{
					MensajeForm("Error", Respuesta.Mensaje);
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

	}
]
});
