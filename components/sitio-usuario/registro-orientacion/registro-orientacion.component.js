

angular.module('registroOrientacion').component('registroOrientacion', {
    templateUrl: 'registro-orientacion/registro-orientacion.template.html',
    controller: ['$routeParams','$http','Autenticacion',
	function registroOrientacionController($routeParams, $http, Autenticacion) {

		Autenticacion.ValidarSesion();

		var self = this;
    self.Orientacion = {
      "Id": $routeParams.idOrientacion,
      "Actividad1": "",
      "Actividad2": "",
      "Actividad3": "",
      "Actividad4": "",
      "Actividad5": "",
      "Actividad6": "",
      "Objeto1": "",
      "Objeto2": "",
      "Objeto3": "",
      "Objeto4": "",
      "Objeto5": "",
      "Objeto6": "",
      "Logro1": "",
      "Logro2": "",
      "Logro3": "",
      "Logro4": "",
      "Pregunta1Opcion1": 0,
      "Pregunta1Opcion2": 0,
      "Pregunta1Opcion3": 0,
      "Pregunta1Opcion4": 0,
      "Pregunta2Opcion1": 0,
      "Pregunta2Opcion2": 0,
      "Pregunta2Opcion3": 0,
      "Pregunta2Opcion4": 0,
      "Pregunta3Opcion1": 0,
      "Pregunta3Opcion2": 0,
      "Pregunta3Opcion3": 0,
      "Pregunta3Opcion4": 0,
      "Pregunta3Opcion5": 0,
      "Pregunta3Opcion6": 0,
      "Pregunta3Opcion7": 0,
      "EstudioMama": "",
      "GraduacionMama": 0,
      "TrabajoMama": "",
      "EstudioPapa": "",
      "GraduacionPapa": 0,
      "TrabajoPapa": "",
      "EstudiosHermanos1": "",
      "EstudiosHermanos2": "",
      "EstudiosHermanos3": "",
      "EstudiosHermanos4": "",
      "EstudiosFamiliares1": "",
      "EstudiosFamiliares2": "",
      "EstudiosFamiliares3": "",
      "EstudiosFamiliares4": "",
      "EstudiosFamiliares5": "",
      "EstudiosFamiliares6": ""
    };

    self.VerDetalleOrientacion = function(){
        window.location = "user-main.html#!/detalle-orientacion/" + self.Orientacion.Id;
    }


    self.VerStatus = function(){
        window.location = "user-main.html#!/detalle-orientacion/" + self.Orientacion.Id;
    }

    self.RegistrarOrientacion = function(){
      //Validar campos obligatorios
      var _strMensajeError = "";
      if( Vacio(self.Orientacion.Actividad1) ) _strMensajeError = "Debes ingresar un valor en el primer campo de actividad.";
      if( Vacio(self.Orientacion.Objeto1) ) _strMensajeError = "Debes ingresar un valor en el primer campo de objeto.";
      if( Vacio(self.Orientacion.Logro1) ) _strMensajeError = "Debes ingresar un valor en el primer campo de logro.";

      if(_strMensajeError != ""){
        MensajeForm("Error", "Todavía no has respondido todo");
      }

      if(_strMensajeError==""){ //No se ha presentado ningún error en el formulario
        $('#mdLoading').modal('show');
        var jsonObject = {
          "Operacion": "registro",
          "Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg===="),
          "Orientacion": self.Orientacion
        };
        $http({
        method: 'POST',
        url: '../../services/orientacion.php',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Data-Type': 'json'
        },
        data: jsonObject
        }).then(function(response, status){
          $('#mdLoading').modal('hide');
          var _Respuesta = response.data.respuesta;
          if(_Respuesta.Exito){ //Orientación activa, ir a pantalla de estatus
            MensajeForm("Ok", _Respuesta.Mensaje);
            Finalizar(); // Función que despliega la tab final
            $('#dvRegistroOk').removeClass('hidden');
          }else{  //Orientación inactiva, ir a pantalla de registro
            MensajeForm("Error", _Respuesta.Mensaje);
          }
        },	function(response){
          MensajeForm("Error", "Error de comunicacion, intente mas tarde");
          //$('#mdLoading').modal('hide');
        });
      }

    }

	}
]
});
