angular.module("primerIngreso").component("primerIngreso", {
  templateUrl: "primer-ingreso/primer-ingreso.template.html",
  controller: [
    "$routeParams",
    "$http",
    "Autenticacion",
    function primerIngresoController($routeParams, $http, Autenticacion) {
      Autenticacion.ValidarSesion();

      self.UsuarioLogueado = JSON.parse(localStorage.getItem("usuario"));

      actualizarEstadoPrimerIngreso(self);

      function actualizarEstadoPrimerIngreso(self) {
        var jsonObject = {
          Operacion: "act-primer-ingreso",
          //"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg====")
          Param1: self.UsuarioLogueado.Token
        };
        $http({
          method: "POST",
          url: "../../services/usuario.php",
          headers: {
            "Content-Type": "application/json; charset=utf-8",
            "Data-Type": "json"
          },
          data: jsonObject
        }).then(
          function(response, status) {
            if (response.data.respuesta.codigo == 1) {
              // No es necesario hacer nada
            } else {
              /*
              localStorage.removeItem("usuario");
              window.location = "user-login.html";
              */
            }
          },
          function(response) {
            /*
            localStorage.removeItem("usuario");
            window.location = "user-login.html";
            */
          }
        );
      }
    }
  ]
});
