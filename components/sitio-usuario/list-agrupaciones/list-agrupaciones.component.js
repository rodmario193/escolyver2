

angular.module('listAgrupaciones').component('listAgrupaciones', {
    templateUrl: 'list-agrupaciones/list-agrupaciones.template.html',
    controller: ['$routeParams','$http', 'Autenticacion',
	function listAgrupacionesController($routeParams, $http, Autenticacion) {
		Autenticacion.ValidarSesion();
		var self = this;
		self.IdTipo = $routeParams.idTipo;
		self.EsPrueba = $routeParams.esPrueba;
		ListaTiposBusqueda(self);
		function ListaTiposBusqueda(self){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "lista_grupos",
				"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg===="),
				"IdTipoGrupo": self.IdTipo,
				"Prueba": self.EsPrueba
			};
			$http({
			method: 'POST',
			url: '../../services/busqueda.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					self.Grupos = response.data.respuesta.grupos;
					if(self.Grupos.length==0){
						MensajeForm("Advertencia", "No existen grupos de b&uacute;squeda para la categor&iacute;a seleccionada");
					}
				}else{
					MensajeForm("Error", "Error obteniendo grupos de b&uacute;squeda, intente mas tarde");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

		self.SeleccionarTipo = function(Grupo){
			self.GrupoABuscar = Grupo;
			$('#mdConfirmacionBusqueda').modal('show');
		}

		self.Regresar = function(){
			window.location = "user-main.html#!/list-tipo/" + self.EsPrueba;
		}

		self.ConfirmarBusqueda = function(){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "consumir",
				"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg===="),
				"IdGrupo": self.GrupoABuscar.Id,
				"Prueba": self.EsPrueba
			};
			$http({
			method: 'POST',
			url: '../../services/busqueda.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					MensajeForm("Ok", "¡Perfecto! Has activado tu búsqueda");
					$('#dvBusquedaGrupo').addClass('hidden');
					$('#dvExito').removeClass('hidden');
				}else{
					MensajeForm("Error", response.data.respuesta.mensaje);
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
			$('#mdConfirmacionBusqueda').modal('hide');
		}

		function IraFormBusqueda(){
			window.location = "user-main.html#!/form-busqueda";
		}

	}
]
});
