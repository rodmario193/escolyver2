

angular.module('formBusqueda').component('formBusqueda', {
    templateUrl: 'form-busqueda/form-busqueda.template.html',
    controller: ['$routeParams','$http', 'Autenticacion',
	function formBusquedaController($routeParams, $http, Autenticacion) {
		Autenticacion.ValidarSesion();
		var self = this;
		self.GrupoBusqueda = $routeParams.nombreGrupo;
		self.IdGrupoBusqueda = $routeParams.idGrupo;
		ListarResultadosBusqueda(self);
		ListarFavoritos(self);
    ListaDepartamentos(self);
    self.CostoBusquedaMensual = 0;
    self.Jornada = '';
		self.IrADetalle = function(Busqueda){
			window.location = "user-main.html#!/detalle-busqueda/" + Busqueda.Id
		}

    // Setea los valores para el filtro de costo, esto se hace así para evitar filtrar mientras el usuario escribe
    self.SetearValoresdeFiltro = function(){
      self.CostoMensualMaxBusqueda = self.CostoMensualMax;
      self.CostoMensualMinBusqueda = self.CostoMensualMin;
    }

		self.Favorito = function(Busqueda){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "favorito",
				"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg===="),
				"IdCarrera": Busqueda.Id
			};
			$http({
			method: 'POST',
			url: '../../services/busqueda.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					console.log("Estatus de favorito cambiado");
					ListarFavoritos(self);
					if(Busqueda.Favorito == 1){
						Busqueda.Favorito = 0;
					}else{
						Busqueda.Favorito = 1;
					}
				}else{
					console.log("Error en cambio de estatus");
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

		function ListarResultadosBusqueda(self){
			$('#mdLoading').modal('show');
			var jsonObject = {
				"Operacion": "lista_carreras",
				"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg===="),
				"IdGrupo": self.IdGrupoBusqueda
			};
			$http({
			method: 'POST',
			url: '../../services/busqueda.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
        var _Respuesta = response.data.Respuesta;
				if(_Respuesta.Exito){
					self.Busquedas = _Respuesta.Busquedas;
				}else{
					MensajeForm("Error", _Respuesta.Mensaje);
				}
				$('#mdLoading').modal('hide');
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
				$('#mdLoading').modal('hide');
			});
		}

		function ListarFavoritos(self){
			var jsonObject = {
				"Operacion": "lista_favoritos",
				"Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg===="),
				"IdGrupo": self.IdGrupoBusqueda
			};
			$http({
			method: 'POST',
			url: '../../services/busqueda.php',
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Data-Type': 'json'
			},
			data: jsonObject
			}).then(function(response, status){
				if(response.data.respuesta.codigo==1){
					self.Favoritos = response.data.respuesta.favoritos;
				}else{
					self.Favoritos = [];
				}
			},	function(response){
				MensajeForm("Error", "Error de comunicacion, intente mas tarde");
			});
		}

    function ListaDepartamentos(self){
      var jsonObject = {
        "Operacion": "lista_departamentos",
        "Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg====")
      };
      $http({
      method: 'POST',
      url: '../../services/busqueda.php',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Data-Type': 'json'
      },
      data: jsonObject
      }).then(function(response, status){
        var _Respuesta = response.data.Respuesta;
        if(_Respuesta.Exito){
          self.Departamentos = _Respuesta.Departamentos;
        }else{
          MensajeForm(_Respuesta.Mensaje);
        }
      },	function(response){
        MensajeForm("Error", "Error de comunicacion, intente mas tarde");
      });
    }

    self.ColocarFiltroJornada = function(){
      var _ValorJornada = self.Jornada;
    }

    self.FiltrarPorJornada = function(TipoJornada){
      return function(Controlador){
        if(TipoJornada==""){
          return true;
        }else if(TipoJornada=="Matutina"){
          return Controlador['JornadaMatutina'] == 1;
        }else if(TipoJornada=="Vespertina"){
          return Controlador['JornadaVespertina'] == 1;
        }else if(TipoJornada=="Nocturna"){
          return Controlador['JornadaNocturna'] == 1;
        }else if(TipoJornada=="Sabatina"){
          return Controlador['JornadaSabatina'] == 1;
        }else if(TipoJornada=="Dominical"){
          return Controlador['JornadaDominical'] == 1;
        }
      }
    }

    // Función usada por los filtros de angular para filtrar por costos menor o igual a un determinado valor
    self.MenorIgualA = function(ParamControlador, Valor){
        return function(Controlador){
          if((Valor==0)||(Valor==null)){
            return true;
          }else{
            return Controlador[ParamControlador] <= Valor;
          }
        }
    }

    // Función usada por los filtros de angular para filtrar por costos mayor o igual a un determinado valor
    self.MayorIgualA = function(ParamControlador, Valor){
        return function(Controlador){
          if((Valor==0)||(Valor==null)){
            return true;
          }else{
            return Controlador[ParamControlador] >= Valor;
          }
        }
    }

    self.CambioDepartamento = function(){
      self.MunicipioBusqueda = '';
      $('#mdLoading').modal('show');
      var jsonObject = {
        "Operacion": "lista_municipios",
        "Param1": localStorage.getItem("LTNeeyqArqQgOirh3izGIg===="),
        "IdDepartamento": self.DepartamentoBusqueda
      };
      $http({
      method: 'POST',
      url: '../../services/busqueda.php',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Data-Type': 'json'
      },
      data: jsonObject
      }).then(function(response, status){
        var _Respuesta = response.data.Respuesta;
        if(_Respuesta.Exito){
          self.Municipios = _Respuesta.Municipios;
        }else{
          MensajeForm(_Respuesta.Mensaje);
        }
        $('#mdLoading').modal('hide');
      },	function(response){
        MensajeForm("Error", "Error de comunicacion, intente mas tarde");
        $('#mdLoading').modal('hide');
      });
    }

		self.ObtenerImagenEstrella = function(intNumEstrella, intNota){
			var _strImagen = "star-gray.png";

			if( (intNota==1) && (intNumEstrella==1) ){
				_strImagen = "star.png";
			}else if ( (intNota==2) && (intNumEstrella<=2) ) {
				_strImagen = "star.png";
			}else if ( (intNota==3) && (intNumEstrella<=3) ) {
				_strImagen = "star.png";
			}else if ( (intNota==4) && (intNumEstrella<=4) ) {
				_strImagen = "star.png";
			}else if ( (intNota==5) && (intNumEstrella<=5) ) {
				_strImagen = "star.png";
			}

			return _strImagen;
		}

	}
]
});
