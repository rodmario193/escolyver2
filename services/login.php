<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization");

$configs = include('config.php');
include('bitacora.php');
include('correo.php');
include('token.php');
include(__DIR__ . '/error-manager/error_manager.php');
include(__DIR__ . '/database/DatabaseManager.php');

$postdata = file_get_contents("php://input");

if (isset($postdata)) {

    $request = json_decode($postdata);
    if ($request->Operacion == "admin") {
        WO_LoginAdmin($request->Usuario, $request->Pass);
    } else if ($request->Operacion == "usuario") {
        WO_LoginUsuario($request->Usuario, $request->Pass);
    } else if ($request->Operacion == "validar") {
        WO_ValidarOperacionUsuario($request->Token);
    } else if ($request->Operacion == "validar_admin") {
        WO_ValidarOperacionAdmin($request->Token);
    }
}


/* * ***** Operaciones Web ****** */

function WO_LoginUsuario($idUsuario, $password) {
	$objUsuario = array();
	$token = '';
	$codigo = 0;
	$sql = "Select IdUsuario, PrimerNombre, PrimerApellido, PrimerIngreso " . 
           "from USUARIO " . 
           "where IdUsuario = '$idUsuario' " . 
           "and Password = '$password' " . 
           "and Activo = 1";
	$Params = array();
	$ResultSet = DatabaseManager::getQueryResult($sql, $Params);
	if($ResultSet->num_rows ==1){
		$token = GenerarToken($idUsuario);
		if($token != '') {
			$ResultRow = $ResultSet->fetch_assoc();
			$objUsuario = array(
				"IdUsuario" => $ResultRow['IdUsuario'],
				"PrimerNombre" => utf8_encode($ResultRow['PrimerNombre']),
				"PrimerApellido" => utf8_encode($ResultRow['PrimerApellido']),
				"Token" => $token
			);
			NLog("Inicio de sesión de usuario " . $idUsuario);
			$codigo = 1;
		}	
	}
	$Respuesta = array(
        "usuario" => $objUsuario,
		"codigo" => $codigo
    );
    header('Content-type: application/json');
    echo json_encode(array("respuesta" => $Respuesta));
}

function WO_LoginAdmin($USUARIO, $PASS) {
    $usuario = '';
    $exito = false;
    $TOKEN = '';
    global $configs;
    $conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
    if (!$conn->connect_error) {

        $sql = "Select IdAdmin from ADMINISTRADOR where IdAdmin = '$USUARIO' and Password = '$PASS'";

        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            NLog("Inicio de sesión de administrador " . $USUARIO);
            $TOKEN = GenerarTokenAdmin($USUARIO);
            if ($TOKEN != '') {
                $exito = true;
            }
        }
        $conn->close();
    } else {
        echo $conn->connect_error;
    }

    $RESPUESTA = array(
        "param1" => $USUARIO,
        "param2" => $TOKEN
    );
    header('Content-type: application/json');
    echo json_encode(array("respuesta" => $RESPUESTA));
}

function WO_ValidarOperacionUsuario($TOKEN) {
    $VALIDO = 0;
    if (ValidarToken($TOKEN) != '') {
        $VALIDO = 1;
    }
    header('Content-type: application/json');
    echo json_encode(array("valido" => $VALIDO));
}

function WO_ValidarOperacionAdmin($TOKEN) {
    $VALIDO = 0;
    if (ValidarTokenAdmin($TOKEN) != '') {
        $VALIDO = 1;
    }
    header('Content-type: application/json');
    echo json_encode(array("valido" => $VALIDO));
}

/* * ***** Operaciones adicionales ****** */

function GenerarToken($USUARIO) {
    $TOKEN_ENCRYPT = '';
    $TOKEN = md5(uniqid(rand(), true));
    if (GuardarTokenUsuario($USUARIO, $TOKEN) == 1) {
        $TOKEN_ENCRYPT = urlencode(base64_encode($TOKEN . '|' . $USUARIO));
        $TOKEN_ENCRYPT = str_replace("%3D", "", $TOKEN_ENCRYPT);
    }
    return $TOKEN_ENCRYPT;
}

function GenerarTokenAdmin($USUARIO) {
    $TOKEN_ENCRYPT = '';
    $TOKEN = md5(uniqid(rand(), true));
    if (GuardarTokenAdmin($USUARIO, $TOKEN) == 1) {
        $TOKEN_ENCRYPT = urlencode(base64_encode($TOKEN . '|' . $USUARIO));
        $TOKEN_ENCRYPT = str_replace("%3D", "", $TOKEN_ENCRYPT);
    }
    return $TOKEN_ENCRYPT;
}

function GuardarTokenAdmin($ADMIN, $TOKEN) {
    $RESULTADO = 0;
    global $configs;
    $conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);

    if (!$conn->connect_error) {

        $sql = "UPDATE ADMINISTRADOR SET Token = '$TOKEN', FechaToken = NOW() WHERE IdAdmin = '$ADMIN'";

        $conn->set_charset("utf8");
        if (mysqli_query($conn, $sql)) {
            $RESULTADO = 1;
        } else {
            $RESULTADO = $conn->errno;
        }
        $conn->close();
    }
    return $RESULTADO;
}

function GuardarTokenUsuario($USUARIO, $TOKEN) {
    $RESULTADO = 0;
    global $configs;
    $conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);

    if (!$conn->connect_error) {

        $sql = "UPDATE USUARIO SET Token = '$TOKEN', FechaToken = NOW() WHERE IdUsuario = '$USUARIO'";

        $conn->set_charset("utf8");
        if (mysqli_query($conn, $sql)) {
            $RESULTADO = 1;
        } else {
            $RESULTADO = $conn->errno;
        }
        $conn->close();
    }
    return $RESULTADO;
}

/*
function WO_LoginUsuario($USUARIO, $PASS) {
    $exito = false;
    $TOKEN = '';
    $NOMBRE = '';
    global $configs;
    $conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
    if (!$conn->connect_error) {

        $sql = "Select IdUsuario, PrimerNombre, PrimerApellido " . 
               "from USUARIO " . 
               "where IdUsuario = '$USUARIO' " . 
               "and Password = '$PASS' " . 
               "and Activo = 1";

        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            $ResultRow = $result->fetch_assoc();
            $NOMBRE = utf8_encode($ResultRow['PrimerNombre']) . '|' . utf8_encode($ResultRow['PrimerApellido']);
            NLog("Inicio de sesión de usuario " . $USUARIO);
            $TOKEN = GenerarToken($USUARIO);
            if ($TOKEN != '') {
                $exito = true;
            }
        }
        $conn->close();
    }

    $RESPUESTA = array(
        "param1" => $USUARIO,
        "param2" => $TOKEN,
        "param3" => $NOMBRE
    );
    header('Content-type: application/json');
    echo json_encode(array("respuesta" => $RESPUESTA));
}
*/


?>


