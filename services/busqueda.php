<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization");

$configs = include('config.php');
include('bitacora.php');
include('correo.php');
include('token.php');
include(__DIR__ . '/error-manager/error_manager.php');
include(__DIR__ . '/database/DatabaseManager.php');
include(__DIR__ . '/manager/carrera_manager.php');

$postdata = file_get_contents("php://input");

if(isset($postdata)) {
	$request = json_decode($postdata);
	if($request->Operacion == "num_busquedas"){
		WO_ObtenerNumeroBusquedas($request->Param1);
	}else if($request->Operacion == "lista_tipos"){
		WO_ObtenerListaTiposGrupos($request->Param1);
	}else if($request->Operacion == "lista_grupos"){
		WO_ObtenerGrupos($request->Param1, $request->IdTipoGrupo, $request->Prueba);
	}else if($request->Operacion == "consumir"){
		WO_ConsumirBusqueda($request->Param1, $request->IdGrupo, $request->Prueba);
	}else if($request->Operacion == "lista_busquedas"){
		WO_ObtenerBusquedas($request->Param1);
	}else if($request->Operacion == "lista_carreras"){
		WO_ObtenerCarrerasGrupo($request->Param1, $request->IdGrupo);
	}else if($request->Operacion == "favorito"){
		WO_ModificarFavorito($request->Param1, $request->IdCarrera);
	}else if($request->Operacion == "lista_favoritos"){
		WO_ObtenerFavoritos($request->Param1, $request->IdGrupo);
	}else if($request->Operacion == "detalle"){
		WO_ObtenerDetalleBusqueda($request->Param1, $request->IdCarrera);
	}else if($request->Operacion == "votar"){
		WO_CalificarCarrera($request->Param1, $request->IdCarrera, $request->Nota);
	}else if($request->Operacion == "lista_departamentos"){ //Lista de departamentos para filtro de búsqueda
		WO_ListaDepartamentos($request->Param1);
	}else if($request->Operacion == "lista_municipios"){ //Lista de municipios para filtro de búsqueda
		WO_ListaMunicipios($request->Param1, $request->IdDepartamento);
	}else if($request->Operacion == "pensum"){ //Lista de municipios para filtro de búsqueda
		WO_ObtenerPensumCarrera($request->Param1, $request->IdCarrera);
	}
}


function WO_ObtenerPensumCarrera($TokenSesion, $IdCarrera){
	$out_Pensum = array();
	$out_Exito = false;
	$out_Mensaje = "";

	if( SesionDeUsuarioAutenticada($TokenSesion) ){
	//if(true){
			$out_Pensum = CarreraManager::ObtenerPensum($IdCarrera);
			if(!is_null($out_Pensum)) $out_Exito = true;
	}
	if( (!$out_Exito) && ($out_Mensaje == "") ) $out_Mensaje = 'Error obteniendo pensum de la carrera';

	$Respuesta = array(
		"Exito" => $out_Exito,
		"Pensum" => $out_Pensum
	);
	header('Content-type: application/json');
	echo json_encode(array("Respuesta"=>$Respuesta));
}

/* ****** Operaciones Web ****** */
//WO_ListaMunicipios('token','1001');
function WO_ListaMunicipios($Token, $IdDepartamento){
	//Variables de sálida
	$out_Municipios = array();
	$out_Exito = false;
	$out_Mensaje = "";

	if(SesionDeUsuarioAutenticada($Token)){
	//if(true){
		$Sql = "SELECT 	IdMunicipio, Descripcion FROM MUNICIPIO " .
					 "WHERE 	IdDepartamento = ? ";
		$Params = array(
			intval($IdDepartamento)
		);
		$ResultSet = DatabaseManager::getQueryResult($Sql, $Params);
		if($ResultSet->num_rows > 0){
			while($ResultRow = $ResultSet->fetch_assoc()) {
				$out_Municipios[] = array(
					"Id" => $ResultRow['IdMunicipio'],
					"Nombre" => utf8_encode($ResultRow['Descripcion'])
				);
			}
			$out_Exito = true;
		}
	}
	if( (!$out_Exito) && ($out_Mensaje=="") ) $out_Mensaje = "Error obteniendo municipios"; //Error general en obtención de departamentos
	$Respuesta = array(
		"Exito" => $out_Exito,
		"Mensaje" => $out_Mensaje,
		"Municipios" => $out_Municipios
	);
	header('Content-type: application/json');
	echo json_encode(array("Respuesta"=>$Respuesta));
}

//WO_ListaDepartamentos('token');
function WO_ListaDepartamentos($Token){
	//Variables de sálida
	$out_Departamentos = array();
	$out_Exito = false;
	$out_Mensaje = "";

	if(SesionDeUsuarioAutenticada($Token)){
	//if(true){
		$Sql = "SELECT IdDepartamento, Nombre FROM DEPARTAMENTO";
		$Params = array();
		$ResultSet = DatabaseManager::getQueryResult($Sql, $Params);
		if($ResultSet->num_rows > 0){
			while($ResultRow = $ResultSet->fetch_assoc()) {
				$out_Departamentos[] = array(
					"Id" => $ResultRow['IdDepartamento'],
					"Nombre" => utf8_encode($ResultRow['Nombre'])
				);
			}
			$out_Exito = true;
		}
	}
	if( (!$out_Exito) && ($out_Mensaje=="") ) $out_Mensaje = "Error obteniendo departamentos"; //Error general en obtención de departamentos
	$Respuesta = array(
		"Exito" => $out_Exito,
		"Mensaje" => $out_Mensaje,
		"Departamentos" => $out_Departamentos
	);
	header('Content-type: application/json');
	echo json_encode(array("Respuesta"=>$Respuesta));
}


function WO_CalificarCarrera($TOKEN, $ID_CARRERA, $NOTA){
	$CODIGO = 0;
	$MENSAJE = '';
	global $configs;
	$ID_USUARIO = ValidarToken($TOKEN);
	$NOTA_TOTAL = 0;
	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			//Validar que nota sea valida
			if((intval($NOTA) <= 5) || (intval($NOTA) >= 1)){
				//Validar si existe calificacion previa del usuario a la carrera indicada
				$EXISTE_CALIF = false;
				$sql = "SELECT COUNT(1) AS NUM FROM CALIFICACION WHERE IdUsuario = ? AND IdCarrera = ?";
				$query = $conn->prepare($sql);
				$query->bind_param('si', $ID_USUARIO, $ID_CARRERA);
				$query->execute();
				$resultado = $query->get_result();
				if($resultado->num_rows==1){
					$total = $resultado->fetch_assoc();
					$NUM_CALIFICACIONES = intval($total['NUM']);
					if($NUM_CALIFICACIONES > 0){
						$EXISTE_CALIF = true;
					}
				}

				if($EXISTE_CALIF){
					//Existe calificacion previa, se debe actualizar el registro anterior
					$sql = "UPDATE CALIFICACION SET Nota = ? WHERE IdUsuario = ? AND IdCarrera = ? ";
				}else{
					//No existe calificacion previa, se debe ingresar un nuevo registro
					$sql = "INSERT INTO CALIFICACION (Nota, IdUsuario, IdCarrera) VALUES(?,?,?)";
				}

				$query = $conn->prepare($sql);
				$query->bind_param("isi", $NOTA, $ID_USUARIO, $ID_CARRERA);
				if($query->execute()){
					$CODIGO = 1;
					$NOTA_TOTAL = ObtenerNotaTotal($ID_CARRERA);
				}
			}
		}else{
			$MENSAJE = $configs['err_comunicacion'];
		}
	}
	if(($CODIGO == 0) && ($MENSAJE == '')){
		$MENSAJE = 'Error agregando calificación, favor intente nuevamente';
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"mensaje" => $MENSAJE,
		"NotaTotal" => $NOTA_TOTAL
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_ObtenerDetalleBusqueda($Token, $IdCarrera){
	$out_Exito = false;
	$out_Carrera = array();
	$out_Mensaje = "";

	$TotalBusquedas = 0;
	if( SesionDeUsuarioAutenticada($Token) ){
  //if(true){
		//Validar que usuario tenga busqueda consumida para id grupo recibido
		$IdUsuario = ObtenerIdUsuarioDeToken($Token);
		//$IdUsuario = 'AUGUSTO90';
		$Sql = 	"SELECT COUNT(1) AS TOTAL_BUSQUEDAS " .
				"FROM 	BUSQUEDA B, GRUPO G, CARRERA C " .
				"WHERE	B.ID_GRUPO = G.IdGrupo " .
				"AND	G.IdGrupo = C.Grupo " .
				"AND	B.ID_USUARIO = ? " .
				"AND	C.IdCarrera = ? " .
				"AND	B.ESTADO = 0 " .
				"AND	B.FECHA_UTILIZACION > NOW() - INTERVAL 1 WEEK";
		$Params = array(
			$IdUsuario,
			intval($IdCarrera)
		);
		$ResultSet = DatabaseManager::getQueryResult($Sql, $Params);
		if($ResultSet->num_rows==1){
			$ResultRow = $ResultSet->fetch_assoc();
			$TotalBusquedas = intval($ResultRow['TOTAL_BUSQUEDAS']);
		}
		if($TotalBusquedas>0){ //El usuario tiene una búsqueda para el grupo del id de carrera ingresado
			$Sql = 	"SELECT	C.IdCarrera, C.Nombre AS Nombre, C.Descripcion, G.Nombre AS Grupo, U.Nombre AS Universidad, " .
							"DEP.Nombre AS Departamento, MUNI.Descripcion AS Municipio, " .
							"U.Localidad, U.Telefono1, U.Telefono2, U.Url, " .
							"C.CostoMensual, C.CostoMatricula, C.CostoInscripcion, C.DetallesPago, C.DetallesPromociones, " .
							"U.CostoParqueo, U.Disponibilidad AS DispParqueo, U.DetalleParqueo, " .
							"C.Modalidad, C.NombreFacultad, C.TelefonoFacultad, " .
							"CASE " .
								"WHEN C.AplicaBeca = 1 THEN 'Si' ELSE 'No' " .
							"END AS AplicaBeca, " .
							"C.DetallesBeca, " .
							"CASE  " .
								"WHEN C.ReqTesis = 1 THEN 'Si' ELSE 'No'  " .
							"END AS ReqTesis, " .
							"CASE " .
								"WHEN C.ReqPrivado = 1 THEN 'Si' ELSE 'No' " .
							"END AS ReqPrivado, " .
							"CASE " .
								"WHEN C.ReqMaestria = 1 THEN 'Si' ELSE 'No' " .
							"END AS ReqMaestria, " .
							"CASE " .
								"WHEN C.ReqCreditos = 1 THEN 'Si' ELSE 'No' " .
							"END AS ReqCreditos, " .
							"CASE " .
								"WHEN C.ReqEps = 1 THEN 'Si' ELSE 'No' " .
							"END AS ReqEps, " .
							"CASE " .
								"WHEN Certi_Internacional = 1 THEN 'Si' Else 'No' " .  //marodriguez 20180422
							"END AS Certi_Internacional, " .
							"CASE " .
								"WHEN ModalidadPresencial = 1 THEN 'Si' Else 'No' " .
							"END AS ModalidadPresencial, " .
							"CASE " .
								"WHEN ModalidadEnLinea = 1 THEN 'Si' Else 'No' " .
							"END AS ModalidadEnLinea, " .
							"CASE " .
								"WHEN ModalidadSemiPresencial = 1 THEN 'Si' Else 'No' " .
							"END AS ModalidadSemiPresencial, " .  //marodriguez 20180422
							"C.DetallesCierre, " .
							"CASE WHEN C.HIniMat != '' AND C.HFinMat !='' THEN concat(C.HIniMat, ' - ', C.HFinMat)  ELSE 'N/D' END AS JornadaMatutina, " .
							"CASE WHEN C.HIniVesp != '' AND C.HFinVesp !='' THEN concat(C.HIniVesp, ' - ', C.HIniVesp) ELSE 'N/D' END AS JornadaVespertina, " .
							"CASE WHEN C.HIniNoc != '' AND C.HFinNoc !='' THEN concat(C.HIniNoc, ' - ', C.HFinNoc) ELSE 'N/D' END AS JornadaNocturna, " .
							"CASE WHEN C.HIniSab != '' AND C.HFinSab !='' THEN concat(C.HIniSab, ' - ', C.HFinSab) ELSE 'N/D' END AS JornadaSabatina, " .
							"CASE WHEN C.HIniDom != '' AND C.HFinDom !='' THEN concat(C.HIniDom, ' - ', C.HFinDom) ELSE 'N/D' END AS JornadaDominical, " .
							"C.DetallesNuevoIngreso AS AdminisionNuevoIngreso, " .
							"C.DetallesReingreso AS AdmisionReingreso, " .
							"C.DetallesContinuacion AS AdmisionContinuacion, " .
							"C.DetallesTraslado AS AdmisionTraslado, " .
							"C.DetallesExtranjero AS AdmisionExtranjero, " .
							"C.CorreoFacultad, " .  //marodriguez 20180422
							"C.DescripcionHorarios, " . //marodriguez 20180422
							"C.DescripcionModalidad " .  //marodriguez 20180422
							"FROM	CARRERA C, GRUPO G, UNIVERSIDAD U, CARRERAXUNI CU, DEPARTAMENTO DEP, MUNICIPIO MUNI " .
							"WHERE	G.IdGrupo = C.Grupo	 " .
							"AND	C.IdCarrera = ? " .
							"AND	U.IdUniversidad = CU.IdUniversidad " .
							"AND	C.IdCarrera = CU.IdCarrera " .
							"AND	U.Departamento = DEP.IdDepartamento " .
							"AND	U.Municipio = MUNI.IdMunicipio " .
							"AND	MUNI.IdDepartamento = DEP.IdDepartamento";
			$Params = array(
				intval($IdCarrera)
			);
			$ResultSet = DatabaseManager::getQueryResult($Sql, $Params);
			if($ResultSet->num_rows==1){
				$ResultRow = $ResultSet->fetch_assoc();
				$out_Carrera = array(
					"Nombre" => utf8_encode($ResultRow['Nombre']),
					"Descripcion" => utf8_encode($ResultRow['Descripcion']),
					"Grupo" => utf8_encode($ResultRow['Grupo']),
					"Universidad" => utf8_encode($ResultRow['Universidad']),
					"Departamento" => utf8_encode($ResultRow['Departamento']),
					"Municipio" => utf8_encode($ResultRow['Municipio']),
					"Localidad" => utf8_encode($ResultRow['Localidad']),
					"Telefono1" => utf8_encode($ResultRow['Telefono1']),
					"Telefono2" => utf8_encode($ResultRow['Telefono2']),
					"Url" => utf8_encode($ResultRow['Url']),
					"CostoMensual" => floatval($ResultRow['CostoMensual']),
					"CostoMatricula" => floatval($ResultRow['CostoMatricula']),
					"CostoInscripcion" => floatval($ResultRow['CostoInscripcion']),
					"DetallesPago" => utf8_encode($ResultRow['DetallesPago']),
					"DetallesPromociones" => utf8_encode($ResultRow['DetallesPromociones']),
					"CostoParqueo" => floatval($ResultRow['CostoParqueo']),
					"DispParqueo" => intval($ResultRow['DispParqueo']),
					"DetalleParqueo" => utf8_encode($ResultRow['DetalleParqueo']),
					"NotaIndividual" => ObtenerNotaIndividual($IdUsuario, $IdCarrera),
					"NotaTotal" => ObtenerNotaTotal($IdCarrera),
					"TotalVotos" =>  ObtenerTotalVotos($IdCarrera),
					"Modalidad" => $ResultRow['Modalidad'],
					"NombreFacultad" => $ResultRow['NombreFacultad'],
					"TelefonoFacultad" => $ResultRow['TelefonoFacultad'],
					"AplicaBeca" => $ResultRow['AplicaBeca'],
					"DetallesBeca" => $ResultRow['DetallesBeca'],
					"ReqTesis" => $ResultRow['ReqTesis'],
					"ReqPrivado" => $ResultRow['ReqPrivado'],
					"ReqMaestria" => $ResultRow['ReqMaestria'],
					"ReqCreditos" => $ResultRow['ReqCreditos'],
					"ReqEps" => $ResultRow['ReqEps'],
					"DetallesCierre" => $ResultRow['DetallesCierre'],
					"JornadaMatutina" => $ResultRow['JornadaMatutina'],
					"JornadaVespertina" => $ResultRow['JornadaVespertina'],
					"JornadaNocturna" => $ResultRow['JornadaNocturna'],
					"JornadaSabatina" => $ResultRow['JornadaSabatina'],
					"JornadaDominical" => $ResultRow['JornadaDominical'],
					"AdminisionNuevoIngreso" => utf8_encode($ResultRow['AdminisionNuevoIngreso']),
					"AdmisionReingreso" => utf8_encode($ResultRow['AdmisionReingreso']),
					"AdmisionContinuacion" =>utf8_encode($ResultRow['AdmisionContinuacion']),
					"AdmisionTraslado" => utf8_encode($ResultRow['AdmisionTraslado']),
					"AdmisionExtranjero" => utf8_encode($ResultRow['AdmisionExtranjero']),
					"CorreoFacultad" => utf8_encode($ResultRow['CorreoFacultad']),
					"Certi_Internacional" => utf8_encode($ResultRow['Certi_Internacional']),
					"DescripcionHorarios" => utf8_encode($ResultRow['DescripcionHorarios']),
					"ModalidadPresencial" => utf8_encode($ResultRow['ModalidadPresencial']),
					"ModalidadSemiPresencial" => utf8_encode($ResultRow['ModalidadSemiPresencial']),
					"ModalidadEnLinea" => utf8_encode($ResultRow['ModalidadEnLinea']),
					"DescripcionModalidad" => utf8_encode($ResultRow['DescripcionModalidad'])
				);
				$out_Exito = true;
			}
		}else{
			$out_Mensaje = "No se pudo encontrar una búsqueda activa asociada a su usuario, favor contactar al administrador";
		}
	}
	if( (!$out_Exito) && ($out_Mensaje == '') ) $out_Mensaje = "Error al obtener detalle de carrera";

	$Respuesta = array(
		"Exito" => $out_Exito,
		"Carrera" => $out_Carrera,
		"Mensaje" => $out_Mensaje
	);
	header('Content-type: application/json');
	echo json_encode(array("Respuesta"=>$Respuesta));
}


function WO_ObtenerFavoritos($TOKEN,$ID_GRUPO){
	$CODIGO = 0;
	$FAVORITOS = array();
	global $configs;
	$ID_USUARIO = ValidarToken($TOKEN);
	$TOTAL_BUSQUEDAS = 0;
	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			//Validar que usuario tenga busqueda consumida para id grupo recibido
			$sql = "SELECT	COUNT(1) AS TOTAL_BUSQUEDAS " .
					"FROM 	BUSQUEDA " .
					"WHERE	ID_USUARIO = ? " .
					"AND		ID_GRUPO = ? " .
					"AND		ESTADO = 0";
			$query = $conn->prepare($sql);
			$query->bind_param('si',$ID_USUARIO, $ID_GRUPO);
			$query->execute();
			$resultado = $query->get_result();
			if($resultado->num_rows==1){
				$total = $resultado->fetch_assoc();
				$TOTAL_BUSQUEDAS = intval($total['TOTAL_BUSQUEDAS']);
			}

			if($TOTAL_BUSQUEDAS>0){
				//Obtener listado de carreras del grupo consultado que SI esten asignadas a una universidad determinada
				$sql = "SELECT	C.IdCarrera, C.Nombre AS Carrera, U.Nombre AS Universidad, DEP.Nombre AS Departamento, MUNI.Descripcion AS Municipio, U.Imagen " .
						"FROM	CARRERA C, FAVORITO F, UNIVERSIDAD U, CARRERAXUNI CU, DEPARTAMENTO DEP, MUNICIPIO MUNI " .
						"WHERE	C.IdCarrera = F.IdCarrera " .
						"AND		CU.IdCarrera = C.IdCarrera " .
						"AND		CU.IdUniversidad = U.IdUniversidad " .
						"AND		U.Departamento = DEP.IdDepartamento " .
						"AND		U.Municipio = MUNI.IdMunicipio " .
						"AND		U.Departamento = DEP.IdDepartamento " .
						"AND		F.IdUsuario = ? " .
						"AND		C.Grupo = ? ";

				$query = $conn->prepare($sql);
				$query->bind_param('si',$ID_USUARIO, $ID_GRUPO);
				$query->execute();
				$resultado = $query->get_result();
				if ($resultado->num_rows > 0) {
					while($favorito = $resultado->fetch_assoc()) {
						$NotaTotal = ObtenerNotaTotal($favorito['IdCarrera']);
						$FAVORITOS[] = array(
							"Id" => $favorito['IdCarrera'],
							"Carrera" => utf8_encode($favorito['Carrera']),
							"Universidad" => utf8_encode($favorito['Universidad']),
							"Departamento" => utf8_encode($favorito['Departamento']),
							"Municipio" => utf8_encode($favorito['Municipio']),
							"Nota" => $NotaTotal,
							"Imagen" => $favorito['Imagen']
						);
					}
					$CODIGO = 1;
				}
			}else{
				$CODIGO = 2;
			}
		}


	}
	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"favoritos" => $FAVORITOS
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_ModificarFavorito($TOKEN, $ID_CARRERA){
	$CODIGO = 0;
	global $configs;
	$ID_USUARIO = ValidarToken($TOKEN);
	$FAVORITO = 0;
	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			//Validar si existe o no como favorito
			$sql = "SELECT COUNT(1) AS EXISTE FROM FAVORITO WHERE IdUsuario = ? AND IdCarrera = ? ";
			$query = $conn->prepare($sql);
			$query->bind_param('si',$ID_USUARIO, $ID_CARRERA);
			$query->execute();
			$resultado = $query->get_result();
			if($resultado->num_rows==1){
				$total = $resultado->fetch_assoc();
				$FAVORITO = intval($total['EXISTE']);
			}
			if($FAVORITO > 0){
				//Ya es favorito
				$sql = "DELETE FROM FAVORITO WHERE IdUsuario = ? AND IdCarrera = ?;";
			}else{
				//No es favorito
				$sql = "INSERT INTO FAVORITO (IdUsuario, IdCarrera) VALUES (?, ?);";
			}
			//echo $sql;
			$query = $conn->prepare($sql);
			$query->bind_param('si', $ID_USUARIO, $ID_CARRERA);
			if($query->execute()){
				$CODIGO = 1;
			}
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}

//WO_ObtenerCarrerasGrupo('token','16');
function WO_ObtenerCarrerasGrupo($Token, $IdGrupo){
	//Variables de sálida
	$out_Exito = 0;
	$out_Mensaje = "";
	$out_Busquedas = array();

	//if(SesionDeUsuarioAutenticada($Token)){
	if(true){
		//$IdUsuario = ObtenerIdUsuarioDeToken($Token);
		$IdUsuario = 'AUGUSTO90';

		$Sql = "SELECT	COUNT(1) AS TOTAL_BUSQUEDAS " .
					 "FROM 	BUSQUEDA " .
					 "WHERE	ID_USUARIO = ? " .
					 "AND		ID_GRUPO = ? " .
					 "AND		ESTADO = 0 ";
		$Params = array(
				$IdUsuario,
				intval($IdGrupo)
		);
		$TotalBusquedas = 0;
		$ResultSet = DatabaseManager::getQueryResult($Sql, $Params);
		if($ResultSet->num_rows == 1){
			$ResultRow = $ResultSet->fetch_assoc();
			$TotalBusquedas = intval($ResultRow['TOTAL_BUSQUEDAS']);
		}

		if($TotalBusquedas > 0){ //Valida si el usuario en efecto tiene búsquedas para el grupo indicado
			$Sql = 	"SELECT	C.IdCarrera AS IdCarrera, C.Nombre AS Carrera, U.Nombre AS Universidad, " .
							"DEP.Nombre AS Departamento, DEP.IdDepartamento AS IdDepartamento, " .
			        "MUNI.Descripcion AS Municipio, MUNI.IdMunicipio AS IdMunicipio, " .
			        "CASE " .
									"WHEN C.IdCarrera IN ( SELECT IdCarrera FROM	FAVORITO WHERE	IdUsuario = ? ) THEN 1 " .
						            "ELSE 0 " .
			        "END AS Favorito, " .
			        "CASE " .
									"WHEN C.IdCarrera IN ( SELECT IdCarrera FROM 	CALIFICACION ) " .
										"THEN ( SELECT 	AVG(Nota) FROM	CALIFICACION WHERE	IdCarrera = C.IdCarrera ) " .
						            "ELSE 0 " .
			        "END AS NotaTotal, " .
			        "C.CostoMensual, " .
			        "CASE " .
									"WHEN C.IdCarrera IN ( SELECT IdCarrera FROM PERIODO ) " .
										"THEN ( SELECT COUNT(1) FROM PERIODO WHERE IdCarrera = C.IdCarrera) * C.Duracion_Periodo_Meses " .
									"ELSE 0 " .
			        "END AS Duracion_Meses, " .
							"CASE " .
								"WHEN ( C.HIniMat != '' AND C.HFinMat != '' ) THEN 1 " .
					            "ELSE 0 " .
			        "END AS JornadaMatutina, " .
			        "CASE " .
								"WHEN ( C.HIniVesp != '' AND C.HFinVesp != '' ) THEN 1 " .
					            "ELSE 0 " .
			        "END AS JornadaVespertina, " .
			        "CASE " .
								"WHEN ( C.HIniNoc != '' AND C.HFinNoc != '' ) THEN 1 " .
					            "ELSE 0 " .
			        "END AS JornadaNocturna, " .
							"CASE " .
								"WHEN ( C.HIniSab != '' AND C.HFinSab != '' ) THEN 1 " .
					            "ELSE 0 " .
			        "END AS JornadaSabatina, " .
			        "CASE " .
								"WHEN ( C.HIniDom != '' AND C.HFinDom != '' ) THEN 1 " .
					            "ELSE 0 " .
			        "END AS JornadaDominical, " .
							"U.Imagen " .
							"FROM	CARRERA C, UNIVERSIDAD U, CARRERAXUNI CU, DEPARTAMENTO DEP, MUNICIPIO MUNI " .
							"WHERE	C.Grupo = ? " .
							"AND		C.IdCarrera = CU.IdCarrera " .
							"AND		U.IdUniversidad = CU.IdUniversidad " .
							"AND		U.Departamento = DEP.IdDepartamento " .
							"AND		U.Municipio = MUNI.IdMunicipio " .
							"AND		DEP.IdDepartamento = MUNI.IdDepartamento";
			//echo $Sql;
			$Params = array(
					$IdUsuario,
					intval($IdGrupo)
			);
			$ResultSet = DatabaseManager::getQueryResult($Sql, $Params);
			if ($ResultSet->num_rows > 0) {
				while($ResultRow = $ResultSet->fetch_assoc()) {
					$out_Busquedas[] = array(
						"Id" => $ResultRow['IdCarrera'],
						"Carrera" => utf8_encode($ResultRow['Carrera']),
						"Universidad" => utf8_encode($ResultRow['Universidad']),
						"Departamento" => utf8_encode($ResultRow['Departamento']),
						"IdDepartamento" => intval($ResultRow['IdDepartamento']),
						"Municipio" => utf8_encode($ResultRow['Municipio']),
						"IdMunicipio" => intval($ResultRow['IdMunicipio']),
						"Favorito" => intval($ResultRow['Favorito']),
						"Nota" => round(floatval($ResultRow['NotaTotal'])),
						"Imagen" => $ResultRow['Imagen'],
						"Duracion" => $ResultRow['Duracion_Meses'],
						"CostoMensual" => floatval($ResultRow['CostoMensual']),
						"JornadaMatutina" => intval($ResultRow['JornadaMatutina']),
						"JornadaVespertina" => intval($ResultRow['JornadaVespertina']),
						"JornadaNocturna" => intval($ResultRow['JornadaNocturna']),
						"JornadaSabatina" => intval($ResultRow['JornadaSabatina']),
						"JornadaDominical" => intval($ResultRow['JornadaDominical'])
					);
				}
				$out_Exito = true;
			}
		}else{
			$out_Mensaje = "No se han detectado búsquedas asociadas a la agrupación seleccionada";
		}
	}
	if( (!$out_Exito)  && ($out_Mensaje=="") ) $out_Mensaje = "Error obteniendo resultados de búsqueda";
	$Respuesta = array(
		"Exito" => $out_Exito,
		"Mensaje" => $out_Mensaje,
		"Busquedas" => $out_Busquedas
	);
	header('Content-type: application/json');
	echo json_encode(array("Respuesta"=>$Respuesta));

}


function WO_ObtenerBusquedas($TOKEN){
	$CODIGO = 0;
	$BUSQUEDAS = array();
	global $configs;
	$ID_USUARIO = ValidarToken($TOKEN);
	ActualizarTokenUsuario($ID_USUARIO);
	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql = "SELECT	G.Nombre, G.IdGrupo AS Id, DATE_FORMAT(B.FECHA_UTILIZACION, '%Y/%m/%d %T') AS FechaBusqueda, " .
				"CASE " .
					"WHEN B.FECHA_UTILIZACION < NOW() - INTERVAL 1 WEEK THEN 'Vencida' " .
					"ELSE 'Activa' " .
				"END AS Estado " .
				"FROM	BUSQUEDA B, GRUPO G " .
				"WHERE	B.ID_GRUPO = G.IdGrupo " .
				"AND	B.ID_USUARIO = ? " .
				"AND	B.ESTADO = 0";
			$query = $conn->prepare($sql);
			$query->bind_param('s', $ID_USUARIO);
			$query->execute();
			$result = $query->get_result();
			if ($result->num_rows > 0) {
				while($busqueda = $result->fetch_assoc()) {
					$BUSQUEDAS[] = array(
						"Id" => $busqueda['Id'],
						"Nombre" => utf8_encode($busqueda['Nombre']),
						"Fecha" => $busqueda['FechaBusqueda'],
						"Estado" => $busqueda['Estado']
					);
				}
			}
			$CODIGO = 1;
		}
	}

	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"busquedas" => $BUSQUEDAS
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_ConsumirBusqueda($TOKEN, $GRUPO, $PRUEBA){
	$CODIGO = 0;
	$MENSAJE = '';
	global $configs;
	$ID_USUARIO = ValidarToken($TOKEN);
	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){

			if (!$conn->query("SET @id_usuario = '" . $ID_USUARIO . "'")
				|| !$conn->query("SET @id_grupo = " . $GRUPO . "")
				|| !$conn->query("SET @prueba = " . $PRUEBA . "")
				|| !$conn->query("SET @respuesta = 0")
				|| !$conn->query("CALL SP_CONSUMIR_BUSQUEDA(@id_usuario, @id_grupo, @prueba, @respuesta)")) {
				echo "Falló CALL: (" . $conn->errno . ") " . $conn->error;
			}

			if (!($resultado = $conn->query("SELECT @respuesta as respuesta"))) {
				echo "Falló la obtención: (" . $conn->errno . ") " . $conn->error;
			}
			$fila = $resultado->fetch_assoc();
			$CODIGO = intval($fila['respuesta']);
			if($CODIGO==1){
				$MENSAJE = 'Busqueda realizada exitosamente';
			}else if($CODIGO==2){
				$MENSAJE = 'Ya se tiene una b&uacute;squeda activa para el grupo indicado';
			}else if($CODIGO==3){
				$MENSAJE = 'No se han podido encontrar b&uacute;squedas disponibles';
			}

			$conn->close();
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"mensaje" => utf8_encode($MENSAJE)
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_ObtenerGrupos($TOKEN,$ID_TIPO_GRUPO, $PRUEBA){
	$CODIGO = 0;
	$GRUPOS = array();
	global $configs;
	$ID_USUARIO = ValidarToken($TOKEN);
	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql = "SELECT	IdGrupo, Nombre  " .
				"FROM 	GRUPO " .
				"WHERE 	TipoGrupo = ? " .
				"AND NOT EXISTS( " .
					"SELECT 1 FROM BUSQUEDA AS B " .
					"WHERE	B.ID_USUARIO = ? " .
					"AND	ESTADO = 0 " .
					"AND	B.ID_GRUPO = GRUPO.IdGrupo " .
					"AND 	B.FECHA_UTILIZACION > NOW() - INTERVAL 1 WEEK " .
				") " .
				"AND	EXISTS( " .
					"SELECT 	1 " .
					"FROM 	CARRERA C, CARRERAXUNI CU " .
					"WHERE 	C.Grupo = GRUPO.IdGrupo " .
					"AND		C.IdCarrera = CU.IdCarrera " .
				")";
			if($PRUEBA=="1"){
				$sql = $sql . " AND Prueba = 1";
			}
			$query = $conn->prepare($sql);
			$query->bind_param('is',$ID_TIPO_GRUPO, $ID_USUARIO);
			$query->execute();
			$result = $query->get_result();
			if ($result->num_rows > 0) {
				while($grupo = $result->fetch_assoc()) {
					$GRUPOS[] = array(
						"Id" => $grupo['IdGrupo'],
						"Nombre" => utf8_encode($grupo['Nombre'])
					);
				}
			}
			$CODIGO = 1;
		}
	}

	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"grupos" => $GRUPOS
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_ObtenerListaTiposGrupos($TOKEN){
	$CODIGO = 0;
	$TIPOS = array();
	global $configs;
	$ID_USUARIO = ValidarToken($TOKEN);
	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$query = $conn->prepare("SELECT IdTipoGrupo, Descripcion FROM TIPO_GRUPO");
			$query->execute();
			$result = $query->get_result();
			if ($result->num_rows > 0) {
				while($tipo = $result->fetch_assoc()) {
					$TIPOS[] = array(
						"Id" => $tipo['IdTipoGrupo'],
						"Descripcion" => utf8_encode($tipo['Descripcion'])
					);
				}
				$CODIGO = 1;
			}
		}
	}

	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"tipos" => $TIPOS
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_ObtenerNumeroBusquedas($TOKEN){
	$NUM_BUSQUEDAS = 0;
	$NUM_BUSQUEDAS_PRUEBA = 0;
	$CODIGO = 0;
	global $configs;
	$ID_USUARIO = ValidarToken($TOKEN);
	if($ID_USUARIO!=''){
		//Si el token es válido realizar el conteo de búsquedas
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql = 	"SELECT COUNT(1) AS Conteo FROM BUSQUEDA WHERE ID_USUARIO = '$ID_USUARIO' AND ESTADO = 1 AND PRUEBA = 0";
			$result = $conn->query($sql);
			if ($result->num_rows == 1) {
				$informacion = $result->fetch_assoc();
				$NUM_BUSQUEDAS = $informacion['Conteo'];
				$CODIGO = 1;
			}
			//Busquedas de prueba
			$sql = 	"SELECT COUNT(1) AS Conteo FROM BUSQUEDA WHERE ID_USUARIO = '$ID_USUARIO' AND ESTADO = 1 AND PRUEBA = 1";
			$result = $conn->query($sql);
			if ($result->num_rows == 1) {
				$informacion = $result->fetch_assoc();
				$NUM_BUSQUEDAS_PRUEBA = $informacion['Conteo'];
				$CODIGO = 1;
			}

			$conn->close();
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"numbusquedas" => intval($NUM_BUSQUEDAS),
		"numbusquedasprueba" => intval($NUM_BUSQUEDAS_PRUEBA)
	);

	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));

}


/* ****** Operaciones adicionales ****** */


function ObtenerNotaTotal($ID_CARRERA){
	$NOTA = 0;

	global $configs;
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$conn->connect_error){
		$sql = "SELECT AVG(Nota) AS Nota FROM CALIFICACION WHERE IdCarrera = ?";
		$query = $conn->prepare($sql);
		$query->bind_param('i',$ID_CARRERA);
		$query->execute();
		$resultado = $query->get_result();
		if($resultado->num_rows==1){
			$nota = $resultado->fetch_assoc();
			$NOTA = round(floatval($nota["Nota"]));
		}
	}

	return $NOTA;
}


function ObtenerNotaIndividual($ID_USUARIO, $ID_CARRERA){
	$NOTA = 0;

	global $configs;
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$conn->connect_error){
		$sql = "SELECT Nota FROM CALIFICACION WHERE IdUsuario = ? AND IdCarrera = ?";
		$query = $conn->prepare($sql);
		$query->bind_param('si',$ID_USUARIO, $ID_CARRERA);
		$query->execute();
		$resultado = $query->get_result();
		if($resultado->num_rows==1){
			$nota = $resultado->fetch_assoc();
			$NOTA = intval($nota["Nota"]);
		}
	}

	return $NOTA;
}


function ObtenerTotalVotos($ID_CARRERA){
	$TOTAL_VOTOS = 0;

	global $configs;
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$conn->connect_error){
		$sql = "SELECT COUNT(1) AS Total FROM CALIFICACION WHERE IdCarrera = ?";
		$query = $conn->prepare($sql);
		$query->bind_param('i', $ID_CARRERA);
		$query->execute();
		$resultado = $query->get_result();
		if($resultado->num_rows==1){
			$total = $resultado->fetch_assoc();
			$TOTAL_VOTOS = intval($total["Total"]);
		}
	}

	return $TOTAL_VOTOS;
}



?>
