<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization");

$configs = include('config.php');
include('bitacora.php');
include('correo.php');
include('token.php');
include(__DIR__ . '/error-manager/error_manager.php');
include(__DIR__ . '/database/DatabaseManager.php');


$postdata = file_get_contents("php://input");

if(isset($postdata)) {
	$request = json_decode($postdata);
	if($request->Operacion == "registrar"){
		WO_RegistrarUsuario($request->Usuario);
	}else if($request->Operacion == "activar"){
		WO_ActivarUsuario($request->IdUsuario, $request->Token);
	}else if($request->Operacion == "codact"){
		$RESULTADO = WO_EnviarCorreoActivacion($request->IdUsuario, $request->Correo);
		echo $RESULTADO;
	}else if($request->Operacion == "editar"){
		WO_EditarUsuario($request->Usuario, $request->Param1);
	}else if($request->Operacion == "detalle"){
		WO_DetalleUsuario($request->Param1);
	}else if($request->Operacion == "cambiopass"){
		WO_CambioPass($request->Param1, $request->PassActual , $request->NuevoPass);
	}else if($request->Operacion == "recordatorio"){
		WO_EnviarRecordatorioContrasena($request->TextoBusqueda);
	}else if($request->Operacion == "pregunta"){
		WO_EnviarPregunta($request->Param1, $request->Pregunta);
	}else if($request->Operacion == "lista"){
		WO_Adm_ObtenerUsuarios($request->Param1);
	}else if($request->Operacion == "detalle_admin"){
		WO_DetalleUsuarioAdmin($request->Param1, $request->IdUsuario);
	}else if($request->Operacion == 'act-primer-ingreso'){
		// marodriguez 20190727 registrar primer ingreso de usuario
		WO_ActPrimerIngreso($request->Param1);
	}
}


// marodriguez 20190727
function WO_ActPrimerIngreso($Token){
	header('Content-type: application/json');
	$Codigo = 0;
	$IdUsuario = ValidarToken($Token);
	if ($IdUsuario == '') {
		echo json_encode(array("respuesta"=> array("codigo" => 0) ));
	}

	$Sql = "UPDATE	USUARIO " .
		   "SET  	PrimerIngreso = 0 " . 
		   "WHERE	IdUsuario = " . $IdUsuario;
	
	if (DatabaseManager::executeQuery($Sql, $Params)) {
		$Codigo = 1;
	}

	echo json_encode(array("respuesta"=> array("codigo" => $Codigo) ));
}

/* ****** Operaciones Web ****** */
//WO_Adm_ObtenerUsuarios('token');
function WO_Adm_ObtenerUsuarios($TOKEN){
	$CODIGO = 0;
	$USUARIOS = array();
	global $configs;
	$IdAdmin = ValidarTokenAdmin($TOKEN);
	$TOTAL_BUSQUEDAS = 0;
	if($IdAdmin!=''){
		$Sql = "SELECT	IdUsuario, CONCAT_WS(' ', PrimerNombre, SegundoNombre, PrimerApellido, SegundoApellido) AS Nombre, " .
		"Correo, TelefonoCelular " .
		"FROM	USUARIO";
		$Params = array();
		$ResultSet = DatabaseManager::getQueryResult($Sql, $Params);
		if ($ResultSet->num_rows > 0) {
			while($ResultRow = $ResultSet->fetch_assoc()) {
				$USUARIOS[] = array(
					"Id" => $ResultRow['IdUsuario'],
					"Nombre" => utf8_encode($ResultRow['Nombre']),
					"Correo" => utf8_encode($ResultRow['Correo']),
					"TelefonoCelular" => utf8_encode($ResultRow['TelefonoCelular'])
				);
			}
			$CODIGO = 1;
			//ActualizarTokenAdmin($IdAdmin); //Actualiza hora de creación de token
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"usuarios" => $USUARIOS
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_EnviarPregunta($TOKEN, $PREGUNTA){
	$CODIGO = 0;
	global $configs;
	$ID_USUARIO = ValidarToken($TOKEN);
	if($ID_USUARIO!=''){
		//Obtener nombre de usuario
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$query = $conn->prepare("SELECT CONCAT(PrimerNombre,' ',SegundoNombre,' ',PrimerApellido,' ',SegundoApellido) AS Nombre, TelefonoCelular " .
				"FROM USUARIO " .
				"WHERE IdUsuario = ?");
			$query->bind_param('s',$ID_USUARIO);
			$query->execute();
			$result = $query->get_result();
			if ($result->num_rows == 1) {
				$usuario = $result->fetch_assoc();
				$NOMBRE = utf8_encode($usuario['Nombre']);
				$TELEFONO_CELULAR = $usuario['TelefonoCelular'];

				//Enviar correo a administrador
				$CODIGO = EnviarCorreoPregunta('escoly.soporte@gmail.com', $PREGUNTA, $ID_USUARIO, $NOMBRE, $TELEFONO_CELULAR);
			}
			$query->close();
		}

	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_CambioPass($TOKEN, $PASS_ACTUAL, $NUEVO_PASS){
	$CODIGO = 0;
	global $configs;
	$ID_USUARIO = ValidarToken($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){

			$query = $conn->prepare("SELECT 1 FROM USUARIO WHERE IdUsuario = ? AND Password = ?");
			$query->bind_param('ss',$ID_USUARIO, $PASS_ACTUAL);
			$query->execute();
			$result = $query->get_result();
			if ($result->num_rows == 1) {
				//Contraseña correcta
				$query = $conn->prepare("UPDATE USUARIO SET Password = ? WHERE IdUsuario = ?");
				$query->bind_param('ss',$NUEVO_PASS, $ID_USUARIO);
				$query->execute();

				if($conn->affected_rows>0){
					$CODIGO = 1;
				}
			}
			$query->close();
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_DetalleUsuarioAdmin($TOKEN, $ID_USUARIO){
	$CODIGO = 0;
	$USUARIO = '';
	$MENSAJE = '';
	global $configs;
	$ID_ADMIN = ValidarTokenAdmin($TOKEN);
	if($ID_ADMIN!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql = 	"SELECT PrimerNombre, SegundoNombre, PrimerApellido, SegundoApellido, Genero, FechaNacimiento, Correo, " .
			"TelefonoCelular, Departamento, Municipio, Direccion, Instituto, " .
			"NombreFacturacion, ApellidoFacturacion, Nit, DepartamentoFacturacion, RedSocial, Medio " .
			"FROM USUARIO WHERE IdUsuario = '$ID_USUARIO'";
			$result = $conn->query($sql);
			if ($result->num_rows == 1) {

				$usuario = $result->fetch_assoc();
				$USUARIO = array(
					"PrimerNombre" => $usuario['PrimerNombre'],
					"SegundoNombre" => utf8_encode($usuario['SegundoNombre']),
					"PrimerApellido" => utf8_encode($usuario['PrimerApellido']),
					"SegundoApellido" => $usuario['SegundoApellido'],
					"Genero" => utf8_encode($usuario['Genero']),
					"FechaNacimiento" => $usuario['FechaNacimiento'],
					"Correo" => $usuario['Correo'],
					"TelefonoCelular" => $usuario['TelefonoCelular'],
					"Departamento" => $usuario['Departamento'],
					"Municipio" => $usuario['Municipio'],
					"Direccion" => $usuario['Direccion'],
					"Instituto" => utf8_encode($usuario['Instituto']),
					"NombreFacturacion" => utf8_encode($usuario['NombreFacturacion']),
					"ApellidoFacturacion" => utf8_encode($usuario['ApellidoFacturacion']),
					"Nit" =>$usuario['Nit'],
					"DepartamentoFacturacion" => $usuario['DepartamentoFacturacion'],
					"RedSocial" => $usuario['RedSocial'],
					"Medio" => $usuario['Medio']
				);
				$CODIGO = 1;
			}
			$conn->close();
		}
	}else{
		$MENSAJE = 'Error autenticando usuario administrador';
	}

	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"usuario" => $USUARIO,
		"mensaje" => $MENSAJE
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));

}


function WO_DetalleUsuario($TOKEN){
	$CODIGO = 0;
	$USUARIO = '';
	global $configs;
	$ID_USUARIO = ValidarToken($TOKEN);
	ActualizarTokenUsuario($ID_USUARIO);
	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql = 	"SELECT PrimerNombre, SegundoNombre, PrimerApellido, SegundoApellido, Genero, FechaNacimiento, Correo, " .
			"TelefonoCelular, Departamento, Municipio, Direccion, Instituto, " .
			"NombreFacturacion, ApellidoFacturacion, Nit, DepartamentoFacturacion, RedSocial, Medio " .
			"FROM USUARIO WHERE IdUsuario = '$ID_USUARIO'";
			$result = $conn->query($sql);
			if ($result->num_rows == 1) {

				$usuario = $result->fetch_assoc();
				$USUARIO = array(
					"PrimerNombre" => $usuario['PrimerNombre'],
					"SegundoNombre" => utf8_encode($usuario['SegundoNombre']),
					"PrimerApellido" => utf8_encode($usuario['PrimerApellido']),
					"SegundoApellido" => $usuario['SegundoApellido'],
					"Genero" => utf8_encode($usuario['Genero']),
					"FechaNacimiento" => $usuario['FechaNacimiento'],
					"Correo" => $usuario['Correo'],
					"TelefonoCelular" => $usuario['TelefonoCelular'],
					"Departamento" => $usuario['Departamento'],
					"Municipio" => $usuario['Municipio'],
					"Direccion" => $usuario['Direccion'],
					"Instituto" => utf8_encode($usuario['Instituto']),
					"NombreFacturacion" => utf8_encode($usuario['NombreFacturacion']),
					"ApellidoFacturacion" => utf8_encode($usuario['ApellidoFacturacion']),
					"Nit" =>$usuario['Nit'],
					"DepartamentoFacturacion" => $usuario['DepartamentoFacturacion'],
					"RedSocial" => $usuario['RedSocial'],
					"Medio" => $usuario['Medio']
				);
				$CODIGO = 1;
			}
			$conn->close();
		}
	}

	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"usuario" => $USUARIO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));

}


function WO_EditarUsuario($USUARIO, $TOKEN){
	$CODIGO = 0;
	$MENSAJE = '';
	global $configs;
	$ID_USUARIO = ValidarToken($TOKEN);
	ActualizarTokenUsuario($ID_USUARIO);
	if($ID_USUARIO!=''){
		$PRIMER_NOMBRE = $USUARIO->PrimerNombre;
		$SEGUNDO_NOMBRE = $USUARIO->SegundoNombre;
		$PRIMER_APELLIDO = $USUARIO->PrimerApellido;
		$SEGUNDO_APELLIDO = $USUARIO->SegundoApellido;
		$GENERO = $USUARIO->Genero;
		$FECHA_NACIMIENTO = $USUARIO->FechaNacimiento;
		$CORREO = $USUARIO->Correo;
		$TELEFONO_CELULAR = $USUARIO->TelefonoCelular;
		$DEPARTAMENTO = $USUARIO->Departamento;
		$MUNICIPIO = $USUARIO->Municipio;
		$DIRECCION = $USUARIO->Direccion;
		$INSTITUTO = $USUARIO->Instituto;
		$NOMBRE_FACTURACION = $USUARIO->NombreFacturacion;
		$APELLIDO_FACTURACION = $USUARIO->ApellidoFacturacion;
		$NIT = $USUARIO->Nit;
		$DEPARTAMENTO_FACTURACION = $USUARIO->DepartamentoFacturacion;
		$IMAGEN = $USUARIO->Imagen;
		$RedSocial = $USUARIO->RedSocial;
		$Medio = $USUARIO->Medio;

		//Validar formato de correo
		if (!filter_var($CORREO, FILTER_VALIDATE_EMAIL)) {
			//Formato incorrecto
			$MENSAJE = 'El correo ingresado tiene formato incorrecto';
		//Validar formato de celular
		}else if(!ctype_digit($TELEFONO_CELULAR)){
			//Formato incorrecto
			$MENSAJE = 'Celular ingresado tiene formato incorrecto';
		}else{
			$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
			if(!$conn->connect_error){
				$sql = 	"UPDATE 	USUARIO " .
				"SET PrimerNombre = '$PRIMER_NOMBRE', SegundoNombre = '$SEGUNDO_NOMBRE', PrimerApellido = '$PRIMER_APELLIDO', SegundoApellido = '$SEGUNDO_APELLIDO', " .
				"Genero = '$GENERO', FechaNacimiento = '$FECHA_NACIMIENTO', Correo='$CORREO', TelefonoCelular = '$TELEFONO_CELULAR', " .
				"Departamento = $DEPARTAMENTO, Municipio = $MUNICIPIO, Direccion = '$DIRECCION', Instituto = '$INSTITUTO', " .
				"NombreFacturacion = '$NOMBRE_FACTURACION', ApellidoFacturacion = '$APELLIDO_FACTURACION', Nit = '$NIT', DepartamentoFacturacion = $DEPARTAMENTO_FACTURACION, " .
				"RedSocial = '$RedSocial', Medio = '$Medio' " .
				"WHERE IdUsuario = '$ID_USUARIO'";
				//echo $sql;
				$conn->set_charset("utf8");
				if (mysqli_query($conn, $sql)) {
					$CODIGO = 1;
				}else{
					$CODIGO = $conn->errno;
					$MENSAJE = "Error en edicion de perfil";
				}
				$conn->close();

			}else{
				$MENSAJE = "Error en edicion de perfil";
			}
		}

	}else{
		$MENSAJE = "Error en edicion de perfil";
	}

	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"mensaje" => $MENSAJE
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));

}


//WO_ActivarUsuario('TUFSSU9ERVYz',"MmJjZGIxNGJiNzdmMzlkOGNhYmM3NGU1MzkxZThmNDc%3D");
function WO_ActivarUsuario($ID_USUARIO, $TOKEN){
	$out_Codigo = 0;
	$out_Mensaje = '';
	global $configs;
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	$USUARIO_DECRYPT = '';
	if(!$conn->connect_error){
		//Desencriptar Token
		$USUARIO_DECRYPT = urldecode(base64_decode($ID_USUARIO));

		$sql = "SELECT IdUsuario FROM USUARIO WHERE IdUsuario = '$USUARIO_DECRYPT' AND Token = '$TOKEN' AND TIMEDIFF(NOW(), FechaToken) < '01:00:00' AND Activo = 0 ";
		//echo $sql;
		$conn->set_charset("utf8");
		$result = $conn->query($sql);
		if ($result->num_rows == 1) {
			//Usuario lozalizado y token valido

			//Obtener id de compra para busquedas de prueba
			$sql = "SELECT IdCompra FROM COMPRA WHERE IdTransaccion = 'C0mpr@Pru3b@'";
			//echo $sql;
			$result = $conn->query($sql);
			if ($result->num_rows == 1) {
				$compra = $result->fetch_assoc();
				$ID_COMPRA = $compra['IdCompra'];
				//Agregar busqueda de prueba
				$sql = 	"INSERT INTO BUSQUEDA(ID_USUARIO, FECHA_CREACION, ESTADO, ID_COMPRA, PRUEBA) " .
						"VALUES('$USUARIO_DECRYPT', NOW(), 1, $ID_COMPRA, 1)";
				$conn->set_charset("utf8");
				if (mysqli_query($conn, $sql)) {
					$sql = "UPDATE USUARIO SET ACTIVO = 1 WHERE IdUsuario = '$USUARIO_DECRYPT'";
					$conn->set_charset("utf8");
					if (mysqli_query($conn, $sql)) {
						//Usuario ha sido activado
						//$RESULTADO = $USUARIO_DECRYPT;
						$out_Codigo = 1;
					}else{
						//$RESULTADO = $conn->errno;
						NError('1003', 'usuario.php', 'Error cambiando estado de usuario a activo', '0', '');
					}
				}else{
					//$RESULTADO = $conn->errno;
					NError('1002', 'usuario.php', 'Error creando nueva busqueda de prueba para usuario', '0', '');
				}
			}else{
				NError('1001', 'usuario.php', 'La compra de prueba no esta registrada!!', '0', '');
			}

			$conn->close();
		}else{
			NError('1004', 'usuario.php', 'Error autenticando usuario a registrar', '0', '');
		}
	}
	if( ($out_Codigo==0) && ($out_Mensaje=="") ) $out_Mensaje = "Error en activación de usuario, por favor intenta pulsar el enlace de activación nuevamente";
	if( ($out_Codigo==1) && ($out_Mensaje=="") ) $out_Mensaje = "Bienvenido usuario " . $USUARIO_DECRYPT . ", ya eres parte de Escoly!";

	$Respuesta = array(
		"Codigo" => $out_Codigo,
		"Mensaje" => $out_Mensaje
	);
	header('Content-type: application/json');
	echo json_encode(array("Respuesta" => $Respuesta));
}


function WO_EnviarRecordatorioContrasena($TEXTO_BUSQUEDA){
	$MENSAJE = "";
	global $configs;
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);

	if(!$conn->connect_error){
		//Obtener contraseña, id de usuario y correo electronico
		$sql = "SELECT IdUsuario, Password, Correo FROM USUARIO WHERE (IdUsuario = ? OR Correo = ?) AND Activo = 1";
		$query = $conn->prepare($sql);
		$query->bind_param('ss',$TEXTO_BUSQUEDA, $TEXTO_BUSQUEDA);
		$query->execute();
		$resultado = $query->get_result();
		if($resultado->num_rows==1){
			$usuario = $resultado->fetch_assoc();
			$PASSWORD = $usuario["Password"];
			$ID_USUARIO = $usuario["IdUsuario"];
			$CORREO = $usuario["Correo"];

			$RESULTADO = EnviarCorreoRecordatorioContraseña($CORREO, $ID_USUARIO, $PASSWORD);
			if($RESULTADO==1){
				$MENSAJE = "OK";
			}else{
				$MENSAJE = "Error en envió de contraseña, favor intenta nuevamente";
			}
		}else{
			$MENSAJE = "¿Estás seguro que es ese? No nos aparece";
		}
	}
	$RESPUESTA = array(
		"Mensaje" => $MENSAJE
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_EnviarCorreoActivacion($ID_USUARIO, $CORREO){
	$RESULTADO = 0;
	$SQL_TEXT = '';
	global $configs;
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);

	if(!$conn->connect_error){
		//Generar Token para usuario
		$TOKEN = md5(uniqid(rand(), true));
		$TOKEN_ENCRYPT = urlencode(base64_encode($TOKEN));
		$COND = '';

		if($ID_USUARIO != ''){
			$COND = "WHERE IdUsuario = '$ID_USUARIO' ";
		}else if($CORREO != ''){
			$COND = "WHERE Correo = '$CORREO' ";
		}else{
			$COND = "WHERE 1 = 0";
		}

		//Validar que usuario exista
		$sql = "SELECT IdUsuario FROM USUARIO " . $COND;
		$conn->set_charset("utf8");
		$result = $conn->query($sql);
		if ($result->num_rows == 1) {
			//Usuario existe

			//Validar que usuario no este activo
			$sql = "SELECT IdUsuario FROM USUARIO " . $COND . " AND Activo = 0";
			$conn->set_charset("utf8");
			$result = $conn->query($sql);
			if ($result->num_rows == 1) {
				//Actualizar token en tabla usuario
				$sql = "UPDATE USUARIO SET Token = '$TOKEN_ENCRYPT', FechaToken = NOW() " . $COND;
				$SQL_TEXT = $sql;
				$conn->set_charset("utf8");
				if (mysqli_query($conn, $sql)) {
					//Token actualizado exitosamente
					$RESULTADO = 1;

					//Obtener infrmacion de usuario
					$sql = "SELECT IdUsuario, Correo FROM USUARIO " . $COND;

					$result = $conn->query($sql);
					if ($result->num_rows == 1) {
						$usuario = $result->fetch_assoc();
						$ID_USUARIO_CORREO = $usuario['IdUsuario'];
						$CORREO_CORREO = $usuario['Correo'];
						//Enviar correo a usuario
						$RESULTADO = EnviarEnlaceActivacion($ID_USUARIO_CORREO, $TOKEN_ENCRYPT, $CORREO_CORREO);
					}

				}
			}else{
				//Usuario ya activo
				$RESULTADO = 3;
			}

		}else{
			//Usuario no existe
			$RESULTADO = 2;
		}

		$conn->close();
	}
	//return $SQL_TEXT;
	return $RESULTADO;
}


function WO_RegistrarUsuario($USUARIO){
	$CODIGO = 0;
	$MENSAJE = '';
	global $configs;
	$ID_USUARIO = $USUARIO->IdUsuario;
	$PASSWORD = $USUARIO->Password;
	$PRIMER_NOMBRE = $USUARIO->PrimerNombre;
	$SEGUNDO_NOMBRE = $USUARIO->SegundoNombre;
	$PRIMER_APELLIDO = $USUARIO->PrimerApellido;
	$SEGUNDO_APELLIDO = $USUARIO->SegundoApellido;
	$GENERO = $USUARIO->Genero;
	$FECHA_NACIMIENTO = $USUARIO->FechaNacimiento;
	$CORREO = $USUARIO->Correo;
	$TELEFONO_CELULAR = $USUARIO->TelefonoCelular;
	$DEPARTAMENTO = $USUARIO->Departamento;
	$MUNICIPIO = $USUARIO->Municipio;
	$DIRECCION = $USUARIO->Direccion;
	$INSTITUTO = $USUARIO->Instituto;
	$NOMBRE_FACTURACION = $USUARIO->NombreFacturacion;
	$APELLIDO_FACTURACION = $USUARIO->ApellidoFacturacion;
	$NIT = $USUARIO->Nit;
	$DEPARTAMENTO_FACTURACION = $USUARIO->DepartamentoFacturacion;
	$IMAGEN = $USUARIO->Imagen;
	$RedSocial = $USUARIO->RedSocial;
	$Medio = $USUARIO->Medio;

	//Validar formato de correo
	if (!filter_var($CORREO, FILTER_VALIDATE_EMAIL)) {
		//Formato incorrecto
		$MENSAJE = 'El correo ingresado tiene formato incorrecto';
	//Validar formato de celular
	}else if(!ctype_digit($TELEFONO_CELULAR)){
		//Formato incorrecto
		$MENSAJE = 'Celular ingresado tiene formato incorrecto';
	//Validar longitud de password
	}else if(strlen($PASSWORD)<6){
		//Longitud invalida
		$MENSAJE = 'Contraseña requerida mínimo de 6 caracteres';
	}else{
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql = "INSERT INTO USUARIO (IdUsuario, Password, PrimerNombre, SegundoNombre, PrimerApellido,SegundoApellido,Genero, " .
					"FechaNacimiento, Correo, TelefonoCelular, Departamento, Municipio, Direccion, Instituto, " .
					"NombreFacturacion, ApellidoFacturacion, Nit, DepartamentoFacturacion, Imagen, RedSocial, Medio) VALUES ( " .
					"'$ID_USUARIO', '$PASSWORD', '$PRIMER_NOMBRE', '$SEGUNDO_NOMBRE', '$PRIMER_APELLIDO', '$SEGUNDO_APELLIDO', " .
					"'$GENERO', '$FECHA_NACIMIENTO', '$CORREO', '$TELEFONO_CELULAR', $DEPARTAMENTO, $MUNICIPIO, '$DIRECCION', '$INSTITUTO', " .
					"'$NOMBRE_FACTURACION', '$APELLIDO_FACTURACION', '$NIT', '$DEPARTAMENTO_FACTURACION', '$IMAGEN', '$RedSocial', '$Medio')";


			$conn->set_charset("utf8");
			if (mysqli_query($conn, $sql)) {
				$CODIGO = WO_EnviarCorreoActivacion($ID_USUARIO, $CORREO);
				$CODIGO = 1;
			}else{
				$CODIGO = $conn->errno;
				if($CODIGO==1062){
					//Id de usuario ya existe
					$CODIGO = 0;
					$MENSAJE = 'Ese ID ya la tiene otra persona, creá otro';
				}
			}
			$conn->close();

		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"mensaje" => $MENSAJE
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}



?>
