<?php

include('SqlParameter.php');

Interface iDatabaseManager{

  public static function getQueryResult($Sql, $Params);
  public static function TestConnection();
  public static function executeQuery($Sql, $Params);

}

class DatabaseManager implements iDatabaseManager{

  static $Host = '45.32.70.155';
  static $User = 'escolyadmin';
  static $Password = '3Scoly@dm!n';
  static $Database = 'DB_ESCOLY';

  /* Makes a test connection to host */
  public static function TestConnection(){
    $ConnMessage = '';
    $Mysqli_Conn = new mysqli(self::$Host, self::$User, self::$Password, self::$Database);
    if(!$Mysqli_Conn->connect_error){
      $ConnMessage = 'OK';
      $Mysqli_Conn->close();
    }else{
      $ConnMessage = $Mysqli_Conn->connect_error;
    }
    return $ConnMessage;
  }

  public static function executeQuery($Sql, $Params){
    $out_Exito = false;
    $Mysqli_Conn = new mysqli(self::$Host, self::$User, self::$Password, self::$Database);
    if(!$Mysqli_Conn->connect_error){
      if( $Query = $Mysqli_Conn->prepare($Sql) ){
        // Temporary bind_param dynamic parameters array handling
        if(count($Params)==1){
          $SqlParam = new SqlParameter($Params[0]);
          $BindTypes = $SqlParam->getBindType();
          $Query->bind_param($BindTypes, $SqlParam->getValue());
        }else if(count($Params)==2){
          $SqlParam1 = new SqlParameter($Params[0]);
          $SqlParam2 = new SqlParameter($Params[1]);
          $BindTypes = $SqlParam1->getBindType() . $SqlParam2->getBindType();
          $Query->bind_param($BindTypes, $SqlParam1->getValue(), $SqlParam2->getValue());
        }else if(count($Params)==3){
          $SqlParam1 = new SqlParameter($Params[0]);
          $SqlParam2 = new SqlParameter($Params[1]);
          $SqlParam3 = new SqlParameter($Params[2]);
          $BindTypes = $SqlParam1->getBindType() . $SqlParam2->getBindType() . $SqlParam3->getBindType();
          $Query->bind_param($BindTypes, $SqlParam1->getValue(), $SqlParam2->getValue(), $SqlParam3->getValue());
        }
        $out_Exito = $Query->execute();
        $Query->close();
      }else{
          NError($Mysqli_Conn->errno, 'DatabaseManager.php',
          " *** Query => " . $Sql . " *** " . $Mysqli_Conn->error, '--', $Sql);
      }
      $Mysqli_Conn->close();
    }else{
        NError($Mysqli_Conn->connect_errno, 'DatabaseManager.php',
        " *** Query => " . $Sql . " *** " . $Mysqli_Conn->connect_error, '--', getConnString());
    }
    return $out_Exito;
  }

  /* Gets the Result object from a query */
  public static function getQueryResult($Sql, $Params){
    $ResultSet = NULL;
    $Mysqli_Conn = new mysqli(self::$Host, self::$User, self::$Password, self::$Database);
    if(!$Mysqli_Conn->connect_error){
      if( $Query = $Mysqli_Conn->prepare($Sql) ){
        // Temporary bind_param dynamic parameters array handling
        if(count($Params)==1){
          $SqlParam = new SqlParameter($Params[0]);
          $BindTypes = $SqlParam->getBindType();
          $Query->bind_param($BindTypes, $SqlParam->getValue());
        }else if(count($Params)==2){
          $SqlParam1 = new SqlParameter($Params[0]);
          $SqlParam2 = new SqlParameter($Params[1]);
          $BindTypes = $SqlParam1->getBindType() . $SqlParam2->getBindType();
          $Query->bind_param($BindTypes, $SqlParam1->getValue(), $SqlParam2->getValue());
        }else if(count($Params)==3){
          $SqlParam1 = new SqlParameter($Params[0]);
          $SqlParam2 = new SqlParameter($Params[1]);
          $SqlParam3 = new SqlParameter($Params[2]);
          $BindTypes = $SqlParam1->getBindType() . $SqlParam2->getBindType() . $SqlParam3->getBindType();
          $Query->bind_param($BindTypes, $SqlParam1->getValue(), $SqlParam2->getValue(), $SqlParam3->getValue());
        }
        $Query->execute();
        $ResultSet = $Query->get_result();
        $Query->close();
      }else{
        NError($Mysqli_Conn->errno, 'DatabaseManager.php',
        " *** Query => " . $Sql . " *** " . $Mysqli_Conn->error, '--', $Sql);
      }
      $Mysqli_Conn->close();
    }else{
      NError($Mysqli_Conn->connect_errno, 'DatabaseManager.php',
        " *** Query => " . $Sql . " *** " . $Mysqli_Conn->connect_error, '--', self::getConnString());
    }
    return $ResultSet;
  }

  private static function getConnString(){
    return 'Host:' . self::$Host . ";User:" . self::$User . ";Password:" . self::$Password . ";Database:" . self::$Database . ";";
  }


}



?>
