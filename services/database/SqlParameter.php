<?php

class SqlParameter{

  function __construct($Value){
    $this->Value = $Value;
  }

  // Gets the value of the $Value variable
  public function getValue(){
    return $this->Value;
  }

  // Gets the acronym of the variable type for bind_param usage
  public function getBindType(){
    $BindType = 's';
    switch(gettype($this->Value)){
        case "integer":
          $BindType = 'i';
          break;
        case "string":
          $BindType = 's';
          break;
        case "double":
          $BindType = 'd';
          break;
        default:
          $BindType = 's';
          break;
    }
    return $BindType;
  }

}

?>
