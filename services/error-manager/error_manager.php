
<?php

function myErrorHandler($errno, $errstr, $errfile, $errline) {
	NError($errno, $errfile, $errstr, $errline, '');
}

function fatalErrorShutdownHandler()
{
  $ultimo_error = error_get_last();
	if($ultimo_error['type'] == E_ERROR){
		//echo  "SOY ERROR => " . $ultimo_error['message'] . ", " . $ultimo_error['file'] . $ultimo_error['line'];
		myErrorHandler($ultimo_error['type'], "FATAL ERROR: " . $ultimo_error['message'], $ultimo_error['file'], $ultimo_error['line']);
	}
}

set_error_handler("myErrorHandler");
register_shutdown_function('fatalErrorShutdownHandler');

 ?>
