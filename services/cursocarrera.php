<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization");

$configs = include('config.php');
include('bitacora.php');
include('correo.php');
include('token.php');
include(__DIR__ . '/error-manager/error_manager.php');
include(__DIR__ . '/manager/carrera_manager.php');
include(__DIR__ . '/database/DatabaseManager.php');


$postdata = file_get_contents("php://input");

if(isset($postdata)) {
	$request = json_decode($postdata);
	if($request->Operacion == "periodos"){
		WO_ObtenerPeriodos($request->IdCarrera);
	}else if($request->Operacion == "addperiodo"){
		WO_AgregarPeriodo($request->IdCarrera, $request->Param1);
	}else if($request->Operacion == "cursosnoasig"){
		WO_ObtenerCursosSinAsignar($request->IdCarrera);
	}else if($request->Operacion == "asigcursos"){
		WO_AsignarCursosEnPeriodo($request->Periodo, $request->Cursos, $request->Param1);
	}else if($request->Operacion == "eliminar"){
		WO_EliminarCursoDePeriodo($request->IdCurso, $request->IdPeriodo, $request->Param1);
	}else if($request->Operacion == "elimperiodo"){
		WO_EliminarPeriodo($request->IdPeriodo, $request->Param1);
	}else if($request->Operacion == "actduracion"){
		WO_ActualizarDuracionPeriodo($request->IdCarrera, $request->Param1, $request->NuevaDuracion);
	}
}


/* ****** Operaciones Web ****** */

function WO_EliminarPeriodo($ID_PERIODO, $TOKEN){
	$CODIGO = 0;
	global $configs;

	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql =  "DELETE FROM PERIODO WHERE IdPeriodo = $ID_PERIODO";

			if (mysqli_query($conn, $sql)) {
				$CODIGO = 1;
			}else{
				$CODIGO = $conn->errno;
			}
			$conn->close();
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_EliminarCursoDePeriodo($ID_CURSO, $ID_PERIODO, $TOKEN){
	$CODIGO = 0;
	global $configs;

	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql =  "DELETE FROM CURSOXPERIODO WHERE IdCurso = $ID_CURSO AND IdPeriodo = $ID_PERIODO";

			if (mysqli_query($conn, $sql)) {
				$CODIGO = 1;
			}else{
				$CODIGO = $conn->errno;
			}
			$conn->close();
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_AsignarCursosEnPeriodo($PERIODO, $CURSOS, $TOKEN){
	global $configs;
	$CODIGO = 0;

	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$CONTADOR = 0;
			for($I = 0; $I < sizeof($CURSOS); $I++){
				$CURSO = $CURSOS[$I];
				IF($CURSO->Seleccionado == 1){
					$ID_CURSO = $CURSO->Id;
					$sql =  "INSERT INTO CURSOXPERIODO VALUES($PERIODO,$ID_CURSO)";

					if (mysqli_query($conn, $sql)) {
						$CODIGO = 1;
					}else{
						$CODIGO = $conn->errno;
						break;
					}
				}
			}
			$conn->close();
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_ObtenerCursosSinAsignar($ID_CARRERA){
	global $configs;
	$cursos = array();
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$conn->connect_error){

		$sql = 	"SELECT 	IdCurso, Nombre, Descripcion, " . //"IFNULL(Categoria,'N/A') AS Categoria " .
				"CASE 		 " .
				"	WHEN 		Categoria IS NULL THEN 'N/A' " .
				"	WHEN 		Categoria = '' THEN 'N/A' " .
				"	ELSE 		Categoria " .
				"END AS Categoria " .
				"FROM 		CURSO " .
				"WHERE NOT EXISTS( SELECT 1 FROM CURSOXPERIODO CXP, PERIODO P WHERE  P.IdPeriodo = CXP.IdPeriodo " .
				"AND P.IdCarrera = $ID_CARRERA AND CXP.IdCurso = CURSO.IdCurso)";

		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($curso = $result->fetch_assoc()) {
				$cursos[] = array(
					"Id" => $curso['IdCurso'],
					"Nombre" => utf8_encode($curso['Nombre']),
					"Descripcion" => utf8_encode($curso['Descripcion']),
					"Categoria" => utf8_encode($curso['Categoria']),
					"Seleccionado" => 0
				);
			}

		}
		$conn->close();
	}
	header('Content-type: application/json');
	echo json_encode(array('cursos'=>$cursos));

}


function WO_AgregarPeriodo($ID_CARRERA, $TOKEN){
	$CODIGO = 0;
	global $configs;

	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql1 = "SELECT Orden FROM PERIODO WHERE IdCarrera = $ID_CARRERA ORDER BY Orden DESC LIMIT 1";
			$result = $conn->query($sql1);
			$NUEVO_PERIODO = 0;
			if ($result->num_rows == 1) {
				$info = $result->fetch_assoc();
				$NUEVO_PERIODO = intval($info["Orden"]) + 1;
			}else{
				$NUEVO_PERIODO = 1;
			}
			$sql2 = "INSERT INTO PERIODO (Orden, IdCarrera) VALUES($NUEVO_PERIODO, $ID_CARRERA)";
			if (mysqli_query($conn, $sql2)) {
				$CODIGO = 1;
			}else{
				$CODIGO = $conn->errno;
			}
			$conn->close();
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}

//WO_ActualizarDuracionPeriodo('1','MzA0MDExOWEyMzA3M2M3N2E1N2MwMWM3N2VjYjM0YzR8Sk9TQURNSU4','3');
function WO_ActualizarDuracionPeriodo($IdCarrera, $TokenSesion, $Nueva_Duracion_Meses){
	$out_Exito = false;
	$out_Mensaje = '';

	if( SesionDeAdminAutenticada($TokenSesion) ){
	//if(true){
		$Sql = 	"UPDATE CARRERA " .
						"SET 		Duracion_Periodo_Meses = ? " .
						"WHERE	IdCarrera = ? ";
		$Params = array(
			intval($Nueva_Duracion_Meses),
			intval($IdCarrera)
		);
		$out_Exito = DatabaseManager::executeQuery($Sql, $Params);
		if($out_Exito) $out_Mensaje = "Duración de período actualizada exitosamente";
	}
	if( (!$out_Exito) && ($out_Mensaje=='') ) $out_Mensaje = 'Error actualizando duración de período de carrera';

	$Respuesta = array(
		"Exito" => $out_Exito,
		"Mensaje" => $out_Mensaje
	);
	header('Content-type: application/json');
	echo json_encode(array("Respuesta"=>$Respuesta));
}

//WO_ObtenerPeriodos('1');
function WO_ObtenerPeriodos($IdCarrera){
	$out_Periodos = CarreraManager::ObtenerPensum($IdCarrera);
	$out_Duracion = CarreraManager::ObtenerDuracionPeriodo($IdCarrera);
	$Respuesta = array(
		"Periodos" => $out_Periodos,
		"Duracion" => $out_Duracion
	);
	header('Content-type: application/json');
	echo json_encode(array("Respuesta"=>$Respuesta));
}
