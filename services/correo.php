<?php

$configs = include('config.php');
require __DIR__ . '/phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer();
$mail->IsSMTP(); // telling the class to use SMTP
//$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
										   // 1 = errors and messages
										   // 2 = messages only
$mail->SMTPAuth   = true;                  // enable SMTP authentication
$mail->SMTPSecure = "tls";
$mail->Host       = "smtp.gmail.com";      // SMTP server
$mail->Port       = 587;                   // SMTP port
$mail->Username   = "escoly.soporte@gmail.com";  // username
$mail->Password   = "3scolyadm!n";            // password

$FOOTER = 	'<div style=" font-size: 12px; color: #A4A4A4; ">' .
				'&#169; 2018 Escoly Guatemala <br/>' .
				'Escoly es un nombre registrado de Guatemala, Guatemala <br/> ' .
				'<a href="http://45.32.70.155/Escoly">www.escoly.com</a> <br/>' .
			'</div>';

$URL = 'http://45.32.70.155/Escoly/components/sitio-usuario/user-activar.html';

function ObtenerURLActivacion($ID_USUARIO, $PASSWORD, $CORREO){
	global $URL;
	$TOKEN = md5(uniqid(rand(), true));
	$TOKEN_ENCRYPT = urlencode(base64_encode($TOKEN));
	$USUARIO_ENC = urlencode(base64_encode($ID_USUARIO));
	$URL_ACTIVACION = $URL . '#?tokenp=' . $TOKEN_ENCRYPT . '&otoken=' .  $USUARIO_ENC;;
	echo $URL_ACTIVACION;
}


function EnviarCorreoExamenOrientacion($CORREO_USUARIO, $NUM_EXAMEN, $ENLACE, $ID_ORIENTACION){
	global $mail;
	global $FOOTER;
	global $configs;
	global $URL;
	$RESULTADO = 0;

	$html = '<html>' .
		'<head></head>' .
		'<body>' .
			'<div style="background-color:#E6E6E6; padding: 20px 20px;">' .
				'<div style="background-color: white; padding: 20px 20px;">' .
					'<h2>Escoly</h2>' .
					'<img src="cid:logo_red" style="width:50px;"/> ' .
					'<br/>' .
					'<h3>¡Momento de Test!</h3>' .
					'<br/>' .
					'<p>' .
						'Para hacer tu test apachá el siguiente enlace: ' .
						'<br/>' .
						'<br/>' .
						'<b><a href="' . $ENLACE . '">Examen ' . $NUM_EXAMEN . '</a></b>' .
						'<br/>' .
						'<br/>' .
						'Luego de que terminés, mandános un mensaje desde el botón “¡Ayúdenme!” en tu perfil de Escoly ' .
						'y decinos que ya lo hiciste para que podamos calificarlo. Una vez que califiquemos vas a poder ' .
						'entrar a “ver status de mi orientación” y conocer cómo va tu proceso y qué te hace falta.' .
					'</p>' .
				'</div>' .
				'<br/>' .
				'<br/><br/>' .
				'<center>'.
					$FOOTER .
				'</center>' .
			'</div>' .
		'</body>' .
		'</html>';

	$mail->SetFrom('escoly.soporte@gmail.com', 'Escoly: enlace de exámen de orientación');
	$mail->CharSet = 'UTF-8';
	$mail->Subject    = "Escoly, enlace de exámen de orientación";
	$mail->AddEmbeddedImage('phpmailer/logo-red.png', 'logo_red');
	$mail->MsgHTML($html);

	$address = $CORREO_USUARIO;
	$mail->AddAddress($address, "Soporte Escoly");
	if($mail->Send()){
		$RESULTADO = 1;
	}

	return $RESULTADO;
}


//EnviarCorreoPregunta('escoly.soporte@gmail.com', 'Ola ke ase?', 'MROD193', 'Mario Augusto Rodríguez Salvatierra');
function EnviarCorreoPregunta($CORREO_ADMIN, $PREGUNTA, $USUARIO, $NOMBRE, $TELEFONO_CELULAR){
	global $mail;
	global $FOOTER;
	global $configs;
	global $URL;
	$RESULTADO = 0;

	$html = '<html>' .
		'<head></head>' .
		'<body>' .
			'<div style="background-color:#E6E6E6; padding: 20px 20px;">' .
				'<div style="background-color: white; padding: 20px 20px;">' .
					'<h2>Escoly</h2>' .
					'<img src="cid:logo_red" style="width:50px;"/> ' .
					'<br/>' .
					'<h3>Pregunta de usuario:</h3>' .
					'<br/>' .
					'<p>' .
						'Se ha recibido una pregunta del usuario <b>' . $USUARIO . '</b>, con nombre <b>' . $NOMBRE  . '</b> ' .
						'y número de teléfono celular <b>' . $TELEFONO_CELULAR . '</b> :' .
						'<br/>' .
						'<br/>' .
						'<i>' .
							$PREGUNTA .
						'</i>' .
					'</p>' .
				'</div>' .
				'<br/>' .
				'<br/><br/>' .
				'<center>'.
					$FOOTER .
				'</center>' .
			'</div>' .
		'</body>' .
		'</html>';

	$mail->SetFrom('escoly.soporte@gmail.com', 'Escoly: Pregunta de usuario');
	$mail->CharSet = 'UTF-8';
	$mail->Subject    = "Escoly, pregunta de usuario";
	$mail->AddEmbeddedImage('phpmailer/logo-red.png', 'logo_red');
	$mail->MsgHTML($html);

	// $address = $CORREO_ADMIN;
	$address = "rodmario193@gmail.com";
	$mail->AddAddress($address, "Soporte Escoly");
	if($mail->Send()){
		$RESULTADO = 1;
	}

	return $RESULTADO;
}


//EnviarCorreoRecordatorioContraseña('mrod.epayment@gmail.com','AUGUSTO90','abc123');
function EnviarCorreoRecordatorioContraseña($CORREO, $ID_USUARIO, $PASSWORD){
	global $mail;
	global $FOOTER;
	global $configs;
	global $URL;
	$RESULTADO = 0;

	$html = '<html>' .
		'<head></head>' .
		'<body>' .
			'<div style="background-color:#E6E6E6; padding: 20px 20px;">' .
				'<div style="background-color: white; padding: 20px 20px;">' .
					'<h2>Escoly</h2>' .
					'<img src="cid:logo_red" style="width:50px;"/> ' .
					'<br/>' .
					'<h3>¿No te acordás de tu contraseña?:</h3>' .
					'<br/>' .
					'<p>' .
						'&emsp;¡No pasa nada! Aquí está: <br/><br/>' .
						'<ul>' .
							'<li><b>Tu contraseña:</b> ' . $PASSWORD . '</li>' .
						'</ul>' .
					'</p>' .
					'<br/>' .
					'<p>Recordá que podés cambiar tu contraseña en el botón <b>¿Querés cambiar tu contraseña?</b> en tu perfil por si se te es difícil recordar esta.</p>' .
				'</div>' .
				'<br/>' .
				'<br/><br/>' .
				'<center>'.
					$FOOTER .
				'</center>' .
			'</div>' .
		'</body>' .
		'</html>';

	$mail->SetFrom('escoly.soporte@gmail.com', 'Escoly: recordatorio de contraseña');
	$mail->CharSet = 'UTF-8';
	$mail->Subject    = "Escoly, recordatorio de contraseña";
	$mail->AddEmbeddedImage('phpmailer/logo-red.png', 'logo_red');
	$mail->MsgHTML($html);

	$address = $CORREO;
	$mail->AddAddress($address, "Soporte Escoly");
	if($mail->Send()){
		$RESULTADO = 1;
	}

	return $RESULTADO;
}


//EnviarCorreoNuevaCarrera('rodmario193@gmail.com','Crianza vida marina', 'Universidad Bub Espuma', 'Izabal', 'Puerto Barrios', 'Acuología');
function EnviarCorreoNuevaCarrera($CORREO, $NOMBRE, $UNIVERSIDAD, $DEPARTAMENTO, $MUNICIPIO, $GRUPO){
	global $mail;
	global $FOOTER;
	global $configs;
	global $URL;
	$RESULTADO = 0;

	$html = '<html>' .
		'<head></head>' .
		'<body>' .
			'<div style="background-color:#E6E6E6; padding: 20px 20px;">' .
				'<div style="background-color: white; padding: 20px 20px;">' .
					'<h2>Escoly</h2>' .
					'<img src="cid:logo_red" style="width:50px;"/> ' .
					'<br/>' .
					'<h3>¡Hay algo nuevo!</h3>' .
					'<br/>' .
					'<p>' .
						'&emsp;Hace poco agregamos un nuevo estudio que se llama <b>' . $NOMBRE . '</b>, ' .
						'a la <b>' . $UNIVERSIDAD . '</b>. <br/>' .
						'&emsp;Para verla anda la página principal de “¡Buscátelo!” y en tu búsqueda que tenés activa de <b>' . $GRUPO . '</b> vas a encontrarlo.' .
					'</p>' .
				'</div>' .
				'<br/>' .
				'<br/><br/>' .
				'<center>'.
					$FOOTER .
				'</center>' .
			'</div>' .
		'</body>' .
		'</html>';

	$mail->SetFrom('escoly.soporte@gmail.com', 'Escoly: notificación de nueva carrera');
	$mail->CharSet = 'UTF-8';
	$mail->Subject    = "Escoly, notificación de nueva carrera";
	$mail->AddEmbeddedImage('phpmailer/logo-red.png', 'logo_red');
	$mail->MsgHTML($html);

	$address = $CORREO;
	$mail->AddAddress($address, "Soporte Escoly");
	if($mail->Send()){
		$RESULTADO = 1;
	}

	return $RESULTADO;
}


function EnviarEnlaceActivacion($ID_USUARIO, $TOKEN_ENC, $CORREO, $NOMBRE){
	global $mail;
	global $FOOTER;
	global $configs;
	global $URL;
	$RESULTADO = 0;

	$USUARIO_ENC = urlencode(base64_encode($ID_USUARIO));
	$URL_ACTIVACION = $URL . '#?tokenp=' . $TOKEN_ENC . '&otoken=' .  $USUARIO_ENC;

	$html = '<html>' .
		'<head></head>' .
		'<body>' .
			'<div style="background-color:#E6E6E6; padding: 20px 20px;">' .
				'<div style="background-color: white; padding: 20px 20px;">' .
					'<h2>Escoly</h2>' .
					'<img src="cid:logo_red" style="width:50px;"/> ' .
					'<br/>' .
					'<h3>¡Ya casi!</h3>' .
					'<br/>' .
					'<p>' .
						'&emsp;Solo apachá el enlace que dice “¡Ya!” que está abajo y eso es todo.' .
					'</p>' .
					'<a href="' . $URL_ACTIVACION . '">¡Ya!</a>' .
				'</div>' .
				'<br/>' .
				'<br/><br/>' .
				'<center>'.
					$FOOTER .
				'</center>' .
			'</div>' .
		'</body>' .
		'</html>';

	$mail->SetFrom('escoly.soporte@gmail.com', 'Escoly: correo de activación');
	$mail->CharSet = 'UTF-8';
	$mail->Subject    = "Escoly, ¡Activá tu usuario!";
	$mail->AddEmbeddedImage('phpmailer/logo-red.png', 'logo_red');
	$mail->MsgHTML($html);

	$address = $CORREO;
	$mail->AddAddress($address, "Soporte Escoly");
	if($mail->Send()){
		$RESULTADO = 1;
	}

	return $RESULTADO;
}


//CorreoPrueba();
function CorreoPrueba(){
	$RESULTADO = 0;
	global $mail;
	global $FOOTER;
	$html = '<html>' .
		'<head></head>' .
		'<body>' .
			'<h3>Correo de prueba</h3>' .
			'<br/>' .
			'<center>' .
				$FOOTER .
			'</center>' .
		'</body>' .
		'</html>';

	$mail->MsgHTML($html);

	$address = "rodmario193@gmail.com";
	$mail->AddAddress($address, "Test");
	if($mail->Send()){
		$RESULTADO = 1;
	}

	echo $RESULTADO;
}


?>
