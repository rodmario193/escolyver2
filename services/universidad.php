<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization");

$configs = include('config.php');
include('bitacora.php');
include('correo.php');
include('token.php');
include(__DIR__ . '/error-manager/error_manager.php');
include(__DIR__ . '/database/DatabaseManager.php');

$postdata = file_get_contents("php://input");

if(isset($postdata)) {
	$request = json_decode($postdata);
	if($request->Operacion == "lista"){
		WO_ListaUniversidades();
	}else if($request->Operacion == "detalle"){
		WO_DetalleUniversidad($request->IdUniversidad, $request->Param1);
	}else if($request->Operacion == "agregar"){
		WO_AgregarUniversidad($request->Universidad, $request->Param1);
	}else if($request->Operacion == "modificar"){
		WO_ModificarUniversidad($request->Universidad, $request->Param1);
	}else if($request->Operacion == "eliminar"){
		WO_EliminarUniversidad($request->IdUniversidad, $request->Param1);
	}else if($request->Operacion == "departamentos"){
		WO_ListaDepartamentos();
	}else if($request->Operacion == "municipios"){
		WO_ListaMunicipios($request->IdDepartamento);
	}
}

/* ****** Operaciones Web ****** */

function WO_ListaMunicipios($ID_DEPARTAMENTO){
	global $configs;
	$municipios = array();
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);

	if(!$conn->connect_error){
		$sql = 	"SELECT 	IdMunicipio, Descripcion " .
				"FROM 		MUNICIPIO " .
				"WHERE		IdDepartamento = $ID_DEPARTAMENTO";


		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			while($municipio = $result->fetch_assoc()) {
				$municipios[] = array(
					"IdMunicipio" => $municipio['IdMunicipio'],
					"Descripcion" => $municipio['Descripcion']
				);
			}

		}

		$conn->close();

	}

	header('Content-type: application/json');
	echo json_encode(array('municipios'=>$municipios));

}


function WO_ListaDepartamentos(){
	global $configs;
	$departamentos = array();
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);

	if(!$conn->connect_error){
		$sql = 	"SELECT 	IdDepartamento, Nombre " .
				"FROM 		DEPARTAMENTO ";


		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			while($departamento = $result->fetch_assoc()) {
				$departamentos[] = array(
					"IdDepartamento" => $departamento['IdDepartamento'],
					"Nombre" => $departamento['Nombre']
				);
			}

		}

		$conn->close();

	}

	header('Content-type: application/json');
	echo json_encode(array('departamentos'=>$departamentos));

}


function WO_EliminarUniversidad($ID_UNIVERSIDAD, $TOKEN){
	$CODIGO = 0;
	global $configs;
	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql =  "DELETE FROM UNIVERSIDAD WHERE IdUniversidad = $ID_UNIVERSIDAD";

			if (mysqli_query($conn, $sql)) {
				$CODIGO = 1;
			}else{
				$CODIGO = $conn->errno;
			}
			$conn->close();
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_ModificarUniversidad($UNIVERSIDAD, $TOKEN){
	$CODIGO = 0;
	global $configs;
	$ID_USUARIO = ValidarTokenAdmin($TOKEN);
	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){

			$ID_UNIVERSIDAD = $UNIVERSIDAD->IdUniversidad;
			$NOMBRE = $UNIVERSIDAD->Nombre;
			$DEPARTAMENTO = $UNIVERSIDAD->Departamento;
			$MUNICIPIO = $UNIVERSIDAD->Municipio;
			$LOCALIDAD = $UNIVERSIDAD->Localidad;
			$ZONA = $UNIVERSIDAD->Zona;
			$TELEFONO1 = $UNIVERSIDAD->Telefono1;
			$TELEFONO2 = $UNIVERSIDAD->Telefono2;
			$URL = $UNIVERSIDAD->Url;
			$DETALLE_INSCRIPCION = $UNIVERSIDAD->DetalleInscripcion;
			$COSTO_PARQUEO = $UNIVERSIDAD->CostoParqueo;
			$DISPONIBILIDAD = $UNIVERSIDAD->Disponibilidad;
			$DETALLEPARQUEO = $UNIVERSIDAD->DetalleParqueo;
			$IMAGEN = $UNIVERSIDAD->Imagen;
			$INFO_ADICIONAL = $UNIVERSIDAD->InfoAdicional;

			$sql =  "UPDATE	UNIVERSIDAD " .
					"SET 	Nombre = '$NOMBRE', Departamento = $DEPARTAMENTO, Municipio = $MUNICIPIO, Localidad = '$LOCALIDAD', Zona = $ZONA, " .
					"Telefono1 = '$TELEFONO1', Telefono2 = '$TELEFONO2', Url = '$URL', DetalleInscripcion = '$DETALLE_INSCRIPCION', " .
					"CostoParqueo = $COSTO_PARQUEO, Disponibilidad = $DISPONIBILIDAD, DetalleParqueo = '$DETALLEPARQUEO', Imagen = '$IMAGEN', InfoAdicional = '$INFO_ADICIONAL', " .
					"UsuarioModifica = '$ID_USUARIO', FechaModificacion = NOW() " .
					"WHERE 	IdUniversidad = $ID_UNIVERSIDAD";


			$conn->set_charset("utf8");
			if (mysqli_query($conn, $sql)) {
				$CODIGO = 1;
			}else{
				$RESULTADO = $conn->errno;
			}
			$conn->close();

		}
	}

	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));

}


function WO_AgregarUniversidad($UNIVERSIDAD, $TOKEN){
	$CODIGO = 0;
	global $configs;

	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){

			$NOMBRE = $UNIVERSIDAD->Nombre;
			$DEPARTAMENTO = $UNIVERSIDAD->Departamento;
			$MUNICIPIO = $UNIVERSIDAD->Municipio;
			$LOCALIDAD = $UNIVERSIDAD->Localidad;
			$ZONA = $UNIVERSIDAD->Zona;
			$TELEFONO1 = $UNIVERSIDAD->Telefono1;
			$TELEFONO2 = $UNIVERSIDAD->Telefono2;
			$URL = $UNIVERSIDAD->Url;
			$DETALLE_INSCRIPCION = $UNIVERSIDAD->DetalleInscripcion;
			$COSTO_PARQUEO = $UNIVERSIDAD->CostoParqueo;
			$DISPONIBILIDAD = $UNIVERSIDAD->Disponibilidad;
			$DETALLEPARQUEO = $UNIVERSIDAD->DetalleParqueo;
			$IMAGEN = $UNIVERSIDAD->Imagen;
			$INFO_ADICIONAL = $UNIVERSIDAD->InfoAdicional;

			$sql =  "INSERT INTO UNIVERSIDAD ( Nombre, Departamento, Municipio, Localidad, Zona, " .
					"Telefono1, Telefono2, Url, DetalleInscripcion, CostoParqueo, Disponibilidad, DetalleParqueo, Imagen, InfoAdicional, UsuarioModifica, FechaModificacion)" .
					" VALUES( '$NOMBRE', $DEPARTAMENTO, $MUNICIPIO, '$LOCALIDAD', $ZONA, " .
					"'$TELEFONO1', '$TELEFONO2', '$URL', '$DETALLE_INSCRIPCION', $COSTO_PARQUEO,$DISPONIBILIDAD,'$DETALLEPARQUEO','$IMAGEN','$INFO_ADICIONAL','$ID_USUARIO',NOW())";

			//NLog($sql);
			$conn->set_charset("utf8");
			if (mysqli_query($conn, $sql)) {
				$CODIGO = 1;
			}else{
				$CODIGO = $conn->errno;
			}
			$conn->close();
		}
	}

	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_DetalleUniversidad($ID_UNIVERSIDAD, $TOKEN){
	global $configs;
	$UNIVERSIDAD = "";
	$CODIGO = 0;
	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql = 	"SELECT 	IdUniversidad, Nombre, Departamento, Municipio, Localidad, Zona, Telefono1, Telefono2, Url, DetalleInscripcion, CostoParqueo, " .
					"Disponibilidad, DetalleParqueo, Imagen, InfoAdicional " .
					"FROM 		UNIVERSIDAD " .
					"WHERE 		IdUniversidad = $ID_UNIVERSIDAD";

			$result = $conn->query($sql);
			if ($result->num_rows == 1) {
				$universidad = $result->fetch_assoc();
				$UNIVERSIDAD = array(
					"IdUniversidad" => $universidad['IdUniversidad'],
					"Nombre" => utf8_encode($universidad['Nombre']),
					"Departamento" => utf8_encode($universidad['Departamento']),
					"Municipio" => $universidad['Municipio'],
					"Localidad" => utf8_encode($universidad['Localidad']),
					"Zona" =>intval($universidad['Zona']),
					"Telefono1" => $universidad['Telefono1'],
					"Telefono2" => $universidad['Telefono2'],
					"Url" => utf8_encode($universidad['Url']),
					"DetalleInscripcion" => utf8_encode($universidad['DetalleInscripcion']),
					"CostoParqueo" => floatval($universidad['CostoParqueo']),
					"Disponibilidad" => intval($universidad['Disponibilidad']),
					"DetalleParqueo" => utf8_encode($universidad['DetalleParqueo']),
					"Imagen" => $universidad['Imagen'],
					"InfoAdicional" => utf8_encode($universidad['InfoAdicional'])
				);
				$CODIGO = 1;
			}
			$conn->close();
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"Universidad" => $UNIVERSIDAD
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));

}


function WO_ListaUniversidades(){
	global $configs;
	$universidades = array();
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);

	if(!$conn->connect_error){
		$sql = 	"SELECT 	U.IdUniversidad, U.Nombre, DEP.Nombre AS NombreDep " .
				"FROM 		UNIVERSIDAD U, DEPARTAMENTO DEP " .
				"WHERE		U.Departamento = DEP.IdDepartamento";

		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			while($universidad = $result->fetch_assoc()) {
				$universidades[] = array(
					"Id" => $universidad['IdUniversidad'],
					"Nombre" => utf8_encode($universidad['Nombre']),
					"Departamento" => utf8_encode($universidad['NombreDep'])
				);
			}
		}
		$conn->close();

	}
	header('Content-type: application/json');
	echo json_encode(array('universidades'=>$universidades));
}


?>
