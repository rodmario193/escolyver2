<?php

function NLog($Mensaje){
	date_default_timezone_set("America/Guatemala"); // Colocamos el timezone de Guatemala para que ingrese la hora correcta en el log
	$ArchivoBitacora = fopen(__DIR__ . '/../logs/' . date('Y-m-d') . "_Log.txt", "a");
	fwrite($ArchivoBitacora,"\n" . date('Y-m-d H:i:s') .  ' => ' . $Mensaje);
	fclose($ArchivoBitacora);
}

//NError("orientacion.php","Error de prueba", "1");
function NError($NumError, $NomArchivoOrigen, $MensajeError, $LineaError, $Extra){
	date_default_timezone_set("America/Guatemala"); // Colocamos el timezone de Guatemala para que ingrese la hora correcta en el log
	$ArchivoBitacora = fopen(__DIR__ . '/../logs/' . date('Y-m-d') . "_ErrorLog.txt", "a");
	fwrite($ArchivoBitacora, "\r\n" . date('Y-m-d H:i:s') . ' : Archivo ' . $NomArchivoOrigen .
		' : Linea ' . $LineaError . ' : NumError ' . $NumError . ' => ' . $MensajeError . ' => ' . $Extra);
	fclose($ArchivoBitacora);
}



?>
