
<?php

Interface iCarreraManager {
  public static function ObtenerPensum($IdCarrera);
}

class CarreraManager implements iCarreraManager{


  // Obtener la duración en meses para una carrera determinada
  public static function ObtenerDuracionPeriodo($IdCarrera){
    $out_Duracion = 0;

    $Sql =  "SELECT Duracion_Periodo_Meses " .
            "FROM   CARRERA " .
            "WHERE  IdCarrera = ? ";
    $Params = array(
      intval($IdCarrera)
    );
    $ResultSet = DatabaseManager::getQueryResult($Sql, $Params);
    if($ResultSet->num_rows == 1){
      $ResultRow = $ResultSet->fetch_assoc();
      $out_Duracion = intval($ResultRow['Duracion_Periodo_Meses']);
    }

    return $out_Duracion;
  }

  public static function ObtenerPensum($IdCarrera){
    $out_Periodos = array();
    $Sql = 	"SELECT		IdPeriodo, Orden " .
    "FROM 	PERIODO " .
    "WHERE	IdCarrera = ? " .
    "ORDER	BY Orden ASC";
    $Params = array(
      intval($IdCarrera)
    );
    $ResultSet = DatabaseManager::getQueryResult($Sql, $Params);
    if($ResultSet->num_rows > 0){
      while($ResultRow = $ResultSet->fetch_assoc()){
        $Cursos = array(); // Vector que contiene todos los cursos para un período determinado
        $IdPeriodo = intval($ResultRow['IdPeriodo']);
        $Sql = 	"SELECT  C.IdCurso, C.Nombre, C.Descripcion, " .
          "CASE  " .
          "	WHEN 		Categoria IS NULL THEN 'N/A' " .
          "	WHEN 		Categoria = '' THEN 'N/A' " .
          "	ELSE 		Categoria " .
          "END AS Categoria " .
          "FROM 	CURSO C, PERIODO P, CURSOXPERIODO CXP " .
          "WHERE	C.IdCurso = CXP.IdCurso ".
          "AND		P.IdPeriodo = CXP.IdPeriodo ".
          "AND		P.IdPeriodo = ? ";
        $Params = array(
          $IdPeriodo
        );
        $ResultSet2 = DatabaseManager::getQueryResult($Sql, $Params);
        // Rellenar el vector de cursos
        if ($ResultSet2->num_rows > 0) { //Validar si el período tiene cursos
					while($ResultRow2 = $ResultSet2->fetch_assoc()) {
						$Cursos[] = array(
							"Id" => utf8_encode($ResultRow2['IdCurso']),
							"Nombre" => utf8_encode($ResultRow2['Nombre']),
							"Descripcion" => utf8_encode($ResultRow2['Descripcion']),
							"Categoria" => utf8_encode($ResultRow2['Categoria'])
						);
					}
				}else{ // En caso el período no tenga cursos agregar un curso dummy con las siglas de No Disponible
					$Cursos =  array(
						array(
							"Id"=>0,
							"Nombre"=>"N/D",
							"Descripcion"=>"N/D",
							"Categoria"=>"N/D"
						)
					);
				}
        //Crear nuevo registro de período con su vector de cursos respectivo
        $out_Periodos[] = array(
					"Id" => $ResultRow['IdPeriodo'],
					"Orden" => $ResultRow['Orden'],
					"Cursos" => $Cursos,
					"BanderaDesp" => true
				);
      }

    }

    return $out_Periodos;
  }

}


 ?>
