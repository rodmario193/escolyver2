<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization");

require_once('Pagadito.php');

$configs = include('config.php');
include('bitacora.php');
include('correo.php');
include('token.php');
include(__DIR__ . '/error-manager/error_manager.php');
include(__DIR__ . '/database/DatabaseManager.php');

//Sandbox

define("UID", "47641bfdf0bf904646e287b36f5bf69b");
define("WSK", "5107ab2022c48fc7f1309b63a801c907");

//Produccion
/*
define("UID", "60de88135d493795bf8b71241bf8c5bf");
define("WSK", "28050a027eb4b45ab02d8a6535bcc7af");
*/

$postdata = file_get_contents("php://input");

if(isset($postdata)) {
	$request = json_decode($postdata);
	if($request->Operacion == "datos"){
		WO_ObtenerDatosFacturacion($request->Param1);
	}else if($request->Operacion == "procesar_compra"){
		WO_ProcesarPago($request->Param1, $request->IdTransaccion, $request->IdItem);
	}else if($request->Operacion == "opciones"){
		WO_ListaOpcionesCompras($request->TipoCompra);
	}else if($request->Operacion == "procesar_compra2"){
		WO_ProcesarPagoPagadito($request->Param1, $request->TokenPagadito, $request->Param2);
	}else if($request->Operacion == "tk_b"){
		WO_ObtenerTokenCompraBusqueda($request->Param1);
	}else if($request->Operacion == "tk_o"){
		WO_ObtenerTokenCompraOrientacion($request->Param1);
	}
}

/* ****** Operaciones Web ****** */

function WO_ObtenerTokenCompraBusqueda($Token){
		global $configs;
		$out_Token = '';
		if(SesionDeUsuarioAutenticada($Token)){
			$out_Token = $configs['token_busqueda'];
			ActualizarTokenUsuarioConToken($Token);
		}
		$Respuesta = array(
			"Token" => $out_Token
		);
		header('Content-type: application/json');
		echo json_encode(array('Respuesta'=>$Respuesta));
}

function WO_ObtenerTokenCompraOrientacion($Token){
		global $configs;
		$out_Token = '';
		if(SesionDeUsuarioAutenticada($Token)){
			$out_Token = $configs['token_orientacion'];
			ActualizarTokenUsuarioConToken($Token);
		}
		$Respuesta = array(
			"Token" => $out_Token
		);
		header('Content-type: application/json');
		echo json_encode(array('Respuesta'=>$Respuesta));
}

function WO_ListaOpcionesCompras($TipoCompra){
	global $configs;
	$CODIGO = 0;
	$compras = array();
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);

	if(!$conn->connect_error){
		$sql = 	"SELECT 	IdItem, Cantidad, Saldo " .
				"FROM 	ITEM_COMPRA " .
				"WHERE 	Tipo = '$TipoCompra'	";

		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($compra = $result->fetch_assoc()) {
				$compras[] = array(
					"IdItem" => $compra['IdItem'],
					"Cantidad" => intval($compra['Cantidad']),
					"Saldo" => floatval($compra['Saldo'])
				);
			}
			$CODIGO = 1;
		}
		$conn->close();
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"compras" => $compras
	);
	header('Content-type: application/json');
	echo json_encode(array('respuesta'=>$RESPUESTA));
}


function WO_ProcesarPago($TOKEN, $ID_TRANSACCION, $ID_ITEM){
	$CODIGO = 0;
	$MENSAJE = '';
	$CANTIDAD_BUSQUEDAS = 0;
	$TRANSACCION_VALIDA = 0;
	$TRANSACCION_NO_REPETIDA = 0;

	$ID_USUARIO = ValidarToken($TOKEN);

	if($ID_USUARIO!=''){
		if(ValidarCompraEnPaypal($ID_TRANSACCION)){
			//Validar que transaccion no exista en tabla compra
			if(ValidarTransaccionNoRepetida($ID_TRANSACCION)){
				//Registrar nueva compra
				if(RegistrarCompra($ID_USUARIO,$ID_TRANSACCION,$ID_ITEM, $CANTIDAD_BUSQUEDAS)){
					$CODIGO = 1;
				}else{
					$MENSAJE = 'Error en registro de compra, favor guarde su id de transacción de PayPal y contacte al administrador';
				}
			}else{
				$MENSAJE = 'Id de transacción ya ha sido utilizado anteriormente en otra compra';
			}
		}else{
			$MENSAJE = 'Id de transacción no válido';
		}
	}else{
		$MENSAJE = 'Error en validación de sesión, no se pudo registrar la compra';
	}

	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"cantidad" => $CANTIDAD_BUSQUEDAS,
		"mensaje" => $MENSAJE
	);

	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}

// busqueda
//WO_ProcesarPagoPagadito('token', '1b8436b15410bbfabf1d6abbc4a262b5','71a45328063be6e4873e774db560a134');
// orientacion
//WO_ProcesarPagoPagadito('token', '83cf1f00bb1a1ca5c8cc2b96c2c67909','bba84e3e12ea7a891e02283fbcd46a88');
function WO_ProcesarPagoPagadito($Token, $TokenPagadito, $TokenIdCompra){
	$CodRespuesta = 0;
	$NumItems = 0;
	$MsjValidacion = ""; // Variable usada para almacenar la respuesta de las diferentes validaciones realizadas
	global $configs;
	$IdUsuario = ValidarToken($Token);
	//$IdUsuario = 'AUGUSTO90';
	$TipoCompra = "";
	$IdItemCompra = "";
	ActualizarTokenUsuario($IdUsuario);
	if($IdUsuario!=""){
		//Validar token de identificación de compra
		if($TokenIdCompra==$configs['token_busqueda']){
			$MsjValidacion = "OK";
			$TipoCompra = "B";
		}else if($TokenIdCompra==$configs['token_orientacion']){
			$MsjValidacion = "OK";
			$TipoCompra = "O";
		}else{
			$MsjValidacion = "Error en autenticación inicial de compra, contacte al administrador";
		}

		//echo $TipoCompra;

		// Validar token de transacción en Pagadito
		if($MsjValidacion=="OK"){
			$MsjValidacion = ValidarCompraEnPagadito($TokenPagadito);
		}

		if($MsjValidacion=="OK"){ // El token recibido es válido
			// Validar que token no este registrado en compra registrada anteriormente
			if(!TokenNoUsadoEnCompra($TokenPagadito)) $MsjValidacion = "El token de transacción ya ha sido utilizado en otra compra anteriormente";
		}

		//Validar que el monto gastado corresponda a un item del tipo que indica el TokenNoUsadoEnCompra
		if($MsjValidacion=="OK"){ // El codigo de transacción no ha sido usado en una compra anterior
			$IdItemCompra = ObtenerIdItemCompra($TokenPagadito, $TipoCompra);
			if($IdItemCompra=='') $MsjValidacion = 'No se pudo contrastar el saldo de la transacci&oacute;n con un item de compra registrado';
		}

		if($MsjValidacion=="OK"){ //El monto pagado concuerda con el item indicado
			// Registrar nueva compra y asignar búsquedas a usuario
			if($TipoCompra=='B'){
					if( RegistrarCompraPagadito($IdUsuario, $TokenPagadito, $IdItemCompra, $NumItems) != 1 ){
						$MsjValidacion = "Error en registro de búsquedas, intente nuevamente";
					}
			}
			if($TipoCompra=='O'){
				if( RegistrarCompraOrientacionPagadito($IdUsuario, $TokenPagadito, $IdItemCompra, $NumItems) != 1 ){
					$MsjValidacion = "Error en registro de orientaciones, intente nuevamente";
				}
			}
		}

	}
	if($MsjValidacion=="OK"){
		$CodRespuesta = 1;
		$MsjValidacion = "Compra registrada exitosamente";
	}
	$TipoCompraDetalle = '';
	if($TipoCompra=="B") $TipoCompraDetalle = "Búsqueda";
	if($TipoCompra=="O") $TipoCompraDetalle = "Orientación";
	$Respuesta = array(
		"Codigo" => $CodRespuesta,
		"Mensaje" => $MsjValidacion,
		"TipoCompra" => $TipoCompraDetalle,
		"TipoCompraMin" => $TipoCompra
	);
	header('Content-type: application/json');
	echo json_encode(array("Respuesta"=>$Respuesta));
}

function WO_ObtenerDatosFacturacion($TOKEN){
	$USUARIO = null;
	$CODIGO = 0;
	global $configs;
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);

	$ID_USUARIO = ValidarToken($TOKEN);
	ActualizarTokenUsuario($ID_USUARIO);
	if($ID_USUARIO!=''){
		if(!$conn->connect_error){

			$query = $conn->prepare("SELECT U.NombreFacturacion, U.ApellidoFacturacion, U.Nit, D.Nombre " .
					"FROM 	USUARIO U, DEPARTAMENTO D " .
					"WHERE	U.DepartamentoFacturacion = D.IdDepartamento " .
					"AND 	U.IdUsuario = ? ");
			$query->bind_param('s', $ID_USUARIO);
			$query->execute();
			$result = $query->get_result();
			if ($result->num_rows == 1) {
				$usuario = $result->fetch_assoc();
				$USUARIO = array(
					"Id" => $ID_USUARIO,
					"NombreFacturacion" => utf8_encode($usuario['NombreFacturacion']),
					"ApellidoFacturacion" => utf8_encode($usuario['ApellidoFacturacion']),
					"Nit" => $usuario['Nit'],
					"NombreDepFacturacion" => utf8_encode($usuario['Nombre'])
				);
				$CODIGO = 1;
			}
			$conn->close();
		}
	}
	$RESPUESTA = array(
		"usuario" => $USUARIO,
		"codigo" => $CODIGO
	);

	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));

}


/* ****** Operaciones adicionales ****** */

function ObtenerIdItemCompra($TokenPagadito, $TipoCompra){
	$out_IdItem = '';

	$SaldoCompra = ObtenerSaldoDeCompra($TokenPagadito);
	$Sql = "SELECT	IdItem " .
		"FROM 	ITEM_COMPRA " .
		"WHERE 	Saldo = $SaldoCompra " .
		"AND 		Tipo = '$TipoCompra' ";
		//echo $Sql;
		$Params = array(
		);
		$ResultSet = DatabaseManager::getQueryResult($Sql, $Params);
		if($ResultSet->num_rows==1){
			$ResultRow = $ResultSet->fetch_assoc();
			$out_IdItem = $ResultRow['IdItem'];
		}

		return $out_IdItem;
}


function TokenNoUsadoEnCompra($TokenPagadito){
	global $configs;
	$TokenUnico = true;
	$mysqli_conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$mysqli_conn->connect_error){
		$sql = 	"SELECT IdTransaccion FROM COMPRA WHERE IdTransaccion = ? ";
		$query = $mysqli_conn->prepare($sql);
		$query->bind_param('s', $TokenPagadito);
		$query->execute();
		if($query->get_result()->num_rows >= 1) $TokenUnico = false;
		$mysqli_conn->close();
	}
	return $TokenUnico;
}


/*
$NB = 0;
echo RegistrarCompraPagadito('AUGUSTO90','ffeabbbe52d331acd9f7d45bf38dcf87','13',$NB);
*/
function RegistrarCompraPagadito($IdUsuario, $TokenPagadito, $IdItemCompra, &$NumeroBusquedas){
	global $configs;
	$CodRespuesta = 0;
	$mysqli_conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$mysqli_conn->connect_error){
		$sql = "CALL SP_REGISTRAR_NUEVA_COMPRA('$IdUsuario', '$TokenPagadito', '$IdItemCompra', @respuesta, @numbsqueda)";
		if (!$mysqli_conn->query("SET @respuesta = 0") || !$mysqli_conn->query("SET @numbusqueda = 0") || !$mysqli_conn->query($sql))
		{
				//Fallo alguno de los comandos para preparar la ejecución del procedimiento almacenado, no realizamos ninguna acción

		}else{ // El procedimiento almacenado se ejecutó exitosamente
				$sql = "SELECT @respuesta as respuesta, @numbusqueda as cantidad"; // Obtenemos valor de variables de respuesta devueltas por el procedimiento
				if ( $resultado = $mysqli_conn->query($sql) ) {
						if( $resultado->fetch_assoc()['respuesta'] == '1' ) $CodRespuesta = 1; // Todos los pasos del registro de compra se ejecutaron correctamente
						$NumeroBusquedas = intval($resultado->fetch_assoc()['cantidad']);
				}
		}
		$mysqli_conn->close();
	}
	return $CodRespuesta;
}

function RegistrarCompraOrientacionPagadito($IdUsuario, $TokenPagadito, $IdItemCompra, &$NumeroOrientaciones){
	global $configs;
	$CodRespuesta = 0;
	$mysqli_conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$mysqli_conn->connect_error){
		$sql = "CALL SP_REGISTRAR_NUEVA_COMPRA_ORIENTACION('$IdUsuario', '$TokenPagadito', '$IdItemCompra', @respuesta, @numbusqueda)";
		if (!$mysqli_conn->query("SET @respuesta = 0") || !$mysqli_conn->query("SET @numbusqueda = 0") || !$mysqli_conn->query($sql))
		{
				//Fallo alguno de los comandos para preparar la ejecución del procedimiento almacenado, no realizamos ninguna acción

		}else{ // El procedimiento almacenado se ejecutó exitosamente
				$sql = "SELECT @respuesta as respuesta, @numbusqueda as cantidad"; // Obtenemos valor de variables de respuesta devueltas por el procedimiento
				if ( $resultado = $mysqli_conn->query($sql) ) {
						if( $resultado->fetch_assoc()['respuesta'] == '1' ) $CodRespuesta = 1; // Todos los pasos del registro de compra se ejecutaron correctamente
						$NumeroOrientaciones = intval($resultado->fetch_assoc()['cantidad']);
				}
		}
		$mysqli_conn->close();
	}
	return $CodRespuesta;
}


function ObtenerSaldoDeCompra($TokenPagadito){
	$SaldoCompra = 0;
	$Pagadito = new Pagadito(UID, WSK);
	$Pagadito->mode_sandbox_on();
	 if ($Pagadito->connect()) {
		 if( $Pagadito->get_status($TokenPagadito) ){
			 $SaldoCompra = floatval($Pagadito->get_rs_amount());
 		}
	}
	return $SaldoCompra;
}

function ValidarCompraEnPagadito($TokenPagadito){
	$MsjRespuesta = "";
	$Pagadito = new Pagadito(UID, WSK);
	$Pagadito->mode_sandbox_on();
	 if ($Pagadito->connect()) {
		if ($Pagadito->get_status($TokenPagadito)) {
			if($Pagadito->get_rs_status()=='COMPLETED'){
				$MsjRespuesta = "OK";
			}else{
				$MsjRespuesta = "Estado de transacción no completado";
			}
		}else{
			$MsjRespuesta = "No es un token de transacción válido";
		}
	}else{
		$MsjRespuesta = "Error de conexión a Pagadito, contacté a un administrador";
	}
	return $MsjRespuesta;
}


function startsWith($haystack, $needle){
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

/*
function RegistrarCompra($ID_USUARIO, $ID_TRANSACCION,$ID_ITEM, &$NUM_BUSQUEDAS){
	global $configs;
	$RESPUESTA = 0;
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$conn->connect_error){

		if (!$conn->query("SET @id_usuario = '" . $ID_USUARIO . "'")
			|| !$conn->query("SET @id_tran = '" . $ID_TRANSACCION . "'")
			|| !$conn->query("SET @id_item = '" . $ID_ITEM . "'")
			|| !$conn->query("SET @respuesta = 0")
			|| !$conn->query("SET @numbusqueda = 0")
			|| !$conn->query("CALL SP_REGISTRAR_NUEVA_COMPRA(@id_usuario, @id_tran, @id_item, @respuesta, @numbusqueda)")) {
			echo "Falló CALL: (" . $conn->errno . ") " . $conn->error;
		}

		if (!($resultado = $conn->query("SELECT @respuesta as respuesta, @numbusqueda as cantidad"))) {
			echo "Falló la obtención: (" . $conn->errno . ") " . $conn->error;
		}
		$fila = $resultado->fetch_assoc();
		if($fila['respuesta']=='1'){
			$RESPUESTA = 1;
		}
		$NUM_BUSQUEDAS = (int)$fila['cantidad'];

		$conn->close();
	}
	return $RESPUESTA;
}

function ValidarCompraEnPaypal($ID_TRANSACCION){
	$RESPUESTA = false;
	$TOKEN = "oFcD811avonZMj--ZSVrexC6MnTOQ9rI9ml1mXLC6tC6LoxrkoRWzBszu8G";
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL,"https://www.paypal.com/cgi-bin/webscr");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
				"cmd=_notify-synch&tx=" . $ID_TRANSACCION . "&at=" . $TOKEN);

	// in real life you should use something like:
	// curl_setopt($ch, CURLOPT_POSTFIELDS,
	//          http_build_query(array('postvar1' => 'value1')));

	// receive server response ...
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$server_output = curl_exec ($ch);

	curl_close ($ch);

	// further processing ....
	//echo $server_output;

	if (startsWith($server_output, 'SUCCESS')){
		$RESPUESTA = true;
	}
	return $RESPUESTA;
}

*/

?>
