<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization");

$configs = include('config.php');
include('bitacora.php');
include('correo.php');
include('token.php');
include(__DIR__ . '/error-manager/error_manager.php');
include(__DIR__ . '/database/DatabaseManager.php');

$postdata = file_get_contents("php://input");

if(isset($postdata)) {
	$request = json_decode($postdata);
	if($request->Operacion == "lista"){
		WO_ListaAgrupaciones();
	}else if($request->Operacion == "detalle"){
		WO_DetalleAgrupacion($request->IdAgrupacion, $request->Param1);
	}else if($request->Operacion == "agregar"){
		WO_AgregarAgrupacion($request->Agrupacion, $request->Param1 );
	}else if($request->Operacion == "modificar"){
		WO_ModificarAgrupacion($request->Agrupacion, $request->Param1);
	}else if($request->Operacion == "eliminar"){
		WO_EliminarAgrupacion($request->IdGrupo, $request->Param1);
	}else if($request->Operacion == "tiposgrupo"){
		WO_ListaTiposGrupo();
	}
}

/* ****** Operaciones Web ****** */

function WO_EliminarAgrupacion($ID_GRUPO, $TOKEN){
	$CODIGO = 0;
	global $configs;

	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql =  "DELETE FROM GRUPO WHERE IdGrupo = $ID_GRUPO";

			if (mysqli_query($conn, $sql)) {
				$CODIGO = 1;
			}else{
				$CODIGO = $conn->errno;
			}
			$conn->close();
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_ModificarAgrupacion($AGRUPACION, $TOKEN){
	$CODIGO = 0;
	global $configs;
	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){

			$NOMBRE = $AGRUPACION->Nombre;
			$TIPOGRUPO = $AGRUPACION->TipoGrupo;
			$DESCRIPCION = $AGRUPACION->Descripcion;
			$ID_GRUPO = $AGRUPACION->Id;
			$PRUEBA = $AGRUPACION->Prueba;

			$sql =  "UPDATE	GRUPO " .
					"SET TipoGrupo = $TIPOGRUPO, ".
					"Descripcion = '$DESCRIPCION', " .
					"Nombre = '$NOMBRE', " .
					"UsuarioModifica = '$ID_USUARIO', " .
					"FechaModificacion = NOW(), " .
					"Prueba = $PRUEBA " .
					"WHERE IdGrupo = $ID_GRUPO";
			$conn->set_charset("utf8");
			if (mysqli_query($conn, $sql)) {
				$CODIGO = 1;
			}else{
				$CODIGO = $conn->errno;
			}
			$conn->close();

		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_AgregarAgrupacion($AGRUPACION, $TOKEN){
	$CODIGO = 0;
	global $configs;
	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){

			$NOMBRE = $AGRUPACION->Nombre;
			$TIPOGRUPO = $AGRUPACION->TipoGrupo;
			$DESCRIPCION = $AGRUPACION->Descripcion;
			$PRUEBA = $AGRUPACION->Prueba;

			$sql =  "INSERT INTO GRUPO (Nombre, TipoGrupo, Descripcion, UsuarioModifica, FechaModificacion, Prueba)" .
					" VALUES('$NOMBRE',$TIPOGRUPO,'$DESCRIPCION', '$ID_USUARIO', NOW(), $PRUEBA)";
			$conn->set_charset("utf8");
			if (mysqli_query($conn, $sql)) {
				$CODIGO = 1;
			}else{
				$CODIGO = $conn->errno;
			}
			$conn->close();

		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_DetalleAgrupacion($ID_GRUPO, $TOKEN){
	global $configs;
	$CODIGO = 0;
	$AGRUPACION = "";
	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql = 	"SELECT 	IdGrupo, TipoGrupo, Nombre, TipoGrupo, Descripcion, Prueba " .
					"FROM 		GRUPO " .
					"WHERE 		IdGrupo = $ID_GRUPO";

			$result = $conn->query($sql);
			if ($result->num_rows == 1) {
				$agrupacion = $result->fetch_assoc();
				$AGRUPACION = array(
					"Id" => $agrupacion['IdGrupo'],
					"Nombre" => utf8_encode($agrupacion['Nombre']),
					"TipoGrupo" => $agrupacion['TipoGrupo'],
					"Descripcion" => utf8_encode($agrupacion['Descripcion']),
					"Prueba" => intval($agrupacion['Prueba'])
				);
				$CODIGO = 1;
			}

			$conn->close();

		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"Agrupacion" => $AGRUPACION
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_ListaTiposGrupo(){
	global $configs;
	$tiposgrupo = array();
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);

	if(!$conn->connect_error){
		$sql = 	"SELECT 	IdTipoGrupo, Descripcion " .
				"FROM 		TIPO_GRUPO ";

		$result = $conn->query($sql);

		if ($result->num_rows > 0) {

			while($tipogrupo = $result->fetch_assoc()) {
				$tiposgrupo[] = array(
					"IdTipoGrupo" => $tipogrupo['IdTipoGrupo'],
					"Descripcion" => utf8_encode($tipogrupo['Descripcion'])
				);
			}

		}

		$conn->close();

	}

	header('Content-type: application/json');
	echo json_encode(array('tiposgrupo'=>$tiposgrupo));

}


function WO_ListaAgrupaciones(){
	global $configs;
	$agrupaciones = array();
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);

	if(!$conn->connect_error){
		$sql = 	"SELECT 	G.IdGrupo, G.Nombre, TP.Descripcion AS Tipo, G.Prueba " .
				"FROM 		TIPO_GRUPO TP, GRUPO G ".
				"WHERE 		TP.IdTipoGrupo = G.TipoGrupo";

		$result = $conn->query($sql);

		if ($result->num_rows > 0) {

			while($agrupacion = $result->fetch_assoc()) {
				$agrupaciones[] = array(
					"Id" => $agrupacion['IdGrupo'],
					"Nombre" => utf8_encode($agrupacion['Nombre']),
					"Tipo" => utf8_encode($agrupacion['Tipo']),
					"Prueba" => intval($agrupacion['Prueba'])
				);
			}

		}

		$conn->close();

	}

	header('Content-type: application/json');
	echo json_encode(array('agrupaciones'=>$agrupaciones));

}
