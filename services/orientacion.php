<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization");

$configs = include('config.php');
include('bitacora.php');
include('correo.php');
include('token.php');
include(__DIR__ . '/error-manager/error_manager.php');
include(__DIR__ . '/database/DatabaseManager.php');


$postdata = file_get_contents("php://input");

if(isset($postdata)) {
	$request = json_decode($postdata);
	if($request->Operacion == "lista"){
		WO_ListaOrientaciones($request->Param1, $request->IdUsuario);
	}else if($request->Operacion == "detalle"){
		WO_DetalleOrientacionAdmin($request->Param1, $request->IdOrientacion);
	}else if($request->Operacion == "correo"){
		WO_EnviarCorreoOrientacion($request->Param1, $request->IdOrientacion, $request->NumExamen);
	}else if($request->Operacion == "finalizar"){
		WO_FinalizarExamenOrientacion($request->Param1, $request->IdOrientacion, $request->NumExamen);
	}else if($request->Operacion == "lista_usuario"){
		WO_ListaOrientacionesDeUsuario($request->Param1);
	}else if($request->Operacion == "detalle_usuario"){
		WO_DetalleOrientacionUsuario($request->Param1, $request->IdOrientacion);
	}else if($request->Operacion == "lista_grupos"){
		WO_ObtenerGruposParaOrientacion($request->Param1);
	}else if($request->Operacion == "act_grupos"){
		WO_GuardarGruposDeOrientacion($request->Param1, $request->Grupo1, $request->Grupo2, $request->Grupo3, $request->IdOrientacion);
	}else if($request->Operacion == "estado"){
		WO_ValidarOrientacionActiva($request->Param1, $request->IdOrientacion);
	}else if($request->Operacion == "registro"){
		WO_RegistrarOrientacion($request->Param1, $request->Orientacion);
	}else if($request->Operacion == "detallereg"){
		WO_ObtenerDatosRegistroDeOrientacion($request->Param1, $request->IdOrientacion);
	}
}


//WO_ObtenerDatosRegistroDeOrientacion('token','6');
function WO_ObtenerDatosRegistroDeOrientacion($Token, $IdOrientacion){
	$out_Orientacion = array();
	$out_Exito = false;
	$out_Mensaje = '';
	if(SesionDeAdminAutenticada($Token)){
	//if(true){
		$Sql = "SELECT Actividad1, Actividad2, Actividad3, Actividad4, Actividad5, Actividad6, " .
						"Objeto1, Objeto2, Objeto3, Objeto4, Objeto5, Objeto6, " . //Inicio pendientes
						"Logro1, Logro2, Logro3, Logro4, " .
						"Pregunta1Opcion1, Pregunta1Opcion2, Pregunta1Opcion3, Pregunta1Opcion4, " .
						"Pregunta2Opcion1, Pregunta2Opcion2, Pregunta2Opcion3, Pregunta2Opcion4, ".
						"Pregunta3Opcion1, Pregunta3Opcion2, Pregunta3Opcion3, Pregunta3Opcion4, Pregunta3Opcion5, Pregunta3Opcion6, Pregunta3Opcion7, " .
						"EstudioMama, GraduacionMama, TrabajoMama, EstudioPapa, GraduacionPapa, TrabajoPapa, " .
						"EstudiosHermanos1, EstudiosHermanos2, EstudiosHermanos3, EstudiosHermanos4, " .
						"EstudiosFamiliares1, EstudiosFamiliares2, EstudiosFamiliares3, EstudiosFamiliares4, EstudiosFamiliares5, EstudiosFamiliares6 " .
						"FROM REGISTRO_ORIENTACION " .
						"WHERE IdOrientacion = ? ";
		$Params = array(
			intval($IdOrientacion)
		);
		$ResultSet = DatabaseManager::getQueryResult($Sql, $Params);
		if($ResultSet->num_rows ==1){
			$ResultRow = $ResultSet->fetch_assoc();
			$out_Orientacion = array(
				"Actividad1" => $ResultRow['Actividad1'],
				"Actividad2" => $ResultRow['Actividad2'],
				"Actividad3" => $ResultRow['Actividad3'],
				"Actividad4" => $ResultRow['Actividad4'],
				"Actividad5" => $ResultRow['Actividad5'],
				"Actividad6" => $ResultRow['Actividad6'],
				"Objeto1" => $ResultRow['Objeto1'],
				"Objeto2" => $ResultRow['Objeto2'],
				"Objeto3" => $ResultRow['Objeto3'],
				"Objeto4" => $ResultRow['Objeto4'],
				"Objeto5" => $ResultRow['Objeto5'],
				"Objeto6" => $ResultRow['Objeto6'],
				"Logro1" => $ResultRow['Logro1'],
				"Logro2" => $ResultRow['Logro2'],
				"Logro3" => $ResultRow['Logro3'],
				"Logro4" => $ResultRow['Logro4'],
				"Pregunta1Opcion1" => intval($ResultRow['Pregunta1Opcion1']),
				"Pregunta1Opcion2" =>  intval($ResultRow['Pregunta1Opcion2']),
				"Pregunta1Opcion3" => intval($ResultRow['Pregunta1Opcion3']),
				"Pregunta1Opcion4" => intval($ResultRow['Pregunta1Opcion4']),
				"Pregunta2Opcion1" => intval($ResultRow['Pregunta2Opcion1']),
				"Pregunta2Opcion2" => intval($ResultRow['Pregunta2Opcion2']),
				"Pregunta2Opcion3" => intval($ResultRow['Pregunta2Opcion3']),
				"Pregunta2Opcion4" => intval($ResultRow['Pregunta2Opcion4']),
				"Pregunta3Opcion1" => intval($ResultRow['Pregunta3Opcion1']),
				"Pregunta3Opcion2" => intval($ResultRow['Pregunta3Opcion2']),
				"Pregunta3Opcion3" => intval($ResultRow['Pregunta3Opcion3']),
				"Pregunta3Opcion4" => intval($ResultRow['Pregunta3Opcion4']),
				"Pregunta3Opcion5" => intval($ResultRow['Pregunta3Opcion5']),
				"Pregunta3Opcion6" => intval($ResultRow['Pregunta3Opcion6']),
				"Pregunta3Opcion7" => intval($ResultRow['Pregunta3Opcion7']),
				"EstudioMama" =>  $ResultRow['EstudioMama'],
				"GraduacionMama" => intval($ResultRow['GraduacionMama']),
				"TrabajoMama" => $ResultRow['TrabajoMama'],
				"EstudioPapa" => $ResultRow['EstudioPapa'],
				"GraduacionPapa" => intval($ResultRow['GraduacionPapa']),
				"TrabajoPapa" => $ResultRow['TrabajoPapa'],
				"EstudiosHermanos1" => $ResultRow['EstudiosHermanos1'],
				"EstudiosHermanos2" => $ResultRow['EstudiosHermanos2'],
				"EstudiosHermanos3" => $ResultRow['EstudiosHermanos3'],
				"EstudiosHermanos4" => $ResultRow['EstudiosHermanos4'],
				"EstudiosFamiliares1" => $ResultRow['EstudiosFamiliares1'],
				"EstudiosFamiliares2" => $ResultRow['EstudiosFamiliares2'],
				"EstudiosFamiliares3" => $ResultRow['EstudiosFamiliares3'],
				"EstudiosFamiliares4" => $ResultRow['EstudiosFamiliares4'],
				"EstudiosFamiliares5" => $ResultRow['EstudiosFamiliares5'],
				"EstudiosFamiliares6" => $ResultRow['EstudiosFamiliares6']
			);
			$out_Exito = true;
		}
	}
	if((!$out_Exito)&&($out_Mensaje=="")) $out_Mensaje = "Error obteniendo datos de registro de orientación";

	$Respuesta = array(
		"Exito" => $out_Exito,
		"Mensaje" => $out_Mensaje,
		"Orientacion" => $out_Orientacion
	);
	header('Content-type: application/json');
	echo json_encode(array("Respuesta"=>$Respuesta));
}


function WO_RegistrarOrientacion($Token, $Orientacion){
	$out_Exito = false;
	$out_Mensaje = '';

	if( SesionDeUsuarioAutenticada($Token)){
			ActualizarTokenUsuarioConToken($Token);
			$Sql = 	"INSERT INTO REGISTRO_ORIENTACION (IdOrientacion, Actividad1, Actividad2, Actividad3, Actividad4, Actividad5, Actividad6, " .
							"Objeto1, Objeto2, Objeto3, Objeto4, Objeto5, Objeto6, " . //Inicio pendientes
							"Logro1, Logro2, Logro3, Logro4, " .
							"Pregunta1Opcion1, Pregunta1Opcion2, Pregunta1Opcion3, Pregunta1Opcion4, " .
							"Pregunta2Opcion1, Pregunta2Opcion2, Pregunta2Opcion3, Pregunta2Opcion4, ".
							"Pregunta3Opcion1, Pregunta3Opcion2, Pregunta3Opcion3, Pregunta3Opcion4, Pregunta3Opcion5, Pregunta3Opcion6, Pregunta3Opcion7, " .
							"EstudioMama, GraduacionMama, TrabajoMama, EstudioPapa, GraduacionPapa, TrabajoPapa, " .
							"EstudiosHermanos1, EstudiosHermanos2, EstudiosHermanos3, EstudiosHermanos4, " .
							"EstudiosFamiliares1, EstudiosFamiliares2, EstudiosFamiliares3, EstudiosFamiliares4, EstudiosFamiliares5, EstudiosFamiliares6) ".
							"VALUES ( " . $Orientacion->Id . ", '" . $Orientacion->Actividad1 . "','" . $Orientacion->Actividad2 . "','" . $Orientacion->Actividad3 . "', " .
								"'" . $Orientacion->Actividad4 . "','" . $Orientacion->Actividad5 . "','" . $Orientacion->Actividad6 . "', " .
								"'" . $Orientacion->Objeto1 . "', '" . $Orientacion->Objeto2 . "', '" . $Orientacion->Objeto3 . "', " .
								"'" . $Orientacion->Objeto4 . "', '" . $Orientacion->Objeto5 . "', '" . $Orientacion->Objeto6 . "', " . // Inicio pendientes
								"'" . $Orientacion->Logro1 . "', '" . $Orientacion->Logro2 . "','" . $Orientacion->Logro3 . "','" . $Orientacion->Logro4 . "', " .
								" " . $Orientacion->Pregunta1Opcion1 . ", " . $Orientacion->Pregunta1Opcion2 . ", " . $Orientacion->Pregunta1Opcion3 . ", " . $Orientacion->Pregunta1Opcion4 . ", " .
								" " . $Orientacion->Pregunta2Opcion1 . ", " . $Orientacion->Pregunta2Opcion2 . ", " . $Orientacion->Pregunta2Opcion3 . ", " . $Orientacion->Pregunta2Opcion4 . ", " .
								" " . $Orientacion->Pregunta3Opcion1 . ", " . $Orientacion->Pregunta3Opcion2 . ", " . $Orientacion->Pregunta3Opcion3 . ", " . $Orientacion->Pregunta3Opcion4 . ", " .
								" " . $Orientacion->Pregunta3Opcion5 . ", " . $Orientacion->Pregunta3Opcion6 . ", " . $Orientacion->Pregunta3Opcion7 . ", " .
								"'" . $Orientacion->EstudioMama . "', " . $Orientacion->GraduacionMama . ", '" . $Orientacion->TrabajoMama . "', " .
								"'" . $Orientacion->EstudioPapa . "', " . $Orientacion->GraduacionPapa . ", '" . $Orientacion->TrabajoPapa . "', " .
								"'" . $Orientacion->EstudiosHermanos1 . "', '" . $Orientacion->EstudiosHermanos2 . "','" . $Orientacion->EstudiosHermanos3 . "','" . $Orientacion->EstudiosHermanos4 . "', " .
								"'" . $Orientacion->EstudiosFamiliares1 . "', '" . $Orientacion->EstudiosFamiliares2 . "','" . $Orientacion->EstudiosFamiliares3 . "','" . $Orientacion->EstudiosFamiliares4 . "', " .
								"'" . $Orientacion->EstudiosFamiliares5 . "', '" . $Orientacion->EstudiosFamiliares6 . "'" .
								")";

			$Params = array();
			$out_Exito = DatabaseManager::executeQuery($Sql, $Params);

			// Cambiar estado de orientación a activa
			if($out_Exito){
				$Sql = "UPDATE ORIENTACION set Estado = 1 WHERE IdOrientacion = ? ";
				$Params = array(
					intval( $Orientacion->Id)
				);
				$out_Exito = DatabaseManager::executeQuery($Sql, $Params);
			}

			if($out_Exito) $out_Mensaje = "¡Buenísmo!";
	}
	if( (!$out_Exito) && ($out_Mensaje=='') ) $out_Mensaje = 'Error registrando nueva orientación, por favor contacte al administrador';

	$Respuesta = array(
		"Exito" => $out_Exito,
		"Mensaje" => $out_Mensaje
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$Respuesta));

}

function WO_ValidarOrientacionActiva($Token, $IdOrientacion){
	$OrientacionActiva = false;
	if(SesionDeUsuarioAutenticada($Token)){ //La sesión del usuario es válida
	//if(true){
		$Sql = "SELECT 	Estado " .
					 "FROM 		ORIENTACION " .
					 "WHERE 	IdOrientacion = ? ";
		$Params = array(
			intval($IdOrientacion)
		);
		$ResultSet = DatabaseManager::getQueryResult($Sql, $Params);
		$ResultRow =  $ResultSet->fetch_assoc();
		if(!is_null($ResultRow)){
			if(intval($ResultRow['Estado'])==1) $OrientacionActiva = true;
		}
	}
	$Respuesta = array(
		"OrientacionActiva" => $OrientacionActiva
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$Respuesta));
}

//WO_GuardarGruposDeOrientacion('token','1','6','2','1');
function WO_GuardarGruposDeOrientacion($Token, $Grupo1, $Grupo2, $Grupo3, $IdOrientacion){
	$CodigoRespuesta = 0;
	$MensajeRespuesta = '';
	global $configs;
	$IdAdmin = ValidarTokenAdmin($Token);
	//$IdAdmin = 'AUGUSTO90';
	if($IdAdmin!=''){
		//Validar que no se asignen dos grupos iguales en la misma orientaciones
		if( ($Grupo1==$Grupo2) || ($Grupo1==$Grupo3) || ($Grupo2==$Grupo3) ){
			$MensajeRespuesta = 'No se pueden asignar dos carreras similares en la misma orientaci&oacute;n';
		}else{
			$Mysqli_Conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
			if(!$Mysqli_Conn->connect_error){
				$Sql = 	"UPDATE ORIENTACION " .
						"SET Grupo1 = ?, " .
						"Grupo2 = ?, " .
						"Grupo3 = ? " .
						"WHERE IdOrientacion = ?";

				$Query = $Mysqli_Conn->prepare($Sql);
				$Query->bind_param('iiii', $Grupo1, $Grupo2, $Grupo3, $IdOrientacion);
				if($Query->execute()){
					$CodigoRespuesta = 1;
				}else{
					$MensajeRespuesta = 'Error actualizando grupos de orientaci&oacute;n';
				}
			}
		}
	}
	if(	($MensajeRespuesta=='')	&&	($CodigoRespuesta!=1)	){
		$MensajeRespuesta = 'Error actualizando grupos para orientaci&oacute;n';
	}
	$Respuesta = array(
		"Codigo" => $CodigoRespuesta,
		"Mensaje" => $MensajeRespuesta
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$Respuesta));
}

//WO_ObtenerGruposParaOrientacion('token');
function WO_ObtenerGruposParaOrientacion($Token){
	$CodigoRespuesta = 0;
	$MensajeRespuesta = '';
	$ListaGrupos = array();
	global $configs;
	$IdAdmin = ValidarTokenAdmin($Token);
	//$IdAdmin = 'AUGUSTO90';
	if($IdAdmin!=''){
		$Mysqli_Conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$Mysqli_Conn->connect_error){
			$Sql = 	"SELECT	DISTINCT G.IdGrupo, G.Nombre " .
					"FROM	GRUPO G, CARRERA C, CARRERAXUNI CU " .
					"WHERE	G.IdGrupo = C.Grupo " .
					"AND	C.IdCarrera = CU.IdCarrera";
			$Query = $Mysqli_Conn->prepare($Sql);
			$Query->execute();
			$Resultado = $Query->get_result();
			if ($Resultado->num_rows > 0) {
				while($Grupo = $Resultado->fetch_assoc()) {
					$ListaGrupos[] = array(
						"Id" => $Grupo['IdGrupo'],
						"Nombre" => utf8_encode($Grupo['Nombre'])
					);
					$CodigoRespuesta = 1;
				}
			}else{
				$MensajeRespuesta = 'No existen grupos v&aacute;lidos para asignar a orientaci&oacute;n';
			}
		}
	}
	if(	($MensajeRespuesta=='')	&&	($CodigoRespuesta!=1)	){
		$MensajeRespuesta = 'Error obteniendo grupos para orientaci&oacute;n';
	}
	$Respuesta = array(
		"Codigo" => $CodigoRespuesta,
		"Mensaje" => $MensajeRespuesta,
		"Grupos" => $ListaGrupos
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$Respuesta));
}

/* ****** Operaciones Web ****** */
//WO_ListaOrientacionesDeUsuario('token');
function WO_ListaOrientacionesDeUsuario($Token){
	$CodigoRespuesta = 0;
	$MensajeRespuesta = '';
	$ListaOrientaciones = array();
	global $configs;
	$IdUsuario = ValidarToken($Token);
	//$IdUsuario = 'MARQUITOS';
	ActualizarTokenUsuario($IdUsuario);
	if( IdUsuario != '' ){
		$Mysqli_Conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$Mysqli_Conn->connect_error){
			$Sql = 	"SELECT IdOrientacion " .
					"FROM 	ORIENTACION " .
					"WHERE 	IdUsuario = ? ";
			$Query = $Mysqli_Conn->prepare($Sql);
			$Query->bind_param('s', $IdUsuario);
			$Query->execute();
			$Resultado = $Query->get_result();
			if ($Resultado->num_rows > 0) {
				while($Orientacion = $Resultado->fetch_assoc()) {
					$ListaOrientaciones[] = array(
						"Id" => $Orientacion['IdOrientacion']
					);
					$CodigoRespuesta = 1;
				}
			}else{
				$CodigoRespuesta = 2;
				$MensajeRespuesta = "No existen orientaciones para este usuario";
			}
		}
	}

	if(	($MensajeRespuesta=='')	&&	($CodigoRespuesta!=1)	){
		$MensajeRespuesta = 'Error obteniendo orientaciones de usuario';
	}
	$Respuesta = array(
		"Codigo" => $CodigoRespuesta,
		"Mensaje" => $MensajeRespuesta,
		"Orientaciones" => $ListaOrientaciones
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$Respuesta));
}


//WO_FinalizarExamenOrientacion('t0k3N','1','1');
function WO_FinalizarExamenOrientacion($TokenAdmin, $IdOrientacion, $NumeroExamen){
	$CodigoRespuesta = 0;
	$MensajeRespuesta = '';

	//if(1==1){
	if( ValidarTokenAdmin($TokenAdmin) != '' ){
		//$IdExamen, $NumEstado, $IdOrientacion
		$CodigoRespuesta = ActualizarEstadoExamen($NumeroExamen,2,$IdOrientacion);
	}
	if( ($CodigoRespuesta==0) && ($MensajeRespuesta=='') ){
		$MensajeRespuesta = 'Error finalizando ex&acute;men de orientaci&oacute;n';
	}

	$Respuesta = array(
		"Codigo" => $CodigoRespuesta,
		"Mensaje" => $MensajeRespuesta
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$Respuesta));
}

//WO_EnviarCorreoOrientacion('t0k3N','1','1');
function WO_EnviarCorreoOrientacion($TokenAdmin, $IdOrientacion, $NumeroExamen){
	$CodigoRespuesta = 0;
	$MensajeRespuesta = '';
	$EnlaceExamen = '';
	global $configs;
	if( ValidarTokenAdmin($TokenAdmin) != '' ){
		$Mysqli_Conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$Mysqli_Conn->connect_error){
			//Obtener correo usuario asociado a orientacion
			$Sql = 	"SELECT	U.IdUsuario, U.Correo " .
					"FROM	ORIENTACION O, USUARIO U " .
					"WHERE	O.IdUsuario = U.IdUsuario " .
					"AND	O.IdOrientacion = ? ";
			$Query = $Mysqli_Conn->prepare($Sql);
			$Query->bind_param('i',$IdOrientacion);
			$Query->execute();
			$Resultado = $Query->get_result();
			if($Resultado->num_rows==1){
				$Correo = $Resultado->fetch_assoc()['Correo'];
				if($Correo == ''){
					$MensajeRespuesta = 'El usuario no tiene correo electrónico configurado';
				}else{
					//Obtener enlace de examen de usuario
					$EnlaceExamen = ObtenerEnlaceExamen($NumeroExamen);
					if($EnlaceExamen==''){
						$MensajeRespuesta = 'Error obteniendo enlace de ex&aacute;men';
					}
				}
				if($MensajeRespuesta==''){
					// EnviarCorreoExamenOrientacion($CORREO_USUARIO, $NUM_EXAMEN, $ENLACE, $ID_ORIENTACION)
					$CodigoRespuesta = EnviarCorreoExamenOrientacion($Correo, $NumeroExamen, $EnlaceExamen, $IdOrientacion);
					//Colocar 1 para pruebas en maquina local
					$CodigoRespuesta = 1;
					if($CodigoRespuesta != 1){
						$MensajeRespuesta = 'Error enviando correo electr&oacute;nico';
					}
				}

				if($MensajeRespuesta==''){
					//Actualizar estado de examen de orientación
					//$IdExamen, $NumEstado, $IdOrientacion
					//echo $GLOBALS['ESTADO_CORREO_ENVIADO'];
					$CodigoRespuesta = ActualizarEstadoExamen($NumeroExamen, 1, $IdOrientacion);
					if($CodigoRespuesta!=1){
						$MensajeRespuesta = 'Error actualizando estado de ex&aacute;men';
					}
				}

			}else{
				$MensajeRespuesta = 'Error obteniendo correo de usuario';
			}
			$Mysqli_Conn->close();
		}

	}
	if(	($MensajeRespuesta=='')	&&	($CodigoRespuesta!=1)	){
		$MensajeRespuesta = 'Error enviando correo de enlace de exámen';
	}
	$Respuesta = array(
		"Codigo" => $CodigoRespuesta,
		"Mensaje" => $MensajeRespuesta
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$Respuesta));
}

function WO_DetalleOrientacionUsuario($TOKEN, $ID_ORIENTACION){
	$CODIGO = 0;
	$ORIENTACION = '';
	$MENSAJE = '';
	global $configs;
	$ID_USUARIO = ValidarToken($TOKEN);
	ActualizarTokenUsuario($ID_USUARIO);
	if($ID_USUARIO!=''){
		$ORIENTACION = ObtenerDetalleOrientacion($ID_ORIENTACION);
		if(count($ORIENTACION)>0){
			$CODIGO = 1;
		}
	}else{
		$MENSAJE = 'Error autenticando usuario administrador';
	}

	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"orientacion" => $ORIENTACION,
		"mensaje" => $MENSAJE
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));

}


function WO_DetalleOrientacionAdmin($TOKEN, $ID_ORIENTACION){
	$CODIGO = 0;
	$ORIENTACION = '';
	$MENSAJE = '';
	global $configs;
	$ID_ADMIN = ValidarTokenAdmin($TOKEN);
	$ID_ADMIN = 'JOSADMIN';
	if($ID_ADMIN!=''){
		$ORIENTACION = ObtenerDetalleOrientacion($ID_ORIENTACION);
		if(count($ORIENTACION)>0){
			$CODIGO = 1;
		}else{
			$MENSAJE = 'Error obteniendo detalles de orientaci&oacute;n';
		}
	}else{
		$MENSAJE = 'Error autenticando usuario administrador';
	}

	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"orientacion" => $ORIENTACION,
		"mensaje" => $MENSAJE
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));

}


function WO_ListaOrientaciones($TOKEN, $ID_USUARIO){
	$CODIGO = 0;
	$ORIENTACIONES = array();
	global $configs;
	$ID_ADMIN = ValidarTokenAdmin($TOKEN);
	$ID_ADMIN = 'JOSADMIN';
	if($ID_ADMIN!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql = "SELECT IdOrientacion, IdUsuario, Test1, Test2, Test3, Estado " .
				"FROM 	ORIENTACION " .
				"WHERE 	IdUsuario = ?";
			$query = $conn->prepare($sql);
			$query->bind_param('s', $ID_USUARIO);
			$query->execute();
			$resultado = $query->get_result();
			if ($resultado->num_rows > 0) {
				while($orientacion = $resultado->fetch_assoc()) {
					$ORIENTACIONES[] = array(
						"Id" => $orientacion['IdOrientacion'],
						"EstadoExamen1" => intval($orientacion['Test1']),
						"EstadoExamen2" => intval($orientacion['Test2']),
						"EstadoExamen3" => intval($orientacion['Test3']),
						"DescExamen1" => DescripcionEstadoExamen(intval($orientacion['Test1'])),
						"DescExamen2" => DescripcionEstadoExamen(intval($orientacion['Test2'])),
						"DescExamen3" => DescripcionEstadoExamen(intval($orientacion['Test3'])),
						"ClaseCss1" => ClaseCssEstadoExamen(intval($orientacion['Test1'])),
						"ClaseCss2" => ClaseCssEstadoExamen(intval($orientacion['Test2'])),
						"ClaseCss3" => ClaseCssEstadoExamen(intval($orientacion['Test3'])),
						"Estado" => intval($orientacion['Estado'])
					);
				}
			}
			$CODIGO = 1;
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"orientaciones" => $ORIENTACIONES
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


/* ****** Operaciones adicionales ****** */

function ObtenerDetalleOrientacion($ID_ORIENTACION){
	global $configs;
	$ORIENTACION = array();
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$conn->connect_error){
		$sql = 	"SELECT 	O.IdUsuario, O.Test1, O.Test2, O.Test3, IFNULL(O.Grupo1,0) AS Grupo1, IFNULL(O.Grupo2,0) AS Grupo2, IFNULL(O.Grupo3,0) AS Grupo3, " .
				"IFNULL(G1.Nombre,'N/D') AS NomGrupo1, IFNULL(G2.Nombre,'N/D') AS NomGrupo2, IFNULL(G3.Nombre,'N/D') AS NomGrupo3 " .
				"FROM	ORIENTACION O " .
				"LEFT	OUTER JOIN GRUPO G1 " .
				"ON		G1.IdGrupo = O.Grupo1 " .
				"LEFT 	OUTER JOIN GRUPO G2 " .
				"ON		G2.IdGrupo = O.Grupo2 " .
				"LEFT 	OUTER JOIN GRUPO G3 " .
				"ON		G3.IdGrupo = O.Grupo3 " .
				"WHERE	IdOrientacion = ? ";

		$query = $conn->prepare($sql);
		$query->bind_param('i',$ID_ORIENTACION);
		$query->execute();
		$resultado = $query->get_result();
		if($resultado->num_rows==1){
			$orientacion = $resultado->fetch_assoc();
			$ORIENTACION = array(
				"IdUsuario" => utf8_encode($orientacion['IdUsuario']),
				"EstadoExamen1" => intval($orientacion['Test1']),
				"EstadoExamen2" => intval($orientacion['Test2']),
				"EstadoExamen3" => intval($orientacion['Test3']),
				"DescExamen1" => DescripcionEstadoExamen(intval($orientacion['Test1'])),
				"DescExamen2" => DescripcionEstadoExamen(intval($orientacion['Test2'])),
				"DescExamen3" => DescripcionEstadoExamen(intval($orientacion['Test3'])),
				"ClaseCss1" => ClaseCssEstadoExamen(intval($orientacion['Test1'])),
				"ClaseCss2" => ClaseCssEstadoExamen(intval($orientacion['Test2'])),
				"ClaseCss3" => ClaseCssEstadoExamen(intval($orientacion['Test3'])),
				"Grupo1" => intval($orientacion['Grupo1']),
				"Grupo2" => intval($orientacion['Grupo2']),
				"Grupo3" => intval($orientacion['Grupo3']),
				"NomGrupo1" => utf8_encode($orientacion['NomGrupo1']),
				"NomGrupo2" => utf8_encode($orientacion['NomGrupo2']),
				"NomGrupo3" => utf8_encode($orientacion['NomGrupo3'])
			);
		}
		$conn->close();
	}
	return $ORIENTACION;
}


function ObtenerEnlaceExamen($strNumExamen){
	$ENLACE = '';
	global $configs;
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$conn->connect_error){

		$sql = "SELECT 	Valor " .
			   "FROM   	CONFIGURACION " .
			   "WHERE 	Llave = ? ";
		$query = $conn->prepare($sql);
		$nom_llave = 'Examen' . $strNumExamen;
		$query->bind_param('s',$nom_llave);
		$query->execute();
		$resultado = $query->get_result();
		if($resultado->num_rows==1){
			$ENLACE = $resultado->fetch_assoc()['Valor'];
		}
	}
	return $ENLACE;
}


function ActualizarEstadoExamen($IdExamen, $NumEstado, $IdOrientacion){
	$CodigoRespuesta = 0;
	global $configs;

	$Mysqli_Conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$Mysqli_Conn->connect_error){
		$Sql = 	"UPDATE ORIENTACION " .
				"SET 	Test" . $IdExamen . " = ? " .
				"WHERE 	IdOrientacion = ? ";

		$Query = $Mysqli_Conn->prepare($Sql);
		$Query->bind_param('ii', $NumEstado, $IdOrientacion);

		if($Query->execute()){
			$CodigoRespuesta = 1;
		}
	}

	return $CodigoRespuesta;
}

function ClaseCssEstadoExamen($Estado){
	$ClaseCss = '';
	if($Estado==0){
		$ClaseCss = 'alert-danger';
	}else if($Estado==1){
		$ClaseCss = 'alert-warning';
	}else if($Estado==2){
		$ClaseCss = 'alert-success';
	}
	return $ClaseCss;
}

function DescripcionEstadoExamen($Estado){
	$Descripcion = '';
	if($Estado==0){
		$Descripcion = 'Pendiente';
	}else if($Estado==1){
		$Descripcion = 'Enviado';
	}else if($Estado==2){
		$Descripcion = 'Finalizado';
	}
	return $Descripcion;
}




?>
