<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization");

$configs = include('config.php');
include('bitacora.php');
include('correo.php');
include('token.php');
include(__DIR__ . '/error-manager/error_manager.php');
include(__DIR__ . '/database/DatabaseManager.php');

$postdata = file_get_contents("php://input");

if(isset($postdata)) {
	$request = json_decode($postdata);
	if($request->Operacion == "lista"){
		WO_ListaCarreras();
	}else if($request->Operacion == "detalle"){
		WO_DetalleCarrera($request->IdCarrera, $request->Param1);
	}else if($request->Operacion == "agregar"){
		WO_AgregarCarrera($request->Carrera, $request->Param1);
	}else if($request->Operacion == "modificar"){
		WO_ModificarCarrera($request->Carrera, $request->Param1);
	}else if($request->Operacion == "eliminar"){
		WO_EliminarCarrera($request->IdCarrera, $request->Param1);
	}else if($request->Operacion == "grupos"){
		WO_ListaGrupos();
	}
}

/* ****** Operaciones Web ****** */
//WO_DetalleCarrera("24","token");
function WO_DetalleCarrera($ID_CARRERA, $TOKEN){
	global $configs;
	$CARRERA = "";
	$CODIGO = 0;
	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql = 	"SELECT IdCarrera, Nombre,  Grupo, Descripcion, DescripcionInterna, NombreFacultad,  TelefonoFacultad,  Modalidad,  " .
					"CostoMensual,  CostoMatricula,  CostoInscripcion,  AplicaBeca,  DetallesBeca,  " .
					"ReqTesis,  ReqPrivado,  ReqMaestria,  ReqCreditos,  ReqEps,  DetallesCierre,  " .
					"HIniMat,  HFinMat,  HIniVesp,  HFinVesp,  HIniNoc,  HFinNoc,  HIniSab,  HFinSab,  HIniDom,  HFinDom,  " .
					"DetallesNuevoIngreso,  DetallesReingreso,  DetallesContinuacion,  DetallesTraslado,  DetallesExtranjero, DetallesPago, DetallesPromociones, " .
					"CorreoFacultad, Certi_Internacional, DescripcionHorarios, " . //marodriguez 20180422
					"ModalidadPresencial, ModalidadSemiPresencial, ModalidadEnLinea, DescripcionModalidad " . //marodriguez 20180422
					"FROM 	CARRERA " .
					"WHERE	IdCarrera = $ID_CARRERA";
			//echo $sql;
			$result = $conn->query($sql);
			if ($result->num_rows == 1) {
				$carrera = $result->fetch_assoc();

				$CARRERA = array(
					"IdCarrera" => $carrera["IdCarrera"],
					"Nombre" => utf8_encode($carrera["Nombre"]),
					"Grupo" => intval($carrera["Grupo"]),
					"Descripcion" => utf8_encode($carrera["Descripcion"]),
					"DescripcionInterna" => utf8_encode($carrera["DescripcionInterna"]),
					"Modalidad" => utf8_encode($carrera["Modalidad"]),
					"CostoMensual" => floatval($carrera["CostoMensual"]),
					"CostoMatricula" => floatval($carrera["CostoMatricula"]),
					"CostoInscripcion" => floatval($carrera["CostoInscripcion"]),
					"TelefonoFacultad" => utf8_encode($carrera["TelefonoFacultad"]),
					"NombreFacultad" => utf8_encode($carrera["NombreFacultad"]),
					"AplicaBeca" => intval($carrera["AplicaBeca"]),
					"DetallesBeca" => utf8_encode($carrera["DetallesBeca"]),
					"ReqTesis" => intval($carrera["ReqTesis"]),
					"ReqPrivado" => intval($carrera["ReqPrivado"]),
					"ReqMaestria" => intval($carrera["ReqMaestria"]),
					"ReqCreditos" => intval($carrera["ReqCreditos"]),
					"ReqEps" => intval($carrera["ReqEps"]),
					"DetallesCierre" => utf8_encode($carrera["DetallesCierre"]),
					"HIniMat" => $carrera["HIniMat"],
					"HFinMat" => $carrera["HFinMat"],
					"HIniVesp" => $carrera["HIniVesp"],
					"HFinVesp" => $carrera["HFinVesp"],
					"HIniNoc" => $carrera["HIniNoc"],
					"HFinNoc" => $carrera["HFinNoc"],
					"HIniSab" => $carrera["HIniSab"],
					"HFinSab" => $carrera["HFinSab"],
					"HIniDom" => $carrera["HIniDom"],
					"HFinDom" => $carrera["HFinDom"],
					"DetallesNuevoIngreso"=> utf8_encode($carrera["DetallesNuevoIngreso"]),
					"DetallesReingreso"=> utf8_encode($carrera["DetallesReingreso"]),
					"DetallesContinuacion"=> utf8_encode($carrera["DetallesContinuacion"]),
					"DetallesTraslado"=> utf8_encode($carrera["DetallesTraslado"]),
					"DetallesExtranjero"=> utf8_encode($carrera["DetallesExtranjero"]),
					"DetallesPago"=> utf8_encode($carrera["DetallesPago"]),
					"DetallesPromociones"=> utf8_encode($carrera["DetallesPromociones"]),
					"CorreoFacultad" => utf8_encode($carrera["CorreoFacultad"]),
					"Certi_Internacional" => intval($carrera["Certi_Internacional"]), //marodriguez 20180422
					"DescripcionHorarios" => utf8_encode($carrera["DescripcionHorarios"]), //marodriguez 20180422
					"ModalidadPresencial" => intval($carrera["ModalidadPresencial"]), //marodriguez 20180422
					"ModalidadSemiPresencial" => intval($carrera["ModalidadSemiPresencial"]), //marodriguez 20180422
					"ModalidadEnLinea" => intval($carrera["ModalidadEnLinea"]), //marodriguez 20180422
					"DescripcionModalidad" => utf8_encode($carrera["DescripcionModalidad"]) //marodriguez 20180422
				);
				$CODIGO = 1;
			}
			$conn->close();
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"Carrera" => $CARRERA
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_ListaGrupos(){
	global $configs;
	$grupos = array();
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);

	if(!$conn->connect_error){
		$sql = 	"SELECT 	IdGrupo, Nombre " .
				"FROM 		GRUPO ";

		$result = $conn->query($sql);

		if ($result->num_rows > 0) {

			while($grupo = $result->fetch_assoc()) {
				$grupos[] = array(
					"Id" => $grupo['IdGrupo'],
					"Nombre" => utf8_encode($grupo['Nombre'])
				);
			}

		}

		$conn->close();

	}

	header('Content-type: application/json');
	echo json_encode(array('grupos'=>$grupos));

}


function WO_ModificarCarrera($CARRERA, $TOKEN){
	$CODIGO = 0;
	global $configs;
	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){

			$ID_CARRERA = $CARRERA->IdCarrera;
			$NOMBRE = $CARRERA->Nombre;
			$GRUPO = $CARRERA->Grupo;
			$DESCRIPCION = $CARRERA->Descripcion;
			$DESCRIPCION_INTERNA = $CARRERA->DescripcionInterna;
			$NOMBRE_FACULTAD = $CARRERA->NombreFacultad;
			$TELEFONO_FACULTAD = $CARRERA->TelefonoFacultad;
			$MODALIDAD = $CARRERA->Modalidad;
			$COSTO_MENSUAL = $CARRERA->CostoMensual;
			$COSTO_MATRICULA = $CARRERA->CostoMatricula;
			$COSTO_INSCRIPCION = $CARRERA->CostoInscripcion;
			$APLICA_BECA = $CARRERA->AplicaBeca;
			$DETALLES_BECA = $CARRERA->DetallesBeca;
			$REQ_TESIS = $CARRERA->ReqTesis;
			$REQ_PRIVADO = $CARRERA->ReqPrivado;
			$REQ_MAESTRIA = $CARRERA->ReqMaestria;
			$REQ_CREDITOS = $CARRERA->ReqCreditos;
			$REQ_EPS = $CARRERA->ReqEps;
			$DETALLES_CIERRE = $CARRERA->DetallesCierre;
			$HINI_MAT = $CARRERA->HIniMat;
			$HFIN_MAT	 = $CARRERA->HFinMat;
			$HINI_VESP = $CARRERA->HIniVesp;
			$HFIN_VESP = $CARRERA->HFinVesp;
			$HINI_NOC = $CARRERA->HIniNoc;
			$HFIN_NOC = $CARRERA->HFinNoc;
			$HINI_SAB = $CARRERA->HIniSab;
			$HFIN_SAB = $CARRERA->HFinSab;
			$HINI_DOM = $CARRERA->HIniDom;
			$HFIN_DOM = $CARRERA->HFinDom;
			$DETALLES_NUEVO_INGRESO = $CARRERA->DetallesNuevoIngreso;
			$DETALLES_REINGRESO = $CARRERA->DetallesReingreso;
			$DETALLES_CONTINUACION = $CARRERA->DetallesContinuacion;
			$DETALLES_TRASLADO = $CARRERA->DetallesTraslado;
			$DETALLES_EXTRANJERO = $CARRERA->DetallesExtranjero;
			$DETALLES_PAGO = $CARRERA->DetallesPago;
			$DETALLES_PROMOCIONES = $CARRERA->DetallesPromociones;
			$CORREO_FACULTAD = $CARRERA->CorreoFacultad; //marodriguez 20180422
			$CERTI_INTERNACIONAL = $CARRERA->Certi_Internacional;  //marodriguez 20180422
			$DESCRIPCION_HORARIOS = $CARRERA->DescripcionHorarios;  //marodriguez 20180422
			$MOD_PRESENCIAL = $CARRERA->ModalidadPresencial; //marodriguez 20180422
			$MOD_SEMIPRESENCIAL = $CARRERA->ModalidadSemiPresencial; //marodriguez 20180422
			$MOD_ENLINEA = $CARRERA->ModalidadEnLinea; //marodriguez 20180422
			$DESC_MODALIDAD = $CARRERA->DescripcionModalidad; //marodriguez 20180422

			$sql =  "UPDATE	CARRERA " .
					"SET 	Nombre='$NOMBRE',  Grupo=$GRUPO, Descripcion='$DESCRIPCION', DescripcionInterna='$DESCRIPCION_INTERNA', NombreFacultad='$NOMBRE_FACULTAD',  TelefonoFacultad = '$TELEFONO_FACULTAD',  Modalidad='$MODALIDAD',  " .
					"CostoMensual=$COSTO_MENSUAL,  CostoMatricula=$COSTO_MATRICULA,  CostoInscripcion=$COSTO_INSCRIPCION,  AplicaBeca=$APLICA_BECA,  DetallesBeca='$DETALLES_BECA',  " .
					"ReqTesis=$REQ_TESIS ,  ReqPrivado=$REQ_PRIVADO,  ReqMaestria=$REQ_MAESTRIA,  ReqCreditos=$REQ_CREDITOS,  ReqEps=$REQ_EPS,  DetallesCierre='$DETALLES_CIERRE',  " .
					"HIniMat='$HINI_MAT',  HFinMat='$HFIN_MAT',  HIniVesp='$HINI_VESP',  HFinVesp='$HFIN_VESP',  HIniNoc='$HINI_NOC',  HFinNoc='$HFIN_NOC',  HIniSab='$HINI_SAB',  HFinSab='$HFIN_SAB',  HIniDom='$HINI_DOM',  HFinDom='$HFIN_DOM',  " .
					"DetallesNuevoIngreso='$DETALLES_NUEVO_INGRESO',  DetallesReingreso='$DETALLES_REINGRESO',  DetallesContinuacion='$DETALLES_CONTINUACION',  DetallesTraslado='$DETALLES_TRASLADO',  DetallesExtranjero='$DETALLES_EXTRANJERO', " .
					"DetallesPago = '$DETALLES_PAGO', DetallesPromociones = '$DETALLES_PROMOCIONES', UsuarioModifica = '$ID_USUARIO', FechaModifica = NOW(),  " .
					"CorreoFacultad = '$CORREO_FACULTAD', Certi_Internacional =  $CERTI_INTERNACIONAL, DescripcionHorarios =  '$DESCRIPCION_HORARIOS', " .  //marodriguez 20180422
					"ModalidadPresencial = $MOD_PRESENCIAL, ModalidadSemiPresencial = $MOD_SEMIPRESENCIAL, ModalidadEnLinea = $MOD_ENLINEA, " . //marodriguez 20180422
					"DescripcionModalidad = '$DESC_MODALIDAD' " . //marodriguez 20180422
					"WHERE IdCarrera = $ID_CARRERA";

			$conn->set_charset("utf8");
			if (mysqli_query($conn, $sql)) {
				$CODIGO = 1;
			}else{
				$CODIGO = $conn->errno;
			}
			$conn->close();

		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_AgregarCarrera($CARRERA, $TOKEN){
	$CODIGO = 0;
	global $configs;
	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){

			$NOMBRE = $CARRERA->Nombre;
			$GRUPO = $CARRERA->Grupo;
			$DESCRIPCION = $CARRERA->Descripcion;
			$DESCRIPCION_INTERNA = $CARRERA->DescripcionInterna;
			$NOMBRE_FACULTAD = $CARRERA->NombreFacultad;
			$TELEFONO_FACULTAD = $CARRERA->TelefonoFacultad;
			$MODALIDAD = $CARRERA->Modalidad;
			$COSTO_MENSUAL = $CARRERA->CostoMensual;
			$COSTO_MATRICULA = $CARRERA->CostoMatricula;
			$COSTO_INSCRIPCION = $CARRERA->CostoInscripcion;
			$APLICA_BECA = $CARRERA->AplicaBeca;
			$DETALLES_BECA = $CARRERA->DetallesBeca;
			$REQ_TESIS = $CARRERA->ReqTesis;
			$REQ_PRIVADO = $CARRERA->ReqPrivado;
			$REQ_MAESTRIA = $CARRERA->ReqMaestria;
			$REQ_CREDITOS = $CARRERA->ReqCreditos;
			$REQ_EPS = $CARRERA->ReqEps;
			$DETALLES_CIERRE = $CARRERA->DetallesCierre;
			$HINI_MAT = $CARRERA->HIniMat;
			$HFIN_MAT	 = $CARRERA->HFinMat;
			$HINI_VESP = $CARRERA->HIniVesp;
			$HFIN_VESP = $CARRERA->HFinVesp;
			$HINI_NOC = $CARRERA->HIniNoc;
			$HFIN_NOC = $CARRERA->HFinNoc;
			$HINI_SAB = $CARRERA->HIniSab;
			$HFIN_SAB = $CARRERA->HFinSab;
			$HINI_DOM = $CARRERA->HIniDom;
			$HFIN_DOM = $CARRERA->HFinDom;
			$DETALLES_NUEVO_INGRESO = $CARRERA->DetallesNuevoIngreso;
			$DETALLES_REINGRESO = $CARRERA->DetallesReingreso;
			$DETALLES_CONTINUACION = $CARRERA->DetallesContinuacion;
			$DETALLES_TRASLADO = $CARRERA->DetallesTraslado;
			$DETALLES_EXTRANJERO = $CARRERA->DetallesExtranjero;
			$DETALLES_PAGO = $CARRERA->DetallesPago;
			$DETALLES_PROMOCIONES = $CARRERA->DetallesPromociones;
			$CORREO_FACULTAD = $CARRERA->CorreoFacultad; //marodriguez 20180422
			$CERTI_INTERNACIONAL = $CARRERA->Certi_Internacional;  //marodriguez 20180422
			$DESCRIPCION_HORARIOS = $CARRERA->DescripcionHorarios;  //marodriguez 20180422
			$MOD_PRESENCIAL = $CARRERA->ModalidadPresencial; //marodriguez 20180422
			$MOD_SEMIPRESENCIAL = $CARRERA->ModalidadSemiPresencial; //marodriguez 20180422
			$MOD_ENLINEA = $CARRERA->ModalidadEnLinea; //marodriguez 20180422
			$DESC_MODALIDAD = $CARRERA->DescripcionModalidad; //marodriguez 20180422

			$sql =  "INSERT INTO CARRERA ( " .
					"Nombre,  Grupo, Descripcion, DescripcionInterna, NombreFacultad,  TelefonoFacultad,  Modalidad,  " .
					"CostoMensual,  CostoMatricula,  CostoInscripcion,  AplicaBeca,  DetallesBeca,  " .
					"ReqTesis,  ReqPrivado,  ReqMaestria,  ReqCreditos,  ReqEps,  DetallesCierre,  " .
					"HIniMat,  HFinMat,  HIniVesp,  HFinVesp,  HIniNoc,  HFinNoc,  HIniSab,  HFinSab,  HIniDom,  HFinDom,  " .
					"DetallesNuevoIngreso,  DetallesReingreso,  DetallesContinuacion,  DetallesTraslado,  DetallesExtranjero, DetallesPago, DetallesPromociones, " .
					"UsuarioModifica, FechaModifica, CorreoFacultad, Certi_Internacional, DescripcionHorarios, " .
					"ModalidadPresencial, ModalidadSemiPresencial, ModalidadEnLinea, DescripcionModalidad) " .
					"VALUES ('$NOMBRE','$GRUPO','$DESCRIPCION', '$DESCRIPCION_INTERNA', '$NOMBRE_FACULTAD', '$TELEFONO_FACULTAD', '$MODALIDAD',  " .
					"$COSTO_MENSUAL, $COSTO_MATRICULA,$COSTO_INSCRIPCION,  " .
					"$APLICA_BECA, '$DETALLES_BECA', $REQ_TESIS, $REQ_PRIVADO, $REQ_MAESTRIA, $REQ_CREDITOS, $REQ_EPS, '$DETALLES_CIERRE',  " .
					"'$HINI_MAT', '$HFIN_MAT', '$HINI_VESP',  '$HFIN_VESP', '$HINI_NOC', '$HFIN_NOC', '$HINI_SAB', '$HFIN_SAB', '$HINI_DOM','$HFIN_DOM',  " .
					"'$DETALLES_NUEVO_INGRESO', '$DETALLES_REINGRESO', '$DETALLES_CONTINUACION', '$DETALLES_TRASLADO', '$DETALLES_EXTRANJERO', " .
					"'$DETALLES_PAGO', '$DETALLES_PROMOCIONES', '$ID_USUARIO', NOW(), '$CORREO_FACULTAD', $CERTI_INTERNACIONAL, '$DESCRIPCION_HORARIOS', " . //marodriguez 20180422
					"$MOD_PRESENCIAL, $MOD_SEMIPRESENCIAL, $MOD_ENLINEA, '$DESC_MODALIDAD' " . //marodriguez 20180422
					")";

			$conn->set_charset("utf8");

			if (mysqli_query($conn, $sql)) {
				$CODIGO = 1;
			}else{
				$CODIGO = $conn->errno;
			}

			$conn->close();

		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_EliminarCarrera($ID_CARRERA, $TOKEN){
	$CODIGO = 0;
	global $configs;
	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql =  "DELETE FROM CARRERA WHERE IdCarrera = $ID_CARRERA	";

			if (mysqli_query($conn, $sql)) {
				$CODIGO = 1;
			}else{
				$CODIGO = $conn->errno;
			}
			$conn->close();
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_ListaCarreras(){
	global $configs;
	$carreras = array();
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);

	if(!$conn->connect_error){
		$sql = 	"SELECT		C.IdCarrera, C.Nombre AS NombreCarrera, TG.Descripcion as NombreTipoGrupo, G.Nombre as NombreGrupo, C.DescripcionInterna AS Descripcion " .
				"FROM 		CARRERA C, GRUPO G, TIPO_GRUPO TG " .
				"WHERE 		C.Grupo = G.IdGrupo " .
				"AND 		G.TipoGrupo = TG.IdTipoGrupo";

		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			while($carrera = $result->fetch_assoc()) {
				$carreras[] = array(
					"Id" => $carrera['IdCarrera'],
					"Nombre" => utf8_encode($carrera['NombreCarrera']),
					"Tipo" => utf8_encode($carrera['NombreTipoGrupo']),
					"Grupo" => utf8_encode($carrera['NombreGrupo']),
					"Descripcion" => utf8_encode($carrera['Descripcion'])
				);
			}
		}
		$conn->close();

	}

	header('Content-type: application/json');
	echo json_encode(array('carreras'=>$carreras));

}
