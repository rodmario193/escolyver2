<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization");

$configs = include('config.php');
include('bitacora.php');
include('token.php');
include('correo.php');
include(__DIR__ . '/error-manager/error_manager.php');
include(__DIR__ . '/database/DatabaseManager.php');

$postdata = file_get_contents("php://input");

if(isset($postdata)) {
	$request = json_decode($postdata);
	if($request->Operacion == "carreras"){
		WO_ObtenerCarreras($request->IdUniversidad);
	}else if($request->Operacion == "carrerasnoasig"){
		WO_ObtenerCarrerasSinAsignar($request->IdUniversidad);
	}else if($request->Operacion == "asigcarreras"){
		WO_AsignarCarreras($request->IdUniversidad, $request->Carreras, $request->Param1);
	}else if($request->Operacion == "elimcarrera"){
		WO_EliminarCarreraDeUniversidad($request->IdUniversidad, $request->IdCarrera, $request->Param1);
	}
}

/* ****** Operaciones Web ****** */

function WO_EliminarCarreraDeUniversidad($ID_UNIVERSIDAD, $ID_CARRERA, $TOKEN){
	$CODIGO = 0;
	global $configs;

	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql =  "DELETE FROM CARRERAXUNI WHERE IdUniversidad = $ID_UNIVERSIDAD AND IdCarrera = $ID_CARRERA";

			if (mysqli_query($conn, $sql)) {
				$CODIGO = 1;
			}else{
				$CODIGO = $conn->errno;
			}
			$conn->close();
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_AsignarCarreras($ID_UNIVERSIDAD, $CARRERAS, $TOKEN){
	global $configs;
	$cursos = array();
	$CODIGO = 0;

	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$CONTADOR = 0;
			for($I = 0; $I < sizeof($CARRERAS); $I++){
				$CARRERA = $CARRERAS[$I];
				IF($CARRERA->Seleccionada == 1){
					$ID_CARRERA = $CARRERA->Id;
					$sql =  "INSERT INTO CARRERAXUNI(IdUniversidad, IdCarrera) VALUES($ID_UNIVERSIDAD,$ID_CARRERA)";

					if (mysqli_query($conn, $sql)) {
						$CODIGO = 1;
						//Enviar correo de notificacion para nueva carrera agregada
						EnviarCorreosNotificacion($ID_CARRERA);

					}else{
						$CODIGO = $conn->errno;
						break;
					}
				}
			}
			$conn->close();
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_ObtenerCarrerasSinAsignar($Id_Universidad){
	global $configs;
	$carreras = array();
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$conn->connect_error){
		// marodriguez 20180701 Se quita restricción de que solo se tomen carreras no asignadas a otras universidades
		$sql = 	"SELECT	C.IdCarrera, C.Nombre, G.Nombre AS Agrupacion, C.DescripcionInterna AS Descripcion " .
		"FROM 	CARRERA C, GRUPO G " .
		"WHERE 	C.Grupo = G.IdGrupo " .
		"AND	NOT EXISTS ( " .
		"	SELECT	1 " .
		"	FROM 	CARRERAXUNI CXU " .
		"	WHERE	CXU.IdCarrera = C.IdCarrera " .
		" 	AND 	CXU.IdUniversidad = " . $Id_Universidad .
		")";
		//NLog("Carrera no asignadas => " . $sql);
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {

			while($carrera = $result->fetch_assoc()) {
				$carreras[] = array(
					"Id" => $carrera['IdCarrera'],
					"Nombre" => utf8_encode($carrera['Nombre']),
					"Agrupacion" => utf8_encode($carrera['Agrupacion']),
					"Descripcion" => utf8_encode($carrera['Descripcion']),
					"Seleccionada" => 0
				);
			}

		}
		$conn->close();
	}

	header('Content-type: application/json');
	echo json_encode(array('carreras'=>$carreras));

}


function WO_ObtenerCarreras($ID_UNI){
	global $configs;
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);

	$carreras = array();

	if(!$conn->connect_error){

		$query = $conn->prepare("SELECT C.IdCarrera, C.Nombre, G.Nombre AS Agrupacion, C.Descripcion " .
			"FROM 	CARRERA C, GRUPO G, CARRERAXUNI CXU " .
			"WHERE 	C.IdCarrera = CXU.IdCarrera " .
			"AND 	C.Grupo = G.IdGrupo " .
			"AND	CXU.IdUniversidad = ? ");
		$query->bind_param('i',$ID_UNI);
		$query->execute();

		$result = $query->get_result();

		if ($result->num_rows > 0) {
			while($carrera = $result->fetch_assoc()) {
				$carreras[] = array(
					"Id" => $carrera['IdCarrera'],
					"Nombre" => utf8_encode($carrera['Nombre']),
					"Agrupacion" => utf8_encode($carrera['Agrupacion']),
					"Descripcion" => utf8_encode($carrera['Descripcion'])
				);
			}
		}

		$conn->close();
	}

	header('Content-type: application/json');
	echo json_encode(array('carreras'=>$carreras));
}


/* ****** Operaciones adicionales ****** */


function EnviarCorreosNotificacion($ID_CARRERA){
	global $configs;
	$CARRERA = ObtenerDatosCarrera($ID_CARRERA);
	$ID_GRUPO = $CARRERA["IdGrupo"];
	$NOMBRE = $CARRERA["Nombre"];
	$UNIVERSIDAD = $CARRERA["Universidad"];
	$DEPARTAMENTO = $CARRERA["Departamento"];
	$MUNICIPIO = $CARRERA["Municipio"];
	$GRUPO = $CARRERA["Grupo"];
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$conn->connect_error){
		$sql = 	"SELECT 	U.Correo, U.IdUsuario, B.ID_GRUPO " .
				"FROM	USUARIO U, BUSQUEDA B, GRUPO G " .
				"WHERE	U.IdUsuario = B.ID_USUARIO " .
				"AND		B.ID_GRUPO = G.IdGrupo " .
				"AND		B.ID_GRUPO = ? " .
				"AND		B.ESTADO = 0";

		$query = $conn->prepare($sql);
		$query->bind_param('i', $ID_GRUPO);
		$query->execute();
		$resultado = $query->get_result();
		if($resultado->num_rows > 0){
			while($correo = $resultado->fetch_assoc()){
				EnviarCorreoNuevaCarrera($correo["Correo"], $NOMBRE, $UNIVERSIDAD, $DEPARTAMENTO, $MUNICIPIO, $GRUPO);
			}
		}
	}
}


function ObtenerDatosCarrera($ID_CARRERA){
	global $configs;
	$CARRERA = array();
	echo $HOST;
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$conn->connect_error){
		$sql = 	"SELECT	C.Nombre AS Nombre, U.Nombre AS Universidad, DEP.Nombre AS Departamento, MUNI.Descripcion AS Municipio, " .
				"G.Nombre AS Grupo, G.IdGrupo AS IdGrupo " .
				"FROM 	CARRERA C, UNIVERSIDAD U, CARRERAXUNI CU, DEPARTAMENTO DEP, MUNICIPIO MUNI, GRUPO G " .
				"WHERE	CU.IdCarrera = C.IdCarrera " .
				"AND		CU.IdUniversidad = U.IdUniversidad " .
				"AND		U.Departamento = DEP.IdDepartamento " .
				"AND		U.Municipio = MUNI.IdMunicipio " .
				"AND		DEP.IdDepartamento = MUNI.IdDepartamento " .
				"AND		G.IdGrupo = C.Grupo " .
				"AND		C.IdCarrera = ?";
		$query = $conn->prepare($sql);
		$query->bind_param('i',$ID_CARRERA);
		$query->execute();
		$resultado = $query->get_result();
		if($resultado->num_rows==1){
			$carrera = $resultado->fetch_assoc();
			$CARRERA = array(
				"Nombre" => utf8_encode($carrera["Nombre"]),
				"Universidad" => utf8_encode($carrera["Universidad"]),
				"Departamento" => utf8_encode($carrera["Departamento"]),
				"Municipio" => utf8_encode($carrera["Municipio"]),
				"Grupo" => utf8_encode($carrera["Grupo"]),
				"IdGrupo" => intval($carrera["IdGrupo"])
			);
		}
	}
	return $CARRERA;
}



?>
