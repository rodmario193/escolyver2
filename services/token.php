
<?php
/*
$configs = include('config.php'); //Solo para pruebas locales
//echo  ValidarToken('MmRkZmFkZTRiMDFiODhjMGEzNWFkYzRkODhkMjcxMDl8QVVHVVNUTzkw');
include('bitacora.php');
include(__DIR__ . '/database/DatabaseManager.php');
include(__DIR__ . '/error-manager/error_manager.php');
*/

function ValidarToken($TOKEN){
	$TOKEN_DEC  = urldecode(base64_decode($TOKEN));
	$piezas = explode("|", $TOKEN_DEC);
	$USUARIO = $piezas[1];
	$TOKEN_USUARIO = $piezas[0];
	$RESPUESTA = '';
	global $configs;
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$conn->connect_error){
		$sql = "Select 1 FROM USUARIO WHERE IdUsuario = '$USUARIO' AND Token = '$TOKEN_USUARIO' AND TIMEDIFF(NOW(), FechaToken) < '01:00:00'";

		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			if(ActualizarTokenUsuario($USUARIO)){
					$RESPUESTA = $USUARIO;
			}
		}
		$conn->close();
	}
	return $RESPUESTA;
}


function ValidarTokenAdmin($TOKEN){
	$TOKEN_DEC  = urldecode(base64_decode($TOKEN));
	$piezas = explode("|", $TOKEN_DEC);
	$USUARIO = $piezas[1];
	$TOKEN_USUARIO = $piezas[0];
	$RESPUESTA = '';
	global $configs;
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$conn->connect_error){
		$sql = "Select 1 FROM ADMINISTRADOR WHERE IdAdmin = '$USUARIO' AND Token = '$TOKEN_USUARIO' AND TIMEDIFF(NOW(), FechaToken) < '01:00:00'";

		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			if(ActualizarTokenAdmin($USUARIO)){
						$RESPUESTA = $USUARIO;
			}
		}
		$conn->close();
	}
	return $RESPUESTA;

}


function ObtenerIdUsuarioDeToken($TokenSesion){
	$Usuario = '';

	$Token_Desenc  = urldecode(base64_decode($TokenSesion));
	$Piezas_Token_Sesion = explode("|", $Token_Desenc);
	$Usuario = $Piezas_Token_Sesion[1];

	return $Usuario;
}

function SesionDeAdminAutenticada($TokenSesion){
	$Token_Desenc  = urldecode(base64_decode($TokenSesion));
	$Piezas_Token_Sesion = explode("|", $Token_Desenc);
	$Administrador = $Piezas_Token_Sesion[1];
	$TokenAdmin = $Piezas_Token_Sesion[0];
	$SesionAutenticada = false;
	global $configs;
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$conn->connect_error){
		$Sql = "Select 1 FROM ADMINISTRADOR WHERE IdAdmin = '$Administrador' AND Token = '$TokenAdmin' AND TIMEDIFF(NOW(), FechaToken) < '01:00:00'";
		$Resultado = $conn->query($Sql);
		if ($Resultado->num_rows > 0) $SesionAutenticada = true;
		$conn->close();
	}
	return $SesionAutenticada;
}

//echo SesionDeUsuarioAutenticada('MmRkZmFkZTRiMDFiODhjMGEzNWFkYzRkODhkMjcxMDl8QVVHVVNUTzkw');
function SesionDeUsuarioAutenticada($TokenSesion){
	$Token_Desenc  = urldecode(base64_decode($TokenSesion));
	$Piezas_Token_Sesion = explode("|", $Token_Desenc);
	$Usuario = $Piezas_Token_Sesion[1];
	$TokenUsuario = $Piezas_Token_Sesion[0];
	$SesionAutenticada = false;
	global $configs;
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
	if(!$conn->connect_error){
		$Sql = "Select 1 FROM USUARIO WHERE IdUsuario = '$Usuario' AND Token = '$TokenUsuario' AND TIMEDIFF(NOW(), FechaToken) < '01:00:00'";
		$Resultado = $conn->query($Sql);
		if ($Resultado->num_rows > 0) $SesionAutenticada = true;
		$conn->close();
	}
	return $SesionAutenticada;
}


function ActualizarTokenUsuarioConToken($TokenSesion){
	$IdUsuario = ObtenerIdUsuarioDeToken($TokenSesion);
	ActualizarTokenUsuario($IdUsuario);
}

/*
function ActualizarTokenUsuario($IdUsuario){
	try{
		global $configs;
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$Sql = "UPDATE USUARIO SET FechaToken = NOW() WHERE IdUsuario = ?";
			$query = $conn->prepare($Sql);
			$query->bind_param('s',$IdUsuario);
			$query->execute();
		}
	}catch(Exception $e){
		//Nada por ahora
	}
}
*/

function ActualizarTokenUsuario($IdUsuario){
	$Sql = "UPDATE USUARIO SET FechaToken = NOW() WHERE IdUsuario = ? ";
	//echo $Sql;
	$Params = array(
		$IdUsuario
	);
	$Resultado = DatabaseManager::executeQuery($Sql, $Params);
	return $Resultado;
}

function ActualizarTokenAdmin($IdAdmin){
	$Sql = "UPDATE ADMINISTRADOR SET FechaToken = NOW() WHERE IdAdmin = ? ";
	//echo $Sql;
	$Params = array(
		$IdAdmin
	);
	$Resultado = DatabaseManager::executeQuery($Sql, $Params);
	return $Resultado;
}


?>
