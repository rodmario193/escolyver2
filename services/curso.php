<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization, Data-Type");

$configs = include('config.php');
include('bitacora.php');
include('correo.php');
include('token.php');
include(__DIR__ . '/error-manager/error_manager.php');
include(__DIR__ . '/database/DatabaseManager.php');

$postdata = file_get_contents("php://input");

if(isset($postdata)) {
	$request = json_decode($postdata);
	if($request->Operacion == "lista"){
		WO_ListaCursos();
	}else if($request->Operacion == "agregar"){
		WO_AgregarCurso($request->Curso, $request->Param1);
	}else if($request->Operacion == "detalle"){
		WO_DetalleCurso($request->IdCurso, $request->Param1);
	}else if($request->Operacion == "eliminar"){
		WO_EliminarCurso($request->IdCurso, $request->Param1);
	}else if($request->Operacion == "modificar"){
		WO_ModificarCurso($request->Curso, $request->Param1);
	}

}


/* ****** Operaciones Web ****** */

function WO_DetalleCurso($ID_CURSO, $TOKEN){
	global $configs;
	$CURSO = "";
	$CODIGO = 0;
	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql = 	"SELECT 	IdCurso, Nombre, Descripcion, Categoria " .
					"FROM 		CURSO " .
					"WHERE 		IdCurso = $ID_CURSO";

			$result = $conn->query($sql);
			if ($result->num_rows == 1) {
				$curso = $result->fetch_assoc();
				$CURSO = array(
					"Id" => $curso['IdCurso'],
					"Nombre" => utf8_encode($curso['Nombre']),
					"Descripcion" => utf8_encode($curso['Descripcion']),
					"Categoria" => utf8_encode($curso['Categoria'])
				);
				$CODIGO = 1;
			}
			$conn->close();
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO,
		"Curso" => $CURSO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_ModificarCurso($CURSO, $TOKEN){
	$CODIGO = 0;
	global $configs;
	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){

			$ID_CURSO = $CURSO->Id;
			$NOMBRE = $CURSO->Nombre;
			$DESCRIPCION = $CURSO->Descripcion;
			$CATEGORIA = $CURSO->Categoria;

			$sql =  "UPDATE	CURSO " .
					"SET 	Nombre='$NOMBRE',Descripcion='$DESCRIPCION',Categoria='$CATEGORIA', UsuarioModifica='$ID_USUARIO', FechaModificacion=NOW() " .
					"WHERE 	IdCurso = '$ID_CURSO'";
			$conn->set_charset("utf8");
			if (mysqli_query($conn, $sql)) {
				$CODIGO = 1;
			}else{
				$CODIGO = $conn->errno;
			}
			$conn->close();

		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_AgregarCurso($CURSO, $TOKEN){
	$CODIGO = 0;
	global $configs;
	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){

			$NOMBRE = $CURSO->Nombre;
			$DESCRIPCION = $CURSO->Descripcion;
			$CATEGORIA = $CURSO->Categoria;

			$sql =  "INSERT INTO CURSO (Nombre, Descripcion, Categoria, UsuarioModifica, FechaModificacion)" .
					" VALUES('$NOMBRE','$DESCRIPCION','$CATEGORIA','$ID_USUARIO', NOW())";

			$conn->set_charset("utf8");
			if (mysqli_query($conn, $sql)) {
				$CODIGO = 1;
			}else{
				$CODIGO = $conn->errno;
			}
			$conn->close();

		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_EliminarCurso($ID_CURSO, $TOKEN){
	$CODIGO = 0;
	global $configs;
	$ID_USUARIO = ValidarTokenAdmin($TOKEN);

	if($ID_USUARIO!=''){
		$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);
		if(!$conn->connect_error){
			$sql =  "DELETE FROM CURSO WHERE IdCurso = $ID_CURSO";

			if (mysqli_query($conn, $sql)) {
				$CODIGO = 1;
			}else{
				$CODIGO = $conn->errno;
			}
			$conn->close();
		}
	}
	$RESPUESTA = array(
		"codigo" => $CODIGO
	);
	header('Content-type: application/json');
	echo json_encode(array("respuesta"=>$RESPUESTA));
}


function WO_ListaCursos(){
	global $configs;
	$cursos = array();
	$conn = new mysqli($configs['host'], $configs['username'], $configs['password'], $configs['database']);

	if(!$conn->connect_error){
		$sql = 	"SELECT IdCurso, Nombre, Descripcion " .
				"FROM 	CURSO";

		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			while($curso = $result->fetch_assoc()) {
				$cursos[] = array(
					"Id" => $curso['IdCurso'],
					"Nombre" => utf8_encode($curso['Nombre']),
					"Descripcion" => utf8_encode($curso['Descripcion'])
				);
			}
		}
		$conn->close();

	}
	header('Content-type: application/json');
	echo json_encode(array('cursos'=>$cursos));
}
